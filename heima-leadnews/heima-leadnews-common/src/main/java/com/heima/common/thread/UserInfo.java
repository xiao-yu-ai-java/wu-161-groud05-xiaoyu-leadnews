package com.heima.common.thread;

import lombok.Data;

/**
 * 给线程局部变量用
 */
@Data
public class UserInfo {
    private Long UserId;
}