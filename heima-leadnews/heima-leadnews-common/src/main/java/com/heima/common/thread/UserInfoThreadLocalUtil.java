package com.heima.common.thread;


/**
 * 局部变量的工具类 都要用所以放公共里面
 */
public class UserInfoThreadLocalUtil {
    private static final ThreadLocal<UserInfo> tl = new ThreadLocal<>();

    /**
     * 向ThreadLocal设置数据
     *
     * @param userInfo
     */
    public static void set(UserInfo userInfo) {
        if (userInfo != null) {
            tl.set(userInfo);
        }
    }

    /**
     * 从ThreadLocal中获取数据
     * @return
     */
    public static UserInfo get(){
        return tl.get();
    }

    /**
     * 清空线程中的数据 , 防止出现内存泄露
     */
    public static void remove(){
        tl.remove();
    }
}
