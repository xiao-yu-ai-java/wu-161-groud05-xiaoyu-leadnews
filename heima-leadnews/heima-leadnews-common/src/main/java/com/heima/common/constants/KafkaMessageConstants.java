package com.heima.common.constants;

public class KafkaMessageConstants {

    /**
     * 自媒体文章自动审核主题
     */
    public static final String WM_NEWS_AUTO_SCAN_TOPIC = "wm.news.auto.scan.topic";

    /**
     * 上下架的主题
     *
     */
    public static final String WM_Material_AUTO_SCAN_TOPIC = "wm.material.auto.scan.topic";

    /**
     * 文章数据同步到es的主题
     */
    public static final String AP_ARTICLE_ES_SYNC_TOPIC = "ap.article.es.sync.topic";
}