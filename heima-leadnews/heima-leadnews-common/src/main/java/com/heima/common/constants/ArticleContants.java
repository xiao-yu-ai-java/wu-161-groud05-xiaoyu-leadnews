package com.heima.common.constants;

public class ArticleContants {

    public static final short LOAD_TYPE_MORE = 1;

    public static final short LOAD_TYPE_NEW = 2;

    public static final String DEFAULT_TAG = "__all__";


    //点赞权重
    public static final Integer HOT_ARTICLE_LIKE_WEIGHT = 3;
    //阅读权重
    public static final Integer HOT_ARTICLE_VIEW_WEIGHT = 1;
    //评论权重
    public static final Integer HOT_ARTICLE_COMMENT_WEIGHT = 5;
    //收藏权重
    public static final Integer HOT_ARTICLE_COLLECT_WEIGHT = 8;

    //频道缓存前缀
    public static final String HOT_ARTICLE_PREFIX = "HOT_ARTICLE";
    //文章基本信息缓存key
    public static final String HOT_ARTICLE_INFO_KEY = "HOT_ARTICLE_INFO";
}