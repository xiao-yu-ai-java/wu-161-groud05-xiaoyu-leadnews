package com.heima.feign.api;

import com.heima.feign.config.LeadNewsFeignConfig;
import com.heima.feign.fallback.WmUserFeignFallbackFactory;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.wemedia.pojo.WmUser;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 申明远程调用 A调B   feign写在A里面
 * 声明降级工厂类 把降级工厂放到ioc中
 * 注解声明: 服务名称, 配置类 , 降级工厂
 */
@FeignClient(value = "leadnews-wemedia",configuration = LeadNewsFeignConfig.class,fallbackFactory = WmUserFeignFallbackFactory.class)
public interface WmUserFeignClient {

    /**
     * 对外暴露接口
     * @param wmUser
     * @return
     */
    @PostMapping("/api/v1/user/save")
    public ResponseResult save(@RequestBody WmUser wmUser);
}
