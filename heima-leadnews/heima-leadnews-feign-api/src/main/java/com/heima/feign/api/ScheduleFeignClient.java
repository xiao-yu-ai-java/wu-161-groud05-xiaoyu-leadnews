package com.heima.feign.api;

import com.heima.feign.config.LeadNewsFeignConfig;
import com.heima.feign.fallback.ScheduleFeignClientFallbackFactory;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.schedule.dto.TaskDto;
import com.heima.model.common.schedule.pojo.TaskInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 定时任务声明远程调用
 * 在客户端写注解configuration 说明要使用这个配置类的配置
 * fallbackFactory 指定降级工厂类型
 */
@FeignClient(value = "leadnews-schedule",configuration = LeadNewsFeignConfig.class,fallbackFactory = ScheduleFeignClientFallbackFactory.class)
public interface ScheduleFeignClient {

    /**
     * 新增任务
     * @return
     */
    @PostMapping(path = "/api/vi/task")
    public ResponseResult<Long> addTask(@RequestBody TaskDto taskDto);


    /**
     * 取消任务
     *
     * @param taskId
     * @return
     */
    @DeleteMapping(path = "/api/vi/task/{taskId}")
    public ResponseResult cancel(@PathVariable("taskId") Long taskId);


    /**
     * 拉取任务消费
     * @return
     */
    @GetMapping(path = "/api/vi/task/poll/{taskType}/{priority}")
    public ResponseResult<TaskInfo> pollTask(@PathVariable("taskType") Integer taskType, @PathVariable("priority") Integer priority);
}
