package com.heima.feign.fallback;

import com.heima.feign.api.ApUserFeignClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.user.pojo.ApUser;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApUserFeignFallbackFactory implements FallbackFactory<ApUserFeignClient> {


    @Override
    public ApUserFeignClient create(Throwable throwable) {
        return new ApUserFeignClient() {
            @Override
            public ResponseResult<ApUser> findUserById(Long id) {
                log.info("根据id调用用户服务没有成功");
                return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
            }
        };
    }
}
