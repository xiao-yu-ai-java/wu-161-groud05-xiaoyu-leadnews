package com.heima.feign.api;

import com.heima.feign.config.LeadNewsFeignConfig;
import com.heima.feign.fallback.ApArticFeignFallbackFactory;
import com.heima.model.common.article.dto.ApArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 远程声明调用 自媒体服务文章-->审核服务去- 调用文章微服务实现文章数据发布  还要编写降级(回滚)保护,降级工厂要搞成bean
 * 指定 服务, 指定降级类, 指定自动配置--调用失败自动执行降级类逻辑 自动
 */
//调用接口出现问题的时候自动执行降级方法  配置类就是为了自动执行
@FeignClient(value = "leadnews-article" , configuration = LeadNewsFeignConfig.class
,fallbackFactory = ApArticFeignFallbackFactory.class)
public interface ApArticleFeignClient {

    @PostMapping("api/v1/article/save")
    public ResponseResult<String> saveApArticle(@RequestBody ApArticleDto apArticleDto);



}
