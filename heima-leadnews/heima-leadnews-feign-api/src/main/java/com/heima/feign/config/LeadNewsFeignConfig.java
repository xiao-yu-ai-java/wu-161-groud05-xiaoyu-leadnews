package com.heima.feign.config;

import com.heima.feign.fallback.ApArticFeignFallbackFactory;
import com.heima.feign.fallback.ApUserFeignFallbackFactory;
import com.heima.feign.fallback.ScheduleFeignClientFallbackFactory;
import com.heima.feign.fallback.WmUserFeignFallbackFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 要在feign客户端加载它才可以   在客户端写注解说明要使用这个配置类的配置
 */
@Configuration
public class LeadNewsFeignConfig {

    //配降级工厂
    @Bean
    public ApArticFeignFallbackFactory apArticFeignFallbackFactory(){
        return new ApArticFeignFallbackFactory();
    }

    //配降级工厂
    @Bean
    public ScheduleFeignClientFallbackFactory scheduleFeignClientFallbackFactory(){
        return new ScheduleFeignClientFallbackFactory();
    }


    //配降级工厂
    @Bean
    public WmUserFeignFallbackFactory wmUserFeignFallbackFactory(){
        return new WmUserFeignFallbackFactory();
    }

    //配降级工厂
    @Bean
    public ApUserFeignFallbackFactory apUserFeignFallbackFactory(){
        return new ApUserFeignFallbackFactory();
    }
}
