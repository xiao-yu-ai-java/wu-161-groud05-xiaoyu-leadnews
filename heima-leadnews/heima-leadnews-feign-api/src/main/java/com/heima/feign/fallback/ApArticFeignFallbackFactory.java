package com.heima.feign.fallback;

import com.heima.feign.api.ApArticleFeignClient;
import com.heima.model.common.article.dto.ApArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * 文章服务的降级类  当调用feign失败的时候自动执行下面的代码 返回友好型信息
 * 实现接口FallbackFactory<ApArticleFeignClient> 指定要降级的类型
 * 接口 所以用匿名内部类来实现 返回友好信息
 */
@Slf4j
public class ApArticFeignFallbackFactory implements FallbackFactory<ApArticleFeignClient> {

    @Override
    public ApArticleFeignClient create(Throwable throwable) {
        return new ApArticleFeignClient(){

            @Override
            public ResponseResult<String> saveApArticle(ApArticleDto apArticleDto) {
                log.info("调用APP端文章服务出现wenti");
                return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
            }
        };
    }
}
