package com.heima.feign.fallback;

import com.heima.feign.api.WmUserFeignClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.wemedia.pojo.WmUser;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 *
 * 工厂想生效 ,要用配置类加载成Bean注册到ioc  Bean加载第三方bean
 */
@Slf4j
public class WmUserFeignFallbackFactory implements FallbackFactory<WmUserFeignClient> {
    @Override
    public WmUserFeignClient create(Throwable throwable) {
        return new WmUserFeignClient() {
            @Override
            public ResponseResult save(WmUser wmUser) {
                log.info("自媒体用户服务 ::::调用任务调度服务保存任务失败");
                return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
                //服务器内部错误
            }
        };
    }
}
