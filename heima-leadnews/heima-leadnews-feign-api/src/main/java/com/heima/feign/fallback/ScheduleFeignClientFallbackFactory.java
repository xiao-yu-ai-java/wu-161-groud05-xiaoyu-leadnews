package com.heima.feign.fallback;

import com.heima.feign.api.ScheduleFeignClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.schedule.dto.TaskDto;
import com.heima.model.common.schedule.pojo.TaskInfo;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * 降级类
 * 这就是一个类 如何让他生效, 首先要在配置类中配置 降级工厂类
 * 工厂想生效 ,要用配置类加载成Bean注册到ioc
 */
@Slf4j
public class ScheduleFeignClientFallbackFactory implements FallbackFactory<ScheduleFeignClient> {


    @Override
    public ScheduleFeignClient create(Throwable throwable) {
        return new ScheduleFeignClient() {
            @Override
            public ResponseResult<Long> addTask(TaskDto taskDto) {
                log.info("调用任务调度服务新增任务失败");
                return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
            }

            @Override
            public ResponseResult cancel(Long taskId) {
                log.info("调用任务调度服务取消任务失败");
                return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
            }

            @Override
            public ResponseResult<TaskInfo> pollTask(Integer taskType, Integer priority) {
                log.info("调用任务调度服务拉取任务失败");
                return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
            }
        };
    }
}
