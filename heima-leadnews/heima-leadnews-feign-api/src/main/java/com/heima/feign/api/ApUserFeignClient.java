package com.heima.feign.api;

import com.heima.feign.config.LeadNewsFeignConfig;
import com.heima.feign.fallback.ApUserFeignFallbackFactory;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.user.pojo.ApUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "leadnews-user",configuration = LeadNewsFeignConfig.class,fallbackFactory = ApUserFeignFallbackFactory.class)
public interface ApUserFeignClient {

    /**
     * 调用用户服务查询用户信息
     */
    @GetMapping("/api/v1/user/{id}")
    public ResponseResult<ApUser> findUserById(@PathVariable("id") Long id);
}
