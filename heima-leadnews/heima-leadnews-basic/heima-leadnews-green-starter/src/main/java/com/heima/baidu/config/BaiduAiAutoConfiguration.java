package com.heima.baidu.config;


import com.baidu.aip.contentcensor.AipContentCensor;
import com.heima.baidu.BaiduAiTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自动配置类
 */
@Configuration  //声明是配置类
@ConditionalOnClass()   //注入了依赖有对象才生效  条件注解
@EnableConfigurationProperties(BaiduAiProperties.class)  //开启自动属性配置扫xx.class
public class BaiduAiAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean //自己配了就用自己的, 没配用容器给我配的
    public AipContentCensor aipContentCensor(BaiduAiProperties baiduAiProperties){
        // 初始化一个AipContentCensor  直接去测试类拿
        AipContentCensor client = new AipContentCensor(baiduAiProperties.getAppId(),baiduAiProperties.getApiKey(),baiduAiProperties.getSecretKey());
        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);

        return client;
    }


    @Bean
    @ConditionalOnMissingBean //自己配了就用自己的, 没配用容器给我配的
    public BaiduAiTemplate baiduAiTemplate(AipContentCensor aipContentCensor){
        return new BaiduAiTemplate(aipContentCensor);
    }
}
