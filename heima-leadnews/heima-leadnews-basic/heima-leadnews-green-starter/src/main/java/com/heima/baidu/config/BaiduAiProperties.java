package com.heima.baidu.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * 配置类, 读取配置文件, 读取一些需要变动的配置
 */
@Data
@ConfigurationProperties(prefix = "leadnews.baidu")
public class BaiduAiProperties {
    private String appId;
    private String apiKey;
    private String secretKey;
}
