package com.heima.baidu;

import com.baidu.aip.contentcensor.AipContentCensor;
import com.baidu.aip.contentcensor.EImgType;
import com.heima.baidu.pojo.ScanResult;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author Administrator
 * @Date 2023/6/13
 **/
@Slf4j
public class BaiduAiTemplate {

    private AipContentCensor aipContentCensor;

    public BaiduAiTemplate(AipContentCensor aipContentCensor) {
        this.aipContentCensor = aipContentCensor;
    }

    /**
     * 百度AI文本审核
     * @param text
     * @return
     */
    public ScanResult textScan(String text) {
        // 调用接口
        JSONObject res = aipContentCensor.textCensorUserDefined(text);
        log.debug(res.toString());
        int conclusionType = res.getInt("conclusionType");
        String conclusion = res.getString("conclusion");

        List<String> keywords = new ArrayList<>();
        if (res.has("data")) {
            JSONArray jsonArray = res.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                JSONArray hits = jsonObject.getJSONArray("hits");
                if (hits!=null || hits.length()>0){
                    for (int j = 0; j < hits.length(); j++) {
                        List<Object> objectList = hits.getJSONObject(j).getJSONArray("words").toList();
                        keywords.addAll(objectList.stream().map(word->word.toString()).collect(Collectors.toList()));
                    }
                }
            }
        }

        return new ScanResult(conclusion, conclusionType, keywords);
    }

    /**
     * 图片审核
     *
     * @param url
     * @return
     */
    public ScanResult imageScan(String url) {
        // 调用接口
        JSONObject res = aipContentCensor.imageCensorUserDefined(url, EImgType.URL, null);
        log.debug(res.toString());
        //获取返回结果
        ScanResult scanResult = getScanResult(res);

        return scanResult;
    }

    /**
     * 图片审核
     *
     * @param imageByte
     * @return
     */
    public ScanResult imageScan(byte[] imageByte) {
        // 调用接口
        JSONObject res = aipContentCensor.imageCensorUserDefined(imageByte, null);
        log.debug(res.toString());
        //获取返回结果
        ScanResult scanResult = getScanResult(res);

        return scanResult;
    }

    /**
     * 解析图片审核结果
     *
     * @param res
     * @return
     */
    private ScanResult getScanResult(JSONObject res) {
        int conclusionType = res.getInt("conclusionType");
        String conclusion = res.getString("conclusion");
        List<String> keywords = new ArrayList<>();
        if (res.has("data")) {
            JSONArray jsonArray = res.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject.has("msg")) {
                    keywords.add(jsonObject.getString("msg"));
                }
            }
        }

        return new ScanResult(conclusion, conclusionType, keywords);
    }

}