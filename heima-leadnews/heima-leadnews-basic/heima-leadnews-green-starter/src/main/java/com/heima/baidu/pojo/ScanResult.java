package com.heima.baidu.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScanResult {
    private String conclusion;
    private Integer conclusionType;  //审核结果类型，可取值 1.合规，2.不合规，3.疑似，4.审核失败
    private List<String> words;       //哪里错的的提示信息
}