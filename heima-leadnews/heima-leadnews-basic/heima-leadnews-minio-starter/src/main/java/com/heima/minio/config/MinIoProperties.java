package com.heima.minio.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 配置属性
 */
@Data
@ConfigurationProperties(prefix = "leadnews.minio") //属性配置  指定读取配置文件前缀 就是一级一级的 spring: rabbit : ....
public class MinIoProperties {
    private String accessKey; //用户
    private String secretKey; //密码
    private String bucket;   //桶
    private String endpoint;  //连接点
    private String readPath;   //用于搭建集群
}
