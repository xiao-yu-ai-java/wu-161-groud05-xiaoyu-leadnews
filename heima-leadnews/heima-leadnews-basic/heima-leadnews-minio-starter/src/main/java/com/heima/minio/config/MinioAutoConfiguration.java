package com.heima.minio.config;

import com.heima.minio.MinIOTemplate;
import io.minio.MinioClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 项目一启动, 就会加载META-INF下的spring.factories
 * 这里里面是key value 形式的 key是spring自动配置, value是我们配置类的全路径. 这样就可以找到配置类. 配置类加@Configuration 注解声明是配置类,  在配置类中自定义bean. 同时要开启配置属性指定.class文件, 好去读取配置属性(配置属性中定义配置文件的前缀 更规范, 这样好找)
 * 哪里需要配置属性, 直接注入对象到参数中
 */

/**
 * 自动配置类
 */
@Configuration  //声明是配置类
@ConditionalOnClass(MinioClient.class)  //注入了依赖有对象才生效  条件注解
@EnableConfigurationProperties(MinIoProperties.class)  //开启自动属性配置扫xx.class  哪里要用注入对象就行
public class MinioAutoConfiguration {

    @ConditionalOnMissingBean  //容器中没有这个bean对象的时候容器的MinioClient就生效, 有就我们自己定义的
    @Bean  //自定义的bean
    public MinioClient minioClient(MinIoProperties minIoProperties){

     return MinioClient.builder()
             .endpoint(minIoProperties.getEndpoint())
             .credentials(minIoProperties.getAccessKey(),minIoProperties.getSecretKey())
             .build();
    }

    @Bean
    @ConditionalOnMissingBean //自己配了就用自己的, 没配用容器给我配的
    public MinIOTemplate minIOTemplate(MinioClient minioClient,MinIoProperties minIoProperties){
        return new MinIOTemplate(minioClient,minIoProperties);
    }
}
