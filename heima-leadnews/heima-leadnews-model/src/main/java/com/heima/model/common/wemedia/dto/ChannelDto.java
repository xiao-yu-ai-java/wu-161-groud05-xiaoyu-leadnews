package com.heima.model.common.wemedia.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChannelDto {
    private Long id; //id
    private String name; //频道名
    private String description; //频道描述
    private Integer ord; //频道排序
    private boolean isDefault;  //是否默认
    private boolean status; //频道状态
    private Date createdTime; //创建时间
}

