package com.heima.model.common.wemedia.pojo;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 自媒体文章表(WmNews)表实体类
 *
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("serial")
@TableName(value = "wm_news")
public class WmNews implements Serializable{
    //主键
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    //文章标题
    private String title;
    //封面图片;图片URL路径, 多个图片 , 分割
    private String images;
    //文章内容
    private String content;
    //作者id
    private Long userId;
    //频道id
    private Long channelId;
    //布局类型;0 : 无图 1 :单图 3:多图
    private Integer type;
    //文章标签
    private String labels;
    //文章状态;0 草稿
//        1 提交（待审核）
//        2 审核失败
//        3 人工审核
//        4 人工审核通过
//        8 审核通过（待发布）
//        9 已发布
    private Integer status;
    //发布时间
    private Date publishTime;
    //是否上架
    private Integer enable;
    //审核失败原因
    private String reason;
    //创建时间
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "UPDATED_TIME",fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;

}

