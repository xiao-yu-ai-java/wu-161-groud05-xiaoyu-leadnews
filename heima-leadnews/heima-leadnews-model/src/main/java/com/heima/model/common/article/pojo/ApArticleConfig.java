package com.heima.model.common.article.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 文章配置表(ApArticleConfig)表实体类
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
@Data
@SuppressWarnings("serial")
@TableName(value = "ap_article_config")
public class ApArticleConfig  {
    //文章id
    @TableId(value = "article_id",type = IdType.INPUT)
    private Long articleId;
    //是否上架
    private Integer enable;
    //是否删除
    private Integer isDelete;
    //是否允许评论
    private Integer isComment;
    //是否允许转发
    private Integer isForward;
    //创建时间
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "UPDATED_TIME",fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;

}

