package com.heima.model.common.recommend.vo;

import com.heima.model.common.article.pojo.ApArticle;
import lombok.Data;

@Data
public class HotArticleVo extends ApArticle {
    /**
     * 文章分值
     */
    private Integer score;
}