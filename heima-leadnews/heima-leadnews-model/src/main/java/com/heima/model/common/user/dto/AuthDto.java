package com.heima.model.common.user.dto;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.Data;

@Data
public class AuthDto extends PageRequestDto {

      /**
     * 状态
     */
    private Short status;

    /**
     * 用户id(实名认证id)
     */
    private  Long id;

    /**
     * 审核失败的原因
     */
     private String msg;

}