package com.heima.model.common.article.pojo;

import lombok.Data;

import java.util.Date;

/**
 * 文章基础信息表(ApArticle)表实体类
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
@Data
@SuppressWarnings("serial")
@TableName(value = "ap_article")
public class ApArticle  {
    //主键
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;
    //自媒体端文章ID
    private Long newsId;
    //文章标题
    private String title;
    //文章封面图片
    private String images;
    //文章作者ID
    private Long authorId;
    //作者名称
    private String authorName;
    //文章频道ID
    private Long channelId;
    //频道名称
    private String channelName;
    //文章布局类型;0 : 无图 1 :单图 3:多图
    private Integer layout;
    //文章标签
    private String labels;
    //文章发布时间
    private Date publishTime;
    //点赞数量
    private Integer likes;
    //评论书数量
    private Integer comment;
    //阅读数量
    private Integer views;
    //收藏数量
    private Integer collection;
    //创建时间
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Date createdTime;

    //更新时间
    @TableField(value = "UPDATED_TIME",fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;

    private String staticUrl;  //文章详情得URl

}

