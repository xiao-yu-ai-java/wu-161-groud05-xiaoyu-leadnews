package com.heima.model.common.user.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * APP用户表(ApUser)表实体类
 *
 * @author makejava
 * @since 2023-06-22 21:26:32
 */
@Data
@SuppressWarnings("serial")
@TableName(value = "ap_user")
public class ApUser {

    //主键
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    //用户名
    private String username;
    //密码
    private String password;
    //盐
    private String salt;
    //昵称
    private String nicknme;
    //头像
    private String image;
    //手机号
    private String phone;
    //性别;0: 男  1 :女
    private Integer gender;
    //状态;0 : 正常 , 1 : 封禁 , 2 : 删除
    private Integer status;
    //标签;0 : 普通用户 , 1 : 大v用户  2 : 企业用户
    private Integer flag;
    //是否认证;0: 未认证  1 : 已认证
    private Integer isAuthentication;
    //创建时间
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "UPDATED_TIME",fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;

}

