package com.heima.model.common.wemedia.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;


@Data  //与表映射
@TableName(value = "wm_user")
public class WmUser {
    @TableId(value = "id",type = IdType.AUTO)  //id属性与数据库表字段映射 这里主要是类型 如果 属性与数据库字段一样不用写
    private Long id ;
    private String username;
    private String salt ;
    private String password ;
    private String avatar ;
    private String phone ;
    private String email ;
    private String nickname ;
    private String location ;
    private Integer status ;
    private Integer type ;
    private Long apUserId;  //表改了, 实体类要加字段

    @TableField(fill = FieldFill.INSERT)  //新增时自动填充
    private Date createdTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)  //新增,修改自动填充
    private Date updatedTime ;
}
