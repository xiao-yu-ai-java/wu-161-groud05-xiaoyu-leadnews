package com.heima.model.common.schedule.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;

import java.util.Date;

/**
 * (TaskInfoLogs)表实体类
 *
 * @author makejava
 * @since 2023-06-18 19:34:57
 */
@Data
@SuppressWarnings("serial")
@TableName(value = "task_info_logs")
public class TaskInfoLogs  {
    //任务id   type : IdType.INPUT根据前一个表生产的id 自动插进去
    @TableId(value = "task_id",type = IdType.INPUT)
    private Long taskId;
    //执行时间
    private Date executeTime;
    //参数  数据库中存的是二进制  不同任务参数不同
    private byte[] parameters;
    //优先级
    private Integer priority;
    //任务类型
    private Integer taskType;
    //版本号,用乐观锁
    @Version  //当更新一条记录的同时, 希望这条记录没有背别人更新
    private Integer version;
    //状态 0=初始化状态 1=EXECUTED 2=CANCELLED
    private Integer status;

}

