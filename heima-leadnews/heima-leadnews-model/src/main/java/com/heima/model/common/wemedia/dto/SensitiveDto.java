package com.heima.model.common.wemedia.dto;


import lombok.Data;

/**
 * @author S
 * @date 2023/6/25 17:07
 */
@Data
public class SensitiveDto {
    private Long id; //id
    private String sensitives; //敏感词
}

