package com.heima.model.common.user.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * APP实名认证信息表(ApUserRealname)表实体类
 *
 * @author makejava
 * @since 2023-06-26 23:25:59
 */

@SuppressWarnings("serial")
@Builder //一旦用了这个就隐藏了有参无参构造 不能newle
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ap_user_realname")
public class ApUserRealname {
    //主键
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    //账号ID
    private Long userId;
    //用户名称
    private String name;
    //资源名称
    private String idno;
    //正面照片
    private String fontImage;
    //背面照片
    private String backImage;
    //手持照片
    private String holdImage;
    //活体照片
    private String liveImage;
    //状态
//            0 创建中
//            1 待审核
//            2 审核失败
//            9 审核通过
    private Integer status;
    //拒绝原因
    private String reason;
    //创建时间
    @TableField(value = "created_time",fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "updated_time",fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;

}

