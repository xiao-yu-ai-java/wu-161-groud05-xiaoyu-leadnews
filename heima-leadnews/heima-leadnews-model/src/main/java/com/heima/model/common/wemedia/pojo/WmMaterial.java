package com.heima.model.common.wemedia.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;
@Data
@TableName("wm_material")  //映射到数据库
public class WmMaterial {

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    
    private Integer userId;
    
    private String url;
    
    private Integer type;
    
    private Integer isCollection;

    @TableField(fill = FieldFill.INSERT)  //新增时自动填充
    private Date createdTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)  //新增,修改自动填充
    private Date updatedTime;

}