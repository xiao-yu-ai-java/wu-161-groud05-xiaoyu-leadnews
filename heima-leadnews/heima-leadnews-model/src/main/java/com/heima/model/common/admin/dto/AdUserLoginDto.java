package com.heima.model.common.admin.dto;

import lombok.Data;

@Data
public class AdUserLoginDto {

    //用户名
    private String name ;
    //密码
    private String password ;
}
