package com.heima.model.common.wemedia.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 返回对象
 */
@Data
@ApiModel(description = "自媒体登录传递的数据模型")
public class WmLoginDto {

     /**
     * 用户名
     */
     @ApiModelProperty(value = "用户名",example = "xiaowang")
    private String name;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码",example = "123456")
    private String password;
}