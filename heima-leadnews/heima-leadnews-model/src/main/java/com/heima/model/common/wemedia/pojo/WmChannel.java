package com.heima.model.common.wemedia.pojo;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 自媒体频道表(WmChannel)表实体类
 *
 * @author makejava
 * @since 2023-06-14 11:30:29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("serial")
@TableName(value = "wm_channel")
public class WmChannel  {
    //主键
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    //频道名称
    private String name;
    //是否是默认频道;0: 否 1: 是
    private Integer isDefault;
    //频道顺序
    private Integer ord;
    //频道描述
    private String description;
    //频道状态;0:禁用 , 1 : 启用 , 2 : 删除
    private Integer status;
    //创建时间
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "UPDATED_TIME",fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;

}

