package com.heima.model.common.wemedia.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 自媒体敏感词表(WmSensitive)表实体类
 *
 * @author makejava
 * @since 2023-06-16 17:55:08
 */
@Data
@SuppressWarnings("serial")
@TableName(value = "wm_sensitive")
public class WmSensitive {
    //主键
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    //敏感词
    private String sensitives;
    //创建时间
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "UPDATED_TIME",fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;

}

