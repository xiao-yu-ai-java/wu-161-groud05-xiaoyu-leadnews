package com.heima.model.common.wemedia.pojo;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

/**
 * 文章素材关联表(WmNewsMaterial)表实体类
 *
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
@Data
@SuppressWarnings("serial")
@TableName(value = "wm_news_material")
public class WmNewsMaterial  {
    //主键
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    //文章ID
    private Long newsId;
    //素材ID
    private Long materialId;
    //引用类型;0 : 封面引用  1 : 内容引用
    private Integer type;
    //创建时间
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "UPDATED_TIME",fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;

}

