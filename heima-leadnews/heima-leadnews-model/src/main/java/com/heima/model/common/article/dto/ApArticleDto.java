package com.heima.model.common.article.dto;

import com.heima.model.common.article.pojo.ApArticle;
import lombok.Data;

@Data
public class ApArticleDto extends ApArticle {

    private String content;
}
