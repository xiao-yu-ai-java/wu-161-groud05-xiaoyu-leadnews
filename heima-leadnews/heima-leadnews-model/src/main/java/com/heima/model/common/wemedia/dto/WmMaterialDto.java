package com.heima.model.common.wemedia.dto;

import com.heima.model.common.dtos.PageRequestDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "分页传递的参数 ")
public class WmMaterialDto extends PageRequestDto {

    /**
     * 1 收藏
     * 0 查询所有
     */
    @ApiModelProperty(value = "收藏或不收藏",example = "0",required = false)
    private Short isCollection;
}