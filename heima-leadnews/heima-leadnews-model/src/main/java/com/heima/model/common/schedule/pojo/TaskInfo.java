package com.heima.model.common.schedule.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * (TaskInfo)表实体类
 *
 * @author makejava
 * @since 2023-06-18 19:34:56
 */
@Data
@SuppressWarnings("serial")
@TableName(value = "task_info")
public class TaskInfo  {
    //任务id
    @TableId(value = "task_id",type = IdType.ASSIGN_ID)
    private Long taskId;
    //执行时间
    private Date executeTime;
    //参数  数据库中存的是二进制  不同任务参数不同
    private byte[] parameters;
    //优先级
    private Integer priority;
    //任务类型
    private Integer taskType;
    //任务是否被加载到redis
    private Integer isLoad;

}

