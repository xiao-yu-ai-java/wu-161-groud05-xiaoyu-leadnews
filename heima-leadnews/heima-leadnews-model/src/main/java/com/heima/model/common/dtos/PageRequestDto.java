package com.heima.model.common.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 分页查询 返回Dto 到时候实体类继承这个就可以不用写这2个属性
 */
@Data
@Slf4j
@ApiModel
public class PageRequestDto {

    @ApiModelProperty(value = "每页显示的数据条数",example = "5",required = true)
    protected Integer size;

    @ApiModelProperty(value = "当前页码",example = "6",required = true)
    protected Integer page;

    //检查
    public void checkParam() {
        if (this.page == null || this.page < 0) {
            setPage(1);
        }
        if (this.size == null || this.size < 0 || this.size > 100) {
            setSize(10);
        }
    }
}
