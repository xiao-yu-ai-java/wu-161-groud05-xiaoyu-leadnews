package com.heima.model.common.dtos;

import com.alibaba.fastjson.JSON;
import com.heima.model.common.enums.AppHttpCodeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 通用的结果返回类
 *
 * @param <T>
 */
@ApiModel
public class ResponseResult<T> implements Serializable {

    @ApiModelProperty(value = "响应主机名称, 一般返回图片路径用")
    private String host; //主机名称

    @ApiModelProperty(value = "响应码",example = "200")
    private Integer code;  //响应吗

    @ApiModelProperty(value ="响应错误信息" ,example = "500")
    private String errorMessage;  //错误信息

    @ApiModelProperty(value = "数据" )
    private T data;

    public ResponseResult() {
        this.code = 200;
    }

    public ResponseResult(Integer code, T data) {
        this.code = code;
        this.data = data;
    }

    public ResponseResult(Integer code, String msg, T data) {
        this.code = code;
        this.errorMessage = msg;
        this.data = data;
    }

    public ResponseResult(Integer code, String msg) {
        this.code = code;
        this.errorMessage = msg;
    }

    public static ResponseResult errorResult(int code, String msg) {
        ResponseResult result = new ResponseResult();
        return result.error(code, msg);
    }

    public static ResponseResult okResult(int code, String msg) {
        ResponseResult result = new ResponseResult();
        return result.ok(code, null, msg);
    }

    public static ResponseResult okResult(Object data) {
        ResponseResult result = setAppHttpCodeEnum(AppHttpCodeEnum.SUCCESS, AppHttpCodeEnum.SUCCESS.getErrorMessage());
        if (data != null) {
            result.setData(data);
        }
        return result;
    }

    public static ResponseResult errorResult(AppHttpCodeEnum enums) {
        return setAppHttpCodeEnum(enums, enums.getErrorMessage());
    }

    public static ResponseResult errorResult(AppHttpCodeEnum enums, String errorMessage) {
        return setAppHttpCodeEnum(enums, errorMessage);
    }

    public static ResponseResult setAppHttpCodeEnum(AppHttpCodeEnum enums) {
        return okResult(enums.getCode(), enums.getErrorMessage());
    }

    private static ResponseResult setAppHttpCodeEnum(AppHttpCodeEnum enums, String errorMessage) {
        return okResult(enums.getCode(), errorMessage);
    }

    public ResponseResult<?> error(Integer code, String msg) {
        this.code = code;
        this.errorMessage = msg;
        return this;
    }

    public ResponseResult<?> ok(Integer code, T data) {
        this.code = code;
        this.data = data;
        return this;
    }

    public ResponseResult<?> ok(Integer code, T data, String msg) {
        this.code = code;
        this.data = data;
        this.errorMessage = msg;
        return this;
    }

    public ResponseResult<?> ok(T data) {
        this.data = data;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }


    public static void main(String[] args) {
        //请求响应的几种形式
        //1. 服务端处理成功, 响应成功
        ResponseResult responseResult1 = ResponseResult.okResult(new HashMap<>());
        ResponseResult responseResult = ResponseResult.okResult(200, "操作成功");
        ResponseResult responseResult2 = ResponseResult.okResult(null);

        //2. 服务端处理失败, 响应错误信息
        ResponseResult responseResult3 = ResponseResult.errorResult(503, "没有找到可用服务");
        ResponseResult responseResult4 = ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        ResponseResult responseResult6=  ResponseResult.errorResult(401,"没有权限访问");

        //3. 服务端处理成功, 当前页, 每页数据条数, 数据总条数, 数据列表 响应成功 ---- 分页
        ResponseResult responseResult5 = new PageResponseResult(1,10,101);
        responseResult5.setData(new ArrayList<>());

        System.out.println(JSON.toJSONString(responseResult5));
    }

}
