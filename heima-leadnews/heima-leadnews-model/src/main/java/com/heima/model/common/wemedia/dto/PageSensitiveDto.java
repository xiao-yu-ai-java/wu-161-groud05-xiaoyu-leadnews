package com.heima.model.common.wemedia.dto;

import com.heima.model.common.dtos.PageRequestDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageSensitiveDto extends PageRequestDto {
    private String name; //敏感词
}
