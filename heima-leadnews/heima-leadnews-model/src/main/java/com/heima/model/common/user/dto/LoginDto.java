package com.heima.model.common.user.dto;

import lombok.Data;

@Data
public class LoginDto {

    private String phone;

    private String password;
}