package com.heima.model.common.article.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 文章内容信息表(ApArticleContent)表实体类
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
@Data
@SuppressWarnings("serial")
@TableName(value = "ap_article_content")
public class ApArticleContent  {
    //文章id
    @TableId(value = "article_id",type = IdType.INPUT)
    private Long articleId;
    //文章内容
    private String content;
    //创建时间
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Date createdTime;
    //更新时间
    @TableField(value = "UPDATED_TIME",fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;

}

