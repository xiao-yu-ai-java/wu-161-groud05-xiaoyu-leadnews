package com.heima.model.common.wemedia.vo;

import com.heima.model.common.wemedia.pojo.WmNews;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WmNewsVo extends WmNews{


    /**
     * 作者名称
     */
    private String authorName;
}
