package com.heima.freemarker.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* @author Administrator
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    private String name;
    private String sex;
    private int age;
    private Float balance;
}