<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>list指令</title>
    <style>
        table {
            margin: 100px auto;
            border: 1px solid black;
            font-weight: 30
        }

        td {
            border: 1px solid black;
            width: 40px;
        }

        .td_birth {
            width: 100px;
        }
    </style>
</head>
<body>

<table>
    <tr>
        <td>编号</td>
        <td>姓名</td>
        <td>性别</td>
        <td>年龄</td>
        <td>余额</td>
    </tr>
    <#list accountMap?keys as key>
        <#--accountMap遍历 ?keys得到key    没有key这个属性, 只能通过[]来取
             map得到key, 再得属性-->
        <tr>
            <td>${key_index+1}</td>
            <td>${accountMap[key].name}</td>
            <td>${accountMap[key].sex}</td>
            <td>${accountMap[key].age}</td>
            <td>${accountMap[key].balance}</td>
        </tr>
    </#list>
</table>

</body>
</html>