<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>list指令</title>
    <style>
        table {
            margin: 100px auto;
            border: 1px solid black;
            font-weight: 30
        }

        td {
            border: 1px solid black;
            width: 40px;
        }

        .td_birth {
            width: 100px;
        }
    </style>
</head>
<body>

<table>
    <tr>
        <td>编号</td>
        <td>姓名</td>
        <td>性别</td>
        <td>年龄</td>
        <td>余额</td>
    </tr>
    <#list list as account>
        <#--list集合根节点下, account遍历的每一个-->
        <tr>
            <td>${account_index+1}</td>
            <td>${account.name}</td>
            <td>${account.sex}</td>
            <td>${account.age}</td>
            <td>${account.balance}</td>
        </tr>
    </#list>
</table>

</body>
</html>