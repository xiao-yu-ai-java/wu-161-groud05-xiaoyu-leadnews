<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>list指令</title>
    <style>
        table {
            margin: 100px auto;
            border: 1px solid black;
            font-weight: 30;
            width: 400px;
        }

        td {
            border: 1px solid black;
            width: 40px;
        }

        .td_birth {
            width: 100px;
        }
    </style>
</head>
<body>

<table>
    <tr>
        <td>编号</td>
        <td>姓名</td>
        <td>性别</td>
        <td>年龄</td>
        <td>余额</td>
    </tr>
    <#list list as account >
        <#if account_index%2==0 >
            <tr style="background-color: skyblue">
                <td>${account_index+1}</td>
                <td>${account.name}</td>
                <td>${account.sex}</td>
                <td>${account.age}</td>
                <td>${account.balance}</td>
            </tr>
        <#else >
            <tr style="background-color: yellowgreen">
                <td>${account_index+1}</td>
                <td>${account.name}</td>
                <td>${account.sex}</td>
                <td>${account.age}</td>
                <td>${account.balance}</td>
            </tr>
        </#if>
    </#list>
</table>

</body>
</html>