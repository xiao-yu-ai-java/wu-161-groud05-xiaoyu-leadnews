package com.heima.freemarker;

import com.heima.freemarker.pojo.Account;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class FreemarkerTest {

    @Resource  //模板对象
    private Configuration configuration;

    @Test
    void test1() throws Exception {
        //1. 加载模板 , 获取模板对象
        Template template = configuration.getTemplate("01-basic.ftl");
        //3. 封装 数据模型  模板要什么就用啥封装
        Map<String, Object> dataModel = new HashMap<>();  //map集合封装 , 只有一层
        dataModel.put("name", "张三");

        //2. 将数据模板中的数据填充到模板, 生成静态文件
        template.process(dataModel, new FileWriter("C:\\Users\\123\\Desktop\\html\\01-basic.html"));
    }

    @Test
    void test2() throws IOException, TemplateException {
        //1. 加载模板 , 获取模板对象
        Template template = configuration.getTemplate("02-object.ftl");
        //3. 封装数据模型
        Map<String, Object> dataModel = new HashMap<>();

        Account account = new Account();
        account.setName("小王");
        account.setAge(18);
        account.setSex("男");
        account.setBalance(100f);
        //dataModel(map) -->account-->Name 从map的根节点开始account
        dataModel.put("account",account);

        //2. 将数据模板中的数据填充到模板, 生成静态文件
        template.process(dataModel, new FileWriter("C:\\Users\\123\\Desktop\\html\\02-object.html"));
    }

    @Test
    void test3() throws IOException, TemplateException {
        //1. 加载模板 , 获取模板对象
        Template template = configuration.getTemplate("02-object.ftl");
        //3. 封装数据模型

        Account account = new Account();
        account.setName("小王");
        account.setAge(18);
        account.setSex("男");
        account.setBalance(100f);  //这个只有一层, 所以直接写属性

        //2. 将数据模板中的数据填充到模板, 生成静态文件
        template.process(account, new FileWriter("C:\\Users\\123\\Desktop\\html\\02-object.html"));
    }

    @Test
    void test4() throws IOException, TemplateException {
        //1. 加载模板
        Template template = configuration.getTemplate("03-map.ftl");
        //2. 封装数据模型
        Map<String, Object> dataModel = new HashMap<>();

        Map<String,Object> data = new HashMap<>();
        data.put("name","小张");
        data.put("age",23);
        data.put("sex","女");
        data.put("balance",2100f);

        dataModel.put("data", data);

        //2. 填充数据生成HTML页面
        template.process(dataModel, new FileWriter("C:\\Users\\123\\Desktop\\html\\02-object.html"));
    }

    @Test// list
    void test04() throws IOException, TemplateException {
        //1. 加载模板
        Template template = configuration.getTemplate("04-list.ftl");
        //2. 封装数据模型
        Map<String, Object> dataModel = new HashMap<>();

        Account account1 = new Account("张三","男",18, 1000f);
        Account account2 = new Account("李四","女",20, 1000f);
        Account account3 = new Account("小红","女",16, 500f);
        Account account4 = new Account("小明","男",28, 2000f);
        Account account5 = new Account("小李","男",38, 5000f);

        List<Account> list = new ArrayList<>();
        list.add(account1);
        list.add(account2);
        list.add(account3);
        list.add(account4);
        list.add(account5);

        dataModel.put("list",list);

        //2. 填充数据生成HTML页面
        template.process(dataModel, new FileWriter("C:\\Users\\123\\Desktop\\html\\04-list.html"));
    }


    @Test
    void test5() throws IOException, TemplateException {
        //1. 加载模板
        Template template = configuration.getTemplate("05-list_map.ftl");
        //2. 封装数据模型
        Map<String, Object> dataModel = new HashMap<>();

        Account account1 = new Account("张三","男",18, 1000f);
        Account account2 = new Account("李四","女",20, 1000f);
        Account account3 = new Account("小红","女",16, 500f);
        Account account4 = new Account("小明","男",28, 2000f);
        Account account5 = new Account("小李","男",38, 5000f);

        Map<String,Account> accountMap = new HashMap<>();
        accountMap.put("account1",account1);
        accountMap.put("account2",account2);
        accountMap.put("account3",account3);
        accountMap.put("account4",account4);
        accountMap.put("account5",account5);

        dataModel.put("accountMap",accountMap);

        //2. 填充数据生成HTML页面
        template.process(dataModel, new FileWriter("C:\\Users\\123\\Desktop\\html\\05-list_map.html"));
    }


    @Test  /*条件判断*/

    void test6() throws IOException, TemplateException {
        //1. 加载模板
        Template template = configuration.getTemplate("06-if.ftl");
        //2. 封装数据模型
        Map<String, Object> dataModel = new HashMap<>();

        Account account1 = new Account("张三","男",18, 1000f);
        Account account2 = new Account("李四","女",20, 1000f);
        Account account3 = new Account("小红","女",16, 500f);
        Account account4 = new Account("小明","男",28, 2000f);
        Account account5 = new Account("小李","男",38, 5000f);

        List<Account> list = new ArrayList<>();
        list.add(account1);
        list.add(account2);
        list.add(account3);
        list.add(account4);
        list.add(account5);

        dataModel.put("list",list);

        //2. 填充数据生成HTML页面
        template.process(dataModel, new FileWriter("C:\\Users\\123\\Desktop\\html\\06-if.html"));
    }

}
