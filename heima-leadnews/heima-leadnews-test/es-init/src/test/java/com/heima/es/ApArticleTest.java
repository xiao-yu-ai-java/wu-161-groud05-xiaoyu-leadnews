package com.heima.es;

import com.alibaba.fastjson.JSON;
import com.heima.es.dao.ApArticleDao;
import com.heima.es.pojo.SearchArticleVo;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ApArticleTest {

    @Resource
    private ApArticleDao apArticleDao;

    @Resource
    private RestHighLevelClient restHighLevelClient;

    /**
     * 注意：数据量的导入，如果数据量过大，需要分页导入
     *
     * @throws Exception
     */
    @Test
    public void init() throws Exception {
       /*//把数据库中的数据查出来导入到es

       //1.查询数据库中的文章列表
        List<SearchArticleVo> articleVoList = apArticleDao.loadArticleList();
         //遍历拿到每一个文章对象
        for (SearchArticleVo searchArticleVo : articleVoList) {
            //2.创建es的请求对象
            IndexRequest request = new IndexRequest("app_article_info");
            //3.封装ES中的DSl请求参数  准备文章id  每一条数据到es都是一个文档对象
            request.id(searchArticleVo.getId().toString()).source(JSON.toJSONString(articleVoList),XContentType.JSON);
            //4.发送请求  (创建索引库)
            restHighLevelClient.index(request,RequestOptions.DEFAULT);

        }*/

        //把数据库中的数据查出来导入到es

        //1.查询数据库中的文章列表
        List<SearchArticleVo> articleVoList = apArticleDao.loadArticleList();
        //bulk批处理
        BulkRequest bulkRequest = new BulkRequest();
        //遍历拿到每一个文章对象
        for (SearchArticleVo searchArticleVo : articleVoList) {
           // searchArticleVo.setSuggestion(Arrays.asList(searchArticleVo.getLabels(),searchArticleVo.getTitle()));
            //2.创建es的请求对象
            IndexRequest request = new IndexRequest("app_article_info");
            //3.封装ES中的DSl请求参数  准备文章id  每一条数据到es都是一个文档对象
            request.id(searchArticleVo.getId().toString()).source(JSON.toJSONString(articleVoList),XContentType.JSON);
            //将添加索引库的请求设置到es批处理对象只中
            bulkRequest.add(request);
            //4.发送请求  (创建索引库)
            restHighLevelClient.index(request,RequestOptions.DEFAULT);
        }
    }

   /* @Test
    public void init02() throws Exception {
        //1. 查询所有的文章数据 (文章数据量太大, 例如 :1000万 , 分页查询)
        List<SearchArticleVo> articleVoList = apArticleDao.loadArticleList();
        //2. 导入数据到索引库
        //2.1 创建请求对象
        BulkRequest request = new BulkRequest();
        //2.2 组装DSL语句
        articleVoList.stream().forEach(searchArticleVo -> {
            searchArticleVo.setSuggestion(Arrays.asList(searchArticleVo.getLabels(),searchArticleVo.getTitle()));
            request.add(new IndexRequest("app_article_info").id(searchArticleVo.getId() + "").source(JSON.toJSONString(searchArticleVo), XContentType.JSON));
        });
        //2.3 发送请求
        restHighLevelClient.bulk(request, RequestOptions.DEFAULT);
    }*/
}