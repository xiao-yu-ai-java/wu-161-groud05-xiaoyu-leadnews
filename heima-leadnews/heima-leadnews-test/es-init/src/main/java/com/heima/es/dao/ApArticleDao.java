package com.heima.es.dao;

import com.heima.es.pojo.SearchArticleVo;

import java.util.List;

public interface ApArticleDao {

    /**
     * 查询所有的频道列表
     * @return
     */
    List<SearchArticleVo> loadArticleList();
}
