package com.heima.minio;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest(classes = MinioApplication.class)
public class MinioStartTest {

    @Resource
    private MinIOTemplate minIOTemplate;

    @Test
   public void upload() throws FileNotFoundException {
        FileInputStream is = new FileInputStream("D:\\2.jpg");
        String url = minIOTemplate.uploadImgFile("", "2.jpg", is);
        System.out.println(url);
    }
}
