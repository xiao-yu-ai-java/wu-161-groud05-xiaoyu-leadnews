package com.heima.minio;


import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.errors.*;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class MinioTest {

//    minio中文件的上传 ，下载等相关操作封装到一个模板类中(类似之前学习的 RedisTemplate ,RabbitTemplate)
//    摸板类对象需要框架自动完成配置， 哪里需要直接注入对象即可
//    相关配置需要抽取为配程文件和参数
//       配置文件(每个操作都需要使用，而且一样的) : accessKey ，secretKey ，endpoint ，bucket
//       方法参数(对不同文件操作 ， 不一样的参数) : 文件流 ， 文件名称 ， 文件类型

    @Test
    void uploadImage() throws Exception {

        //1.创建minio客户端对象
        MinioClient minioClient = MinioClient.builder()
                .credentials("minio", "minio123")  //凭证:用户密码
                .endpoint("http://192.168.200.130:9000")  //连接上传地址
                .build();

        //2.构建上传文件的请求参数
        FileInputStream is = new FileInputStream("D:\\1.jpg");   //文件流
        PutObjectArgs args = PutObjectArgs.builder()   //构建 添加文件参数
                .bucket("leadnews")  //桶 目录
                .object("2.jpg")  //文件名称
                .contentType("image/jpeg") //文件类型
                .stream(is, is.available(), -1)  //文件流  文件可用 文件过大分开上传
                .build();


        //3.发送请求上传文件
        minioClient.putObject(args);
    }
}
