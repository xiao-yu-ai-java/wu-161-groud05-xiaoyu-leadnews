package com.heima.xxl.job;

import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Author Administrator
 * @Date 2023/6/29
 **/
@Component
@Slf4j
public class SampleXxlJob {

    @XxlJob("hellojob")  //指定是哪一个任务
    public void demoJobHandler() throws Exception {
        String jobParam = XxlJobHelper.getJobParam();
        //获取分片节点数量
        int shardTotal = XxlJobHelper.getShardTotal();
        //获取当前节点的分片编号
        int shardIndex = XxlJobHelper.getShardIndex();
        log.info("定时任务执行了 , 任务参数:{},分片数量:{},分片编号:{}", jobParam,shardTotal,shardIndex);
    }

    @XxlJob("childJob")
    public void childJob() throws Exception {
        log.info("子任务开始执行了--------");
    }
}
