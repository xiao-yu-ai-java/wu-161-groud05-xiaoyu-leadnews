package com.heima.mongo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * ORM思想 : 0bject Relation Mapping (对象关系映射)
 * 为Java对象和实体类建议关联映射，我们作Java对象就相当于作数据
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "user")  //数据库集合名称
public class User {

    @Id
    private ObjectId id;
    @Field(name = "username")  //如果数据库的字段和属性类的一样, 就可以不用写
    private String username;
    @Field(name = "nickname")
    private String nick_name;
    private String password;
    private Integer age;

}