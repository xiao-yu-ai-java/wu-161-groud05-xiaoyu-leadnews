package com.heima.mongo;

import com.heima.mongo.pojo.Places;
import com.heima.mongo.pojo.User;
import org.bson.types.ObjectId;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Metrics;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
public class MongoTest {

    @Resource
    private MongoTemplate mongoTemplate;

    /**
     * 新增
     */
    @Test
    void add(){
        User user = new User(ObjectId.get(), "xiaowang", "小王", "123456", 20);
        User user1 = new User(ObjectId.get(), "hongqigong", "北丐", "123456", 18);
        User user2 = new User(ObjectId.get(), "ouyangfeng", "欧阳锋", "123456", 20);
        User user3 = new User(ObjectId.get(), "wangchongyang", "王重阳", "123456", 28);
        User user4 = new User(ObjectId.get(), "huangrong", "huangrong", "123456", 30);
        mongoTemplate.save(user1);
        mongoTemplate.save(user2);
        mongoTemplate.save(user3);
        mongoTemplate.save(user4);
        mongoTemplate.save(user);
    }


    /**
     * 修改
     */
    @Test
    void update(){
        User user = new User(new ObjectId("64965d007fe337158f34e37c"), "xiaowang", "小肖", "123456", 20);
        //新增或者修改, 如果id在数据库中已经存在, 执行修改操作
        mongoTemplate.save(user);
    }


    /**
     * 删除
     */
    @Test
    void deleteById(){
        User user = new User(new ObjectId("64965dba79612c5c0ff1ed26"), "xiaowang", "小肖", "123456", 20);
        //根据对象中的ID删除数据
        mongoTemplate.remove(user);
    }

    /**
     * 查询
     *
     */
    @Test
    void find(){
        //查询所有
//        List<User> userList = mongoTemplate.findAll(User.class);
//        userList.stream().forEach(user -> System.out.println(user));

        //根据id查询详情
        User user = mongoTemplate.findById(new ObjectId("64965dba79612c5c0ff1ed26"), User.class);
        System.out.println(user);
    }

    /**
     * 查询
     */

    //模糊 一个多个, 可有可无 .*?
    @Test
    void query(){
        //根据唯一字段条件查询单个对象: 等值查询
        /*Query query = Query.query(Criteria.where("username").is("xiaowang"));
        User user = mongoTemplate.findOne(query, User.class);*/

        //(条件查询)范围查询
        /*Query query = Query.query(Criteria.where("age").gte(18));
        List<User> userList = mongoTemplate.find(query, User.class);
        userList.stream().forEach(user -> System.out.println(user));*/

        //模糊查询(正则查询)   //模糊 一个或多个, 可有可无 .*?
        Query query = Query.query(Criteria.where("nick_name").regex(".*?王.*"));

        List<User> userList = mongoTemplate.find(query, User.class);
        userList.stream().forEach(user -> System.out.println(user));
    }

    /**
     * and
     * 多条件查询
     */

    @Test
    void multiQuery01And(){
        //查询昵称中包含王,且年纪大于20岁的用户信息---并且
        Query query = Query.query(Criteria.where("nick_name").regex(".*?王.*").and("age").gt(20));
        List<User> userList = mongoTemplate.find(query, User.class);
        userList.stream().forEach(user -> System.out.println(user));
    }

    /**
     *
     * or
     * 多条件查询
     * 要new  Criteria
     */

    @Test
    void multiQuery02Or(){
      //查询昵称中包含王或者年龄大于20岁的用户信息 --- 或者  要new Criteria
        //条件 拼接or
        Criteria criteria = new Criteria();
        criteria.orOperator(Criteria.where("nick_name").regex(".*?王.*"), Criteria.where("age").gt(20));
       //条件
        Query query = Query.query(criteria);
        List<User> userList = mongoTemplate.find(query, User.class);
        userList.stream().forEach(user -> System.out.println(user));
    }


    //查询年龄大于20 并且昵称中包含王或者昵称中包含黄的用户信息
    //orOperator 里面是或,外面是and
    //select * form user where age>20 and (name like '%王%' orname like '%黄%' )
    @Test
    void multiQuery03AndOr(){
        Query query = Query.query(Criteria.where("age").gt(20).orOperator(Criteria.where("nick_name").regex(".*?王.*"), Criteria.where("nick_name").regex(".*?黄.*")));


        List<User> userList = mongoTemplate.find(query, User.class);
        userList.stream().forEach(user -> System.out.println(user));
    }


    /**
     * 分页查询
     */
    @Test
    void page(){
    //分页查询年龄>18岁的用户信息 ， 每页展示2条数据 skip = (页码 - )*size
    //Query query = Query.query(Criteria.where("age").gt(18)).skip(2).limit(2);

        // PageRequest.of(0,2) : 设置分页参数，page参数从0开始 ，0 代表第一页，1代表第二页

        Query query = Query.query(Criteria.where("age").gt(18)).with(PageRequest.of(0, 2));
        List<User> userList = mongoTemplate.find(query, User.class);
        userList.stream().forEach(user -> System.out.println(user));
    }


    /**
     *排序
     * //排序 : 查询年>18岁的用户信息 ， 按照年大小排序(大--->小)
     * // Sort.by("age").descending() : 设置排序字段和排序方式
     */
    @Test
    void sort(){
        Query query = Query.query(Criteria.where("age").gt(18)).with(Sort.by("age").descending());

        List<User> userList = mongoTemplate.find(query, User.class);
        userList.stream().forEach(user -> System.out.println(user));
    }

    /**
     * 分业$排序:
     * //分页&排序 : 分页查询年>18 岁的用户信息，每页展示2条数据 ， 按照年大小排序(大--->小)
     * //Query query = Query.query(Criteria.where("age").gt(18).with(PageRequest.of(0,2)).with(Sort.by("age").descending());
     */

    @Test
    void sortAndPage(){
        Query query = Query.query(Criteria.where("age").gt(18)).with(PageRequest.of(0,2,Sort.by("age").descending()));
        List<User> userList = mongoTemplate.find(query, User.class);
        userList.stream().forEach(user -> System.out.println(user));
    }









    @Test
    public void testAdd() {

        Places places = new Places();
        places.setId(ObjectId.get());
        places.setAddress("湖北省武汉市东西湖区金山大道");
        places.setTitle("金山大道");
        places.setLocation(new GeoJsonPoint(114.226867, 30.636001));
        mongoTemplate.save(places);

        Places places2 = new Places();
        places2.setId(ObjectId.get());
        places2.setAddress("湖北省武汉市东西湖区奥园东路");
        places2.setTitle("奥园东路");
        places2.setLocation(new GeoJsonPoint(114.240592, 30.650171));
        mongoTemplate.save(places2);

        Places places3 = new Places();
        places3.setId(ObjectId.get());
        places3.setAddress("湖北省武汉市黄陂区X003");
        places3.setTitle("黄陂区X003");
        places3.setLocation(new GeoJsonPoint(114.355876, 30.726886));
        mongoTemplate.save(places3);

        Places places4 = new Places();
        places4.setId(ObjectId.get());
        places4.setAddress("湖北省武汉市黄陂区汉口北大道");
        places4.setTitle("汉口北大道");
        places4.setLocation(new GeoJsonPoint(114.364111, 30.722166));
        mongoTemplate.save(places4);
    }

    /*Metrics 度量*/
    @Test
    void nearQuery(){
        //2.构建nearQuery对象, 指定中心点和半径
        NearQuery nearQuery = NearQuery.near(new GeoJsonPoint(114.364111, 30.722166), Metrics.KILOMETERS).maxDistance(50, Metrics.KILOMETERS);
        //1.执行地理位置查询
        GeoResults<Places> geoResults = mongoTemplate.geoNear(nearQuery, Places.class);
        //3. 解析查询结果
        for (GeoResult<Places> geoResult : geoResults) {
            //获取原始文档数据
            Places places = geoResult.getContent();
            Distance distance = geoResult.getDistance();

            System.out.println(places+"---距离："+distance + "km");
        }

    }

}


