package com.heima.kafka.listener;


import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * 接收消息(监听)Listener
 */
@Component
@Slf4j
public class KafkaConsumerListener {


    //传过来的是key value 收就用ConsumerRecord收  要配置文件指定去哪里消费
    //要监听topic , 就要先申明topic 在消费者或者生产者里面都可以 搞个配置类
    @KafkaListener(topics = "test.topic1",groupId = "test1")
    public void listenTopic1(ConsumerRecord<String,String> record){
        String key = record.key();
        String value = record.value();

        log.info("接收到topic1的消息,消息内容,key:{},value:{}",key,value);
    }
}
