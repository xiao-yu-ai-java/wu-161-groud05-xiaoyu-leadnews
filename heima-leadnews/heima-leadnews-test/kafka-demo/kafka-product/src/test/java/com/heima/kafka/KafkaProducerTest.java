package com.heima.kafka;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;

import javax.annotation.Resource;

@SpringBootTest
public class KafkaProducerTest {

    @Resource
    private KafkaTemplate kafkaTemplate;

    //发送什么topic主题, 收就用什么topic  key可以不写
    @Test
    public void sendTOTopic1(){
        kafkaTemplate.send("test.topic1","hello","nihaoa");
    }
}
