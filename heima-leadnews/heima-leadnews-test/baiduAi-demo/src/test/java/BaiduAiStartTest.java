import com.heima.BaiduAiApplication;
import com.heima.baidu.BaiduAiTemplate;
import com.heima.baidu.pojo.ScanResult;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
//必须加这个classes
@SpringBootTest(classes = BaiduAiApplication.class)
public class BaiduAiStartTest {

    @Resource
    private BaiduAiTemplate baiduAiTemplate;

    @Test
    void testTextScan(){
        ScanResult scanResult = baiduAiTemplate.textScan("一箭41星！吉林长光卫星创中国航天发射新纪录！北京时间2023年6月15日13时30分，由吉林长光卫星技术股份有限公司研制的“吉林一号”高分06A系列卫星、“吉林一号”高分03D19-26星、“吉林一号”平台02A01-02星等41颗卫星，在中国太原卫星发射中心搭载长征二号丁运载火箭发射升空，创造了中国航天单次发射卫星数量最多的纪录。在");
        System.out.println(scanResult);
    }
}
