package com.heima.app.gateway.filter;

import com.heima.utils.common.AppJwtUtil;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
@Order(1)
public class TokenFilter implements GlobalFilter {

    /**
     *
     * @param exchange  交换机  web数据和网关数据交互的桥梁
     * @param chain   过滤器链
     * @return
     * 放行 :chain.filter(exchange);
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

       /* 1.获得请求响应对象
        2.放行登录相关信息
        3.获取请求头中的token
        4.校验token是否有效 */


        //1.获得请求响应对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        //2.放行登录相关信息
        if (request.getURI().getPath().contains("/login/login_auth")) {
            return chain.filter(exchange);  //放行
        }
        //3.获取请求头中的token
        String token = request.getHeaders().getFirst("token");
        //请求中没token  返回401状态码
        if (StringUtils.isEmpty(token)){
            response.setStatusCode(HttpStatus.UNAUTHORIZED);  //在http响应中设置状态码
            response.setComplete();  //设置响应结束
        }


        //4.校验token是否有效
        Claims claims = AppJwtUtil.getClaimsBody(token); ///解析token

        if (claims==null){
            response.setStatusCode(HttpStatus.UNAUTHORIZED);  //在http响应中设置状态码
            response.setComplete();  //设置响应结束
        }

        //判断时效性
        int result = AppJwtUtil.verifyToken(claims);
        if (result==0||result==-1){

            /*为了得到用户的id 在请求对象token中拿id 并且重构请求头, 再重构交换机
             * 网关-->过滤器----->拦截器-->controller
             * 获取token中用户id, 设置到请求中路由到拦截器
             * */
            Long userId = claims.get("id", Long.class);

            //mutate 请求对象重构 请求头, 最后加bulid
            ServerHttpRequest httpRequest = request.mutate().headers(httpHeaders -> httpHeaders.add("userId", userId + "")).build();
            //交换机重构请求
            ServerWebExchange webExchange = exchange.mutate().request(httpRequest).build();


            return chain.filter(webExchange);  //放行
        }


        //token无效  返回401
        response.setStatusCode(HttpStatus.UNAUTHORIZED);  //在http响应中设置状态码

        return  response.setComplete();  //设置响应结束
    }
}


