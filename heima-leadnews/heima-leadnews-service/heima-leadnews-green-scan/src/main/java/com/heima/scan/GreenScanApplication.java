package com.heima.scan;

import com.heima.feign.api.ApArticleFeignClient;
import com.heima.feign.api.ScheduleFeignClient;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication
@MapperScan(basePackages = "com.heima.scan.mapper")
//@EnableFeignClients(basePackages = "com.heima.feign.api")
@EnableFeignClients(clients = {ApArticleFeignClient.class, ScheduleFeignClient.class})
public class GreenScanApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreenScanApplication.class, args);
    }

}