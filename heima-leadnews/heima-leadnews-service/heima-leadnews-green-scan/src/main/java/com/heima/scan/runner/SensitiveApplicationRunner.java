package com.heima.scan.runner;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import com.heima.model.common.wemedia.pojo.WmSensitive;
import com.heima.scan.mapper.WmSensitiveMapper;
import com.heima.utils.common.SensitiveWordUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**监听器
 * 一个类实现了ApplicationRunner接口 监听器, 实现其中的run方法之后
 * 当spring项目启动完成之后就会自动执行其中的run方法 , 一般用于一些数据的初始化操作
 * @Author Administrator
 * @Date 2023/6/15
 **/
@Component
@Slf4j
public class SensitiveApplicationRunner implements ApplicationRunner {

    @Resource
    private WmSensitiveMapper wmSensitiveMapper;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("spring项目启动完成了, 开始执行敏感词DFA初始化操作------------------------------------");
        //1. 从数据看中查询敏感词列表
        List<WmSensitive> sensitiveList = wmSensitiveMapper.selectList(Wrappers.emptyWrapper());
        if (CollectionUtils.isEmpty(sensitiveList)) {
            return;
        }
        List<String> words = sensitiveList.stream().map(sensitive -> sensitive.getSensitives()).collect(Collectors.toList());
        //2. 初始化DFA数据结构
        SensitiveWordUtil.initMap(words);
    }
}