package com.heima.scan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.wemedia.pojo.WmChannel;
import org.springframework.stereotype.Repository;

/**
 * 自媒体频道表(WmChannel)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-14 11:30:28
 */
@Repository
public interface WmChannelMapper extends BaseMapper<WmChannel> {

}

