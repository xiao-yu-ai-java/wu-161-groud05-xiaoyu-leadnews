package com.heima.scan.service;

import com.heima.model.common.wemedia.pojo.WmNews;

public interface WmNewsAutoScanService {


    /**
     * 自媒体文章自动审核
     * @param wmNews
     */
    void newsAutoScan(WmNews wmNews);
}
