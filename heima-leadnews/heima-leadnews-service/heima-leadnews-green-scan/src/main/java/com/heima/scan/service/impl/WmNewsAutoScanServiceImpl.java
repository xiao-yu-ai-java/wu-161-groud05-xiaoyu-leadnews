package com.heima.scan.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.baidu.BaiduAiTemplate;
import com.heima.baidu.pojo.ScanResult;
import com.heima.common.enums.TaskTypeEnum;
import com.heima.common.exception.CustomException;
import com.heima.feign.api.ApArticleFeignClient;
import com.heima.feign.api.ScheduleFeignClient;
import com.heima.minio.MinIOTemplate;
import com.heima.model.common.article.dto.ApArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.schedule.dto.TaskDto;
import com.heima.model.common.wemedia.pojo.WmChannel;
import com.heima.model.common.wemedia.pojo.WmNews;
import com.heima.model.common.wemedia.pojo.WmSensitive;
import com.heima.model.common.wemedia.pojo.WmUser;
import com.heima.scan.mapper.WmChannelMapper;
import com.heima.scan.mapper.WmNewsMapper;
import com.heima.scan.mapper.WmSensitiveMapper;
import com.heima.scan.mapper.WmUserMapper;
import com.heima.scan.service.WmNewsAutoScanService;
import com.heima.utils.common.ProtostuffUtil;
import com.heima.utils.common.SensitiveWordUtil;
import jdk.nashorn.api.scripting.ScriptUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class WmNewsAutoScanServiceImpl implements WmNewsAutoScanService {

    @Resource
    private BaiduAiTemplate baiduAiTemplate;

    @Resource
    private MinIOTemplate minIOTemplate;


    @Resource
    private WmNewsMapper wmNewsMapper;

    @Resource
    private WmSensitiveMapper wmSensitiveMapper;

    @Resource
    private ApArticleFeignClient apArticleFeignClient;

    @Resource
    private WmUserMapper wmUserMapper;

    @Resource
    private WmChannelMapper wmChannelMapper;

    @Resource
    private ScheduleFeignClient scheduleFeignClient;


    /**
     * 自媒体文章自动审核
     *自媒体服务保存文章到Wmnews表中, 就开始用kafka 异步发送消息, 自动扫描类就可以监听到, 开始检查
     *
     * @param wmNews
     */
    @Override
    public void newsAutoScan(WmNews wmNews) {
        //1.文章状态是否是待审   1 是提交(待审核)
        if (wmNews.getStatus() != 1) {
            return;  //结束这个方法
        }
        //2.抽取文章中的文本和图片数据  map集合 "test": "xxx",   "images": {"url","url2"}.
        Map<String, Object> imageAndTest = extractImageAndTest(wmNews);

        //补充-----做baiduAI 审核前, 要自管理敏感词审核
        String text = imageAndTest.get("text").toString();
        ScanResult sensitiveScanResult = sensitiveWordScan(text);
        //1.合规，2.不合规，3.疑似，4.审核失败
        if (sensitiveScanResult.getConclusionType() != 1) {  //1为合格
            updateWmNewStatus(wmNews, sensitiveScanResult);
            return;
        }

        //3.调用百度AI对文章的文本审核
        // String text = imageAndTest.get("text").toString();
        ScanResult textScanResult = textScan(text);
        //1.合规，2.不合规，3.疑似，4.审核失败
        if (textScanResult.getConclusionType() != 1) {  //1为合格
            updateWmNewStatus(wmNews, textScanResult);
            return;
        }
        //4.调用百度AI对文章的图片审核
        List<String> images = (List<String>) imageAndTest.get("images");
        ScanResult imagesScanResult = imagesScan(images);

        if (imagesScanResult.getConclusionType() != 1) {
            updateWmNewStatus(wmNews, imagesScanResult);
            return;
        }

        // 5.更新文章的审核状态
        updateWmNewStatus(wmNews, new ScanResult("合格", 1, Collections.emptyList()));

        //TODO 发布文章到APP端 未完成+++++++++
        //6.通过feign 发布文章到APP端
        //定时时间到了发布
        /*if (wmNews.getPublishTime().getTime() <= System.currentTimeMillis()) {

            saveApArticle(wmNews);
            //修改文章状态为审核通过(已发布)
            wmNews.setStatus(9);
            wmNewsMapper.updateById(wmNews);
        }*/

        //6.发布延迟任务, 文章发布时间到了之后自动执行文章发布(审核服务完成后, 调用延迟任务服务)
        wmNewsAutoPublish(wmNews);
    }




    /**
     * 抽取文章中的文本和图片数据
     *
     * @param wmNews
     * @return
     */
    private Map<String, Object> extractImageAndTest(WmNews wmNews) {
        //文本: 标题, 内容
        //图片: 封面, 内容

        //*1.拼接文本的容器
        StringBuilder textSb = new StringBuilder();  //textSb里面存有所有的文本内容
        //*2.存图片的URL
        List<String> imagesList = new ArrayList<>();

        //1.2抽取--文本---数据
        textSb.append(wmNews.getTitle());  //抽取标拼接
        String content = wmNews.getContent();  //content 格式是json格式的数组
        List<Map> mapList = JSON.parseArray(content, Map.class);   //转化json(字符串)格式的数组为map集合
        //1.3利用stream取出type为text的. 取出他的value拼接  --操作集合 遍历
        mapList.stream().forEach(map -> {
            if ("text".equals(map.get("type"))) {
                textSb.append(map.get("value") + "@"); //拼接 2段拼接加特殊符号避免出现末尾组合违规
            }
        });

        //2.1抽取图片数据
        //2.1.1封面图片类容
        String images1 = wmNews.getImages();  //字符串里面有多个url 用","隔开
        if (StringUtils.isNotEmpty(images1)) {
            //split是数组, 里面是图片url  Arrays.asList转为集合
            String[] split = images1.split(",");
            //集合.addAll(集合1, 集合2) list.addAll 逐一添加集合1,2中内容
            imagesList.addAll(Arrays.asList(split));
        }


        //2.1.2内容图片类容
        List<String> imageTextUrlList = mapList.stream()
                .filter(map -> "text".equals(map.get("type")))
                .map(map -> map.get("value").toString())
                .collect(Collectors.toList());
        // 把imageTextUrlList元素添加到imagesList中用addAll
        imagesList.addAll(imageTextUrlList);


        HashMap<String, Object> result = new HashMap<>();
        result.put("text", textSb.toString());
        result.put("images", imagesList);
        return result;
    }

    /**
     * 调用百度AI做文本审核
     *
     * @param text
     * @return
     */
    private ScanResult textScan(String text) {
        //没有文本内容直接合格
        if (StringUtils.isEmpty(text)) {
            return new ScanResult("合格", 1, Collections.emptyList());
        }
        //有文本内容就审核
        return baiduAiTemplate.textScan(text);
    }


    /**
     * 调用百度AI做图片审核
     *
     * @param images url的集合
     * @return
     */
    private ScanResult imagesScan(List<String> images) {
        //1.如果一个图片都没有不用审核
        if (CollectionUtils.isEmpty(images)) {
            return new ScanResult("合格", 1, Collections.emptyList());
        }

        //有图片内容就审核  用字节数组
        /*for (String imageUrl : images) {
            //用图片url去minio中下载字节
            byte[] bytes = minIOTemplate.downLoadFile(imageUrl);
            //用字节去掉baiduAi审核
            ScanResult scanResult = baiduAiTemplate.imageScan(bytes);
            //判断 如果所有的  scanResult.conclusionType 等于1, 就通过了
            if (scanResult.getConclusionType() !=1){
                return new  ScanResult("不合格",2,list);
            } ......
        }*/

        //stream 流来做
        //1.遍历图片url调用百度AI进行审核
        List<ScanResult> resultList = images.stream()
                //用图片url去minio中下载字节
                .map(imageUrl -> minIOTemplate.downLoadFile(imageUrl))
                //用字节去掉baiduAi审核
                .map(bytes -> baiduAiTemplate.imageScan(bytes))
                .collect(Collectors.toList());
        //2.判断审核是否通过  --conclusionType 全部是1 就通过, --有任何一个不为1 就不通过
        //审核结果类型, 可取值: 1.合规 , 2.不合规 ,3.疑似 , 4.审核失败   allMatch匹配所有
        //审核通过
        if (resultList.stream().allMatch(scanResult -> scanResult.getConclusionType() == 1)) {
            return new ScanResult("合格", 1, Collections.emptyList());
        }
        //审核结果不合格
        if (resultList.stream().anyMatch(scanResult -> scanResult.getConclusionType() == 2)) {
            return new ScanResult("不合格", 2, Arrays.asList("图片中包含不合规信息"));
        }
        //审核结果疑似
        if (resultList.stream().anyMatch(scanResult -> scanResult.getConclusionType() == 3)) {
            return new ScanResult("疑似", 3, Arrays.asList("审核信息疑似"));
        }
        //审核结果失败
        return new ScanResult("失败", 4, Arrays.asList("审核信息失败"));


    }


    /**
     * 更新文章审核状态
     *
     * @param wmNews
     * @param scanResult 审核结果类型, 可取值: 1.合规 , 2.不合规 ,3.疑似 , 4.审核失败
     */
    private void updateWmNewStatus(WmNews wmNews, ScanResult scanResult) {

        //判断审核结果状态  1. 2. 3 .4
        Integer conclusionType = scanResult.getConclusionType();
        //conclusionType 的值等于1吗?等于status就是8(审核通过) ... 不等于就是(审核失败) 用:分开
        Integer status = conclusionType == 1 ? 8 : conclusionType == 2 ? 2 : conclusionType == 3 ? 3 : 2;
        //更新文章状态到数据库
        wmNews.setStatus(status);
        wmNews.setReason(StringUtils.join(scanResult.getWords(), ","));
        wmNewsMapper.updateById(wmNews);
    }


    /**
     * 自定义敏感词审核
     *
     * @param text
     * @return
     */
    private ScanResult sensitiveWordScan(String text) {
        //用ApplicationRunner 监听器 来实现 实现这个接口就可以了
        /*//从数据库中查询敏感词列表
        List<WmSensitive> sensitiveList = wmSensitiveMapper.selectList(Wrappers.emptyWrapper());
        if(CollectionUtils.isEmpty(sensitiveList)){ //数据库无敏感词, 不用审直接合格
            return new ScanResult("合格",1,Collections.emptyList());
        }

        List<String> words = sensitiveList.stream().map(sensitive -> sensitive.getSensitives()).collect(Collectors.toList());
        //1.初始化DFA数据结构
        SensitiveWordUtil.initMap(words);*/
        //2.判断文件中是否包含敏感词
        Map<String, Integer> result = SensitiveWordUtil.matchWords(text);  //病毒: 2

        if (CollectionUtils.isEmpty(result)) {  //如果集合为空, 就是文档不包含敏感字
            return new ScanResult("合格", 1, Collections.emptyList());
        }
        List<String> list = result.keySet().stream().collect(Collectors.toList());
        return new ScanResult("不合格", 2, list);

    }

    /**
     * 通过feign 发布(保存)自媒体文章到APP端(文章服务)
     *
     * @param wmNews
     */
    private void saveApArticle(WmNews wmNews) {
        //1.封装文章信息   对比者2个类的属性wmNews 和apArticleDto
        ApArticleDto apArticleDto = new ApArticleDto();
        BeanUtils.copyProperties(wmNews, apArticleDto, "id");
        // newsId authorId outhorName channelName Layout
        apArticleDto.setNewsId(wmNews.getId());
        //封装作者信息
        apArticleDto.setAuthorId(wmNews.getUserId());
        WmUser wmUser = wmUserMapper.selectById(wmNews.getUserId());
        if (wmUser != null) {
            apArticleDto.setAuthorName(wmUser.getNickname());
        }
        //封装频道信息
        WmChannel wmChannel = wmChannelMapper.selectById(wmNews.getChannelId());
        if (wmChannel != null){
            apArticleDto.setChannelName(wmChannel.getName());
        }
        apArticleDto.setLayout(wmUser.getType());

        //2.远程调用 把自媒体的文章保存到文章服务中
        ResponseResult<String> result = apArticleFeignClient.saveApArticle(apArticleDto);

        //3.调用失败 抛异常
        if (result.getCode() !=200){
            throw new CustomException(AppHttpCodeEnum.SERVER_ERROR);
        }
    }

    /**
     * 审核服务完成后, 调用延迟任务服务 ,实现文章自动发布
     * @param wmNews
     */
    private void wmNewsAutoPublish(WmNews wmNews) {
        TaskDto taskDto = new TaskDto();
        taskDto.setTaskType(TaskTypeEnum.NEWS_SCAN_TIME.getTaskType());
        taskDto.setPriority(TaskTypeEnum.NEWS_SCAN_TIME.getPriority());
        taskDto.setExecuteTime(wmNews.getPublishTime().getTime());

        WmNews param = new WmNews();
        param.setId(wmNews.getId());
        //再把对象序列化为byte 直接网上找工具类 把java对象序列化字节
        taskDto.setParameters(ProtostuffUtil.serialize(param));

        ResponseResult<Long> result = scheduleFeignClient.addTask(taskDto);
        if (result.getCode() != 200||result.getData() ==null){
            throw new CustomException(AppHttpCodeEnum.SERVER_ERROR);
        }
    }

}
