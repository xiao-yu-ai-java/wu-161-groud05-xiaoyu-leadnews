package com.heima.scan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.wemedia.pojo.WmSensitive;


/**
 * 自媒体敏感词表(WmSensitive)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-16 17:55:08
 */
public interface WmSensitiveMapper extends BaseMapper<WmSensitive> {

}

