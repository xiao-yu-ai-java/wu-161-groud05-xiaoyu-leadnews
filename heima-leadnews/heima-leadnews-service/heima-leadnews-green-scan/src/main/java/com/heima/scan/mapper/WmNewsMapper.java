package com.heima.scan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.wemedia.pojo.WmNews;
import org.springframework.stereotype.Repository;

/**
 * 自媒体文章表(WmNews)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
@Repository
public interface WmNewsMapper extends BaseMapper<WmNews> {

}

