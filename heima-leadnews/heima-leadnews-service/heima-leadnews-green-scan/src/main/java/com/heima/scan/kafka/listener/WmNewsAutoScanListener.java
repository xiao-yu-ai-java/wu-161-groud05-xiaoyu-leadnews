package com.heima.scan.kafka.listener;


import com.heima.model.common.wemedia.pojo.WmNews;
import com.heima.scan.mapper.WmNewsMapper;
import com.heima.scan.service.WmNewsAutoScanService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * kafka 异步调用避免用户(虚假)等待, 避免后台压力大  消息中间件
 * 监听器也是spring中的一个组件 所以要加注解@Component
 * 监听哪一个主题(队列), 自定意组
 *
 *自媒体服务保存文章到Wmnews表中, 就开始用kafka 异步发送消息, 自动扫描类就可以监听到, 开始检查
 */
@Component
@Slf4j
public class WmNewsAutoScanListener {


    @Resource
    private WmNewsAutoScanService wmNewsAutoScanService;

    @Resource
    private WmNewsMapper wmNewsMapper;


    @KafkaListener(topics = "KafkaMessageConstants.WM_NEWS_AUTO_SCAN_TOPIC",groupId = "wmNewsAutoScan")
    public void wmNewsAutoScan(ConsumerRecord<String,String> record){
        log.info("接收到自媒体自动审核消息, 消息内容:{}",record.value());

        String id = record.value();  //1.传过来的是id  key为null, value:文章id
        //参数条件
        WmNews wmNews = wmNewsMapper.selectById(Long.valueOf(id));
        if (wmNews !=null){
            wmNewsAutoScanService.newsAutoScan(wmNews);   //2.调用审核业务完成文章的审核
        }
        log.info("自媒体文章自动审核完成,文章id:{}",record.value());

    }
}
