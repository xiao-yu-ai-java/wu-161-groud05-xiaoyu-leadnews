package com.heima.scan.service;

import com.heima.model.common.wemedia.pojo.WmNews;
import com.heima.scan.mapper.WmNewsMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class WmNewsAutoScanServiceTest {

    @Resource
    private WmNewsAutoScanService wmNewsAutoScanService;

    @Resource
    private WmNewsMapper wmNewsMapper;

    @Test
    void newsAutoScan() {
        WmNews wmNews = wmNewsMapper.selectById(5);
        wmNewsAutoScanService.newsAutoScan(wmNews);
    }
}