package com.heima.scan;

import com.heima.baidu.BaiduAiTemplate;
import com.heima.baidu.pojo.ScanResult;
import com.heima.minio.MinIOTemplate;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest
public class GreenScanTest {
    @Autowired
    private BaiduAiTemplate baiduAiTemplate;

    @Autowired
    private MinIOTemplate minIOTemplate;

    @Test
    public void testTextScan() {
        ScanResult scanResult = baiduAiTemplate.textScan("我是个好人, 买卖冰毒是违法的!");
        System.out.println(scanResult);
    }

    @Test
    public void testImageScan() throws Exception {
        String url = "http://192.168.200.130:9000/leadnews/2023/06/12/1.jpg";
        byte[] bytes = minIOTemplate.downLoadFile(url);
        ScanResult scanResult = baiduAiTemplate.imageScan(url);
        System.out.println(scanResult);

      /*  FileInputStream fis = new FileInputStream("D:\\1.jpg");  //流
        byte[] bytes = new byte[fis.available()];   //字节
        fis.read(bytes);  //把输入流的东西读到字节中
        JSONObject res =  baiduAiTemplate.imageScan();
        System.out.println(res);*/
    }
}
