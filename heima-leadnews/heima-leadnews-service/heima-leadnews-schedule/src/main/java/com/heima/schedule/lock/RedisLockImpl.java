package com.heima.schedule.lock;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.ThreadUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class RedisLockImpl implements RedisLock{


    //构造器注入bean对象  属性必须要注入就用构造器注入
    private StringRedisTemplate stringRedisTemplate;
    private String uuid;
    private long timeout ;
    private TimeUnit unit;
    private String key;

    public RedisLockImpl(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
        this.uuid = uuid;
        this.timeout = timeout;
        this.unit = unit;
        this.key = key;
    }
    //搞一个线程局部变量对象 , 枷锁的过程要给一个唯一表示
    private ThreadLocal<String> tl = new ThreadLocal<>();


    @Override
    public boolean tryLock(String key, long timeout, TimeUnit unit) {
        //自己上锁, 自己解锁. 所以在redis中key的值为唯一的id, 解锁的时候用它验证
        //生成线程唯一id
        String uuid = UUID.randomUUID().toString();
        //将线程唯一id和当前线程绑定
        tl.set(uuid);

        //用reids设置分布式锁  加锁, 需要将持锁线程id和锁进行绑定
        Boolean isLock = stringRedisTemplate.opsForValue().setIfAbsent(key, uuid, timeout, unit);

        //对分布式锁进行续期操作  : 用while (true)一直循环, 最后代码（枷锁结果）不会执行  阻塞
        //用异步续期　：枷锁成功给客户返回枷锁成功，再搞个线程来执行续期
        if(isLock){
            //while (true){
            //    stringRedisTemplate.expire(key, timeout, unit);  //续期
            //}
           /*用异步续期, 线程来做*/
            new Thread(new UpdateLockTimeout(stringRedisTemplate,uuid,timeout,unit,key)) .start();


                /*@Override
                public void run() {
                    //获得当前线程id
                    Long threadId = Thread.currentThread().getId();
                    //redis中lock作为key uuid作为值
                    //再搞一个 uuid作为key thredid作为值 其他线程就可以获得这个线程id了   过期时间与分布式一样
                    stringRedisTemplate.opsForValue().set(uuid,threadId.toString(),timeout, unit);

                    //线程不销毁中断就一直续命
                    boolean interrupted = Thread.currentThread().isInterrupted();
                    while (!interrupted){
                        stringRedisTemplate.expire(key, timeout, unit);  //续期
                        try { //sleep 的异常要try一下
                            Thread.sleep(unit.toMillis(timeout/3));  //超时时间还剩3/1时, 就续命
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();*/
            //什么时候要结束线程呢? 释放锁的时候, 锁都释放了, 续命就没用了
        }

        return isLock;
    }

    @Override
    public void releaseLock(String key) {
        String uuid = stringRedisTemplate.opsForValue().get(key);
        //如果枷锁key中的值 与 线程中的值一样就可以去解锁
        if (StringUtils.equals(uuid,tl.get())){
            //解锁
            stringRedisTemplate.delete(key);
            //清空线程局部变量, 防止内存泄露
            tl.remove();
            //什么时候要结束线程呢? 释放锁的时候, 锁都释放了, 续命就没用了
            //删续命线程
            String threadId = stringRedisTemplate.opsForValue().get(uuid);

            Thread thread = ThreadUtils.findThreadById( Long.valueOf(threadId));  //寻找线程
            thread.interrupt();  //销毁线程

        }

    }
}
