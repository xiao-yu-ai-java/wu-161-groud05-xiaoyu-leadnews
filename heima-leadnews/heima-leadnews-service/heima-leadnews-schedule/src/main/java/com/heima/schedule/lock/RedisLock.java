package com.heima.schedule.lock;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public interface RedisLock {



    /**
     * 加锁操作
     *
     * @param key
     * @param timeout
     * @param unit
     * @return
     */
    boolean tryLock(String key, long timeout, TimeUnit unit);

    /**
     * 解锁操作
     *
     * @param key
     */
    void releaseLock(String key);

}