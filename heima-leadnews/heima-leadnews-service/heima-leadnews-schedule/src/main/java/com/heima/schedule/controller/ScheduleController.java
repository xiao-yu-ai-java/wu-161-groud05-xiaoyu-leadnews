package com.heima.schedule.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.schedule.dto.TaskDto;
import com.heima.model.common.schedule.pojo.TaskInfo;
import com.heima.schedule.service.ScheduleService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping(path = "/api/vi/task")
public class ScheduleController {

    @Resource
    private ScheduleService scheduleService;

    /**
     * 新增任务
     * @return
     */
    @PostMapping
    public ResponseResult<Long> addTask(@RequestBody TaskDto taskDto){
        Long taskId = scheduleService.addTask(taskDto);
        return ResponseResult.okResult(taskId);
    }

    /**
     * 取消任务
     *
     * @param taskId
     * @return
     */
    @DeleteMapping(path = "/{taskId}")
    public ResponseResult cancel(@PathVariable("taskId") Long taskId) {
        boolean cancel = scheduleService.cancelTask(taskId);
        return ResponseResult.okResult(cancel);
    }

    /**
     * 拉取任务消费
     *
     * @return
     */
    @GetMapping(path = "/poll/{taskType}/{priority}")
    public ResponseResult<TaskInfo> pollTask(@PathVariable("taskType") Integer taskType, @PathVariable("priority") Integer priority) {
        TaskInfo taskInfo = scheduleService.pollTask(taskType, priority);
        return ResponseResult.okResult(taskInfo);
    }
}
