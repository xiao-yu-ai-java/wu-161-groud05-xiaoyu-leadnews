package com.heima.schedule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.heima.model.common.schedule.pojo.TaskInfoLogs;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * (TaskInfoLogs)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-18 19:34:57
 */
@Repository
public interface TaskInfoLogsMapper extends BaseMapper<TaskInfoLogs> {

}

