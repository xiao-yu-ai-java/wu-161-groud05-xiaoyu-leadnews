package com.heima.schedule.service;

import com.heima.model.common.schedule.dto.TaskDto;
import com.heima.model.common.schedule.pojo.TaskInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Transactional(rollbackFor = Exception.class)
public interface ScheduleService {

    /**
     *发布任务
     */

    @Transactional(rollbackFor = Exception.class)   //加接口或方法都可以 开要开启
    public Long addTask(TaskDto taskDto);

    /**
     * 取消任务
     * @param taskId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean cancelTask(Long taskId);

    /**
     * 拉取任务消费
     * @param taskType
     * @param priority
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public TaskInfo pollTask(Integer taskType, Integer priority);
}
