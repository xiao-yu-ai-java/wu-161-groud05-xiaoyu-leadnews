package com.heima.schedule.lock;

import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.concurrent.TimeUnit;

public class UpdateLockTimeout implements Runnable{


    private StringRedisTemplate stringRedisTemplate;
    private String uuid;
    private long timeout ;
    private TimeUnit unit;
    private String key;

    public UpdateLockTimeout (StringRedisTemplate stringRedisTemplate, String uuid, long timeout, TimeUnit unit, String key) {
        this.stringRedisTemplate = stringRedisTemplate;
        this.uuid = uuid;
        this.timeout = timeout;
        this.unit = unit;
        this.key = key;
    }


    @Override
    public void run() {
        //获得当前线程id
        Long threadId = Thread.currentThread().getId();
        //redis中lock作为key uuid作为值
        //再搞一个 uuid作为key thredid作为值 其他线程就可以获得这个线程id了   过期时间与分布式一样
        stringRedisTemplate.opsForValue().set(uuid,threadId.toString(),timeout, unit);

        //线程不销毁中断就一直续命
        boolean interrupted = Thread.currentThread().isInterrupted();
        while (!interrupted){
            stringRedisTemplate.expire(key, timeout, unit);  //续期
            try { //sleep 的异常要try一下
                Thread.sleep(unit.toMillis(timeout/3));  //超时时间还剩3/1时, 就续命
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
