package com.heima.schedule.service.impl;


import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.common.constants.ScheduleConstants;
import com.heima.common.exception.CustomException;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.schedule.dto.TaskDto;
import com.heima.model.common.schedule.pojo.TaskInfo;
import com.heima.model.common.schedule.pojo.TaskInfoLogs;
import com.heima.schedule.mapper.TaskInfoLogsMapper;
import com.heima.schedule.mapper.TaskInfoMapper;
import com.heima.schedule.service.ScheduleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Resource
    private TaskInfoMapper taskInfoMapper;
    @Resource
    private TaskInfoLogsMapper taskInfoLogsMapper;

    @Resource
    private StringRedisTemplate stringRedisTemplate;
    /**
     * 发布任务  返回一个任务id
     * @param taskDto
     * @return
     */
    @Override
    public Long addTask(TaskDto taskDto) {

        //1.校验参数
        if (taskDto==null||taskDto.getTaskType()==null||taskDto.getPriority()==null||taskDto.getExecuteTime()<=0){
            throw new CustomException(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.保存任务数据到数据库
        TaskInfo taskInfo =  saveTaskToDb(taskDto);

        //3.保存任务数据到redis
        if (taskInfo != null){
           Integer isload =  saveTaskToRedis(taskInfo);

           if (isload==1){
               taskInfo.setIsLoad(1);
               taskInfoMapper.updateById(taskInfo);
           }
        }
        return taskInfo.getTaskId();
    }


    /**
     * 取消任务
     * @param taskId
     * @return
     */
    @Override
    public boolean cancelTask(Long taskId) {

        //1.检验参数
        if (taskId==null){
            throw new CustomException(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.从数据库删除任务
          TaskInfo taskInfo = removeTaskFromDb(taskId);
        //3.从redis中删除任务
       if (taskInfo != null){
          return removeTaskFromRedis(taskInfo);
       }
        return false;
    }


    /**
     * 拉取任务消费
     * @param taskType
     * @param priority
     * @return
     */
    @Override
    public TaskInfo pollTask(Integer taskType, Integer priority) {
        //1.参数校验
        if (taskType==null||priority==null){
            throw new CustomException(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.从redis的list(立刻马上需要执行的任务)集合头部获取任务数据
        String taskId = pollTaskFromRedis(taskType, priority);

        if (StringUtils.isEmpty(taskId)){
            throw new CustomException(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //查询任务详情到数据库删除任务数据
        TaskInfo taskInfo = getAndRemoveTaskInfo(taskId);
        return taskInfo;
    }


//    /**
//     * 从数据库中加载任务到redis实现数据预加载
//     */
//    @Scheduled(fixedDelay = 5000)
//    public void loadTaskFromDb(){
//     //1.查询数据库中到了预加载时间,但是还没有被加载的数据
//        //预加载时间(当前时间(执行时间)加5分钟)  用DateUtil
//        long preloadTime = DateUtil.offsetMinute(new Date(), 5).getTime();
//        LambdaQueryWrapper<TaskInfo> wrapper = Wrappers.<TaskInfo>lambdaQuery().eq(TaskInfo::getIsLoad, 0).le(TaskInfo::getExecuteTime, preloadTime).ge(TaskInfo::getExecuteTime, System.currentTimeMillis());
//        List<TaskInfo> taskInfoList = taskInfoMapper.selectList(wrapper);
//        if (CollectionUtils.isEmpty(taskInfoList)){
//            return;
//        }
//        //2.保存预加载数据到redis的zset集合
//        taskInfoList.stream().forEach(taskInfo -> {
//            String key = ScheduleConstants.TASK_FUTURE+taskInfo.getTaskType()+"_"+taskInfo.getPriority();
//            //zset 根据执行时间来排序, 执行时间小的排到前面
//            stringRedisTemplate.opsForZSet().add(key,taskInfo.getTaskId().toString(),taskInfo.getExecuteTime().getTime());
//
//            //3.更新任务的加载状态
//            taskInfo.setIsLoad(1);
//            taskInfoMapper.updateById(taskInfo);
//        });
//
//    }



    //+++++++++++++++++++++++++++++++++++++++++++++++++++++


    /**
     * 保存任务数据到数据库
     * @param taskDto
     * @return
     */
    private TaskInfo saveTaskToDb(TaskDto taskDto) {
        //1.保存任务数据到task_info表
        TaskInfo taskInfo = new TaskInfo();
        BeanUtils.copyProperties(taskDto,taskInfo,"taskId");
        //executeTime执行时间要有Long转变为date
        taskInfo.setExecuteTime(new Date(taskDto.getExecuteTime()));

        taskInfoMapper.insert(taskInfo);
        //2.保存任务数据task_info_logs表
        TaskInfoLogs taskInfoLogs = new TaskInfoLogs();
        BeanUtils.copyProperties(taskInfo,taskInfoLogs);
        taskInfoLogs.setVersion(0);
        taskInfoLogs.setStatus(0);
        taskInfoLogsMapper.insert(taskInfoLogs);

        return taskInfo;
    }


    /**
     * 保存任务数据到redis 左进右出
     * @param taskInfo
     */
    private Integer saveTaskToRedis(TaskInfo taskInfo) {
        Integer isLoad = 0;

        //当前时间, 任务执行时间, 预加载时间(执行时间加5分钟)
        long executeTime = taskInfo.getExecuteTime().getTime();  //任务执行时间
        //预加载时间(当前时间(执行时间)加5分钟)  用DateUtil
        long preloadTime = DateUtil.offsetMinute(new Date(), 5).getTime();

        //1.如果任务执行时间小于当前时间(任务需要立刻执行), 添加任务数据到redis中list集合 左进右出
        if (executeTime<=System.currentTimeMillis()){
            //List集合 key的规则 :前缀+任务类型+优先级
            String key = ScheduleConstants.TASK_TOPIC+taskInfo.getTaskType()+"_"+taskInfo.getPriority();
            stringRedisTemplate.opsForList().leftPush(key,taskInfo.getTaskId().toString());  //只用任务id
            isLoad = 1;
        }else if (executeTime<=preloadTime){
            //2.如果 当前时间 <任务执行时间 <预加载时间 (任务即将执行), 添加任务数据到redis中的zset集合(任务数据预加载)
            //Zset集合 key的规则 :前缀+任务类型+优先级
            String key = ScheduleConstants.TASK_FUTURE+taskInfo.getTaskType()+"_"+taskInfo.getPriority();
            //zset 根据执行时间来排序, 执行时间小的排到前面
            stringRedisTemplate.opsForZSet().add(key,taskInfo.getTaskId().toString(),taskInfo.getExecuteTime().getTime());
            isLoad = 1;
        }
        return isLoad;
    }


    /**
     * 从数据库删除任务
     * @param taskId
     * @return
     */
    private TaskInfo removeTaskFromDb(Long taskId) {
        //1. 根据id删除数据库的中任务
        //根据id查询任务详情
        TaskInfo taskInfo = taskInfoMapper.selectById(taskId);
        if (taskInfo==null){
            throw new CustomException(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        taskInfoMapper.deleteById(taskId);

        //2. 修改任务日志表中的状态为取消
        TaskInfoLogs taskInfoLogs = taskInfoLogsMapper.selectById(taskId);
        taskInfoLogs.setStatus(ScheduleConstants.CANCELLED);
        taskInfoLogsMapper.updateById(taskInfoLogs);

        return taskInfo;
    }

    /**
     * 从redis中删除任务
     * @param taskInfo
     * @return
     */
    private boolean removeTaskFromRedis(TaskInfo taskInfo) {
        //当前时间, 任务执行时间, 预加载时间(执行时间加5分钟)
        long executeTime = taskInfo.getExecuteTime().getTime();  //任务执行时间
        //预加载时间(当前时间(执行时间)加5分钟)  用DateUtil
        long preloadTime = DateUtil.offsetMinute(new Date(), 5).getTime();

        //1.如果任务执行时间小于当前时间(任务需要立刻执行), 从redis中list集合删除数据
        if (executeTime <= System.currentTimeMillis()){
            //List集合 key的规则 :前缀+任务类型+优先级
            String key = ScheduleConstants.TASK_TOPIC+taskInfo.getTaskType()+"_"+taskInfo.getPriority();
            stringRedisTemplate.opsForList().remove(key,1,taskInfo.getTaskId().toString());   // key 删除一个, 值:任务id

        }else if (executeTime <= preloadTime){
            //2.如果 当前时间 <任务执行时间 <预加载时间 (任务即将执行), 从redis中的zset集合中删除任务
            //Zset集合 key的规则 :前缀+任务类型+优先级
            String key = ScheduleConstants.TASK_FUTURE+taskInfo.getTaskType()+"_"+taskInfo.getPriority();
            //zset 根据执行时间来排序, 执行时间小的排到前面
            stringRedisTemplate.opsForZSet().remove(key,taskInfo.getTaskId().toString());
        }
        return true;
    }

    /**
     * 从redis中拉取任务
     * @param taskType
     * @param priority
     * @return
     */
    private String pollTaskFromRedis(Integer taskType, Integer priority) {
        String key = ScheduleConstants.TASK_TOPIC+ taskType +"_"+ priority;
        String taskId = stringRedisTemplate.opsForList().rightPop(key);  //reids中list中查
        return taskId;
    }

    /**
     * 查询任务详情到数据库修改
     * @param taskId
     * @return
     */
    private TaskInfo getAndRemoveTaskInfo(String taskId) {
        TaskInfo taskInfo = taskInfoMapper.selectById(taskId);
        if (taskInfo == null){
            throw new CustomException(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //3.从数据库中删除任务, 修改任务日志的状态
        taskInfoMapper.deleteById(taskId);
        //修改日志表中的记录
        TaskInfoLogs taskInfoLogs = taskInfoLogsMapper.selectById(taskId);
        taskInfoLogs.setStatus(ScheduleConstants.EXECUTED);
        taskInfoLogsMapper.updateById(taskInfoLogs);
        return taskInfo;
    }

}
