package com.heima.schedule.task;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.common.constants.ScheduleConstants;
import com.heima.model.common.schedule.pojo.TaskInfo;
import com.heima.schedule.lock.RedisLock;
import com.heima.schedule.mapper.TaskInfoMapper;
import com.heima.schedule.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class ScheduleTask {

    @Resource
    private RedisUtil redisUtil;


    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private TaskInfoMapper taskInfoMapper;

    @Resource
    private RedisLock redisLock;

    @Resource
    private RedissonClient redissonClient;

    //@Scheduled(cron = "0/5 * * * * ?")
    @Scheduled(fixedDelay = 5000)
    public void refresh() throws InterruptedException {
        log.info("定时刷新任务开始执行了");
        //加锁----->
        // 操作redis获取分布式锁  setIfAbsent 设置key(任意,在redis中的表名)  不存在返回1 存在就返回0  值待会有大用, 现在随意
       // Boolean isLock = stringRedisTemplate.opsForValue().setIfAbsent("refresh_lock", "1", Duration.ofSeconds(30));

        //boolean isLock = redisLock.tryLock("refresh_lock", 30, TimeUnit.SECONDS);

        //Redission 解决资源竞争问题
        RLock rLock = redissonClient.getLock("refresh_lock");
        boolean isLock = rLock.tryLock(1, TimeUnit.SECONDS);
        if (!isLock){ //枷锁失败, 返回方法结束, 下面逻辑不执行
            log.info("获取分布式锁失败");
           return;
        }
        log.info("获取分布式锁成功, 开始执行定时刷新业务");

        try {
            //1.找到redis中所有的预加载任务集合
            Set<String> keys = redisUtil.scan("task_future_*");

            //2.从每一个预加载集合中获取执行时间任务 遍历
            keys.forEach(task_future_key ->{
               //获取zset集合中到了执行时间的任务列表 获取执行时间在0--系统当前时间的集合
                Set<String> taskIds = stringRedisTemplate.opsForZSet().rangeByScore(task_future_key, 0, System.currentTimeMillis());

                //3.将任务数据添加到list集合排队执行  替换key放到list中  task_future_3_3 --->task_topic_3_3
                String task_topic_key = task_future_key.replace(ScheduleConstants.TASK_FUTURE, ScheduleConstants.TASK_TOPIC);
                //list集合得到数据 ,保存到这样的key中task_topic_key, 但是zset的key叫task_future_key , 所以要替换
    //            stringRedisTemplate.opsForList().leftPushAll(task_topic_key,taskIds);
    //            //从zset集合中删除任务数据
    //            stringRedisTemplate.opsForZSet().remove(task_future_key,taskIds.toArray());

                redisUtil.refreshWithPipeline(task_future_key,task_topic_key,taskIds);
                //能刷新能删
            });
        } finally {
            //释放分布式锁--->其他线程就可以用了
            log.info("业务代码执行完成释放分布式锁----------");
           // stringRedisTemplate.delete("refresh_lock");
           // redisLock.releaseLock("refresh_lock");
            if (isLock){
                rLock.unlock();
            }
        }
    }


    /**
     * 从数据库中加载任务到redis实现数据预加载
     */
    @Scheduled(fixedDelay = 5000)
    public void loadTaskFromDb(){
        //1.查询数据库中到了预加载时间,但是还没有被加载的数据
        //预加载时间(当前时间(执行时间)加5分钟)  用DateUtil
        long preloadTime = DateUtil.offsetMinute(new Date(), 5).getTime();
        LambdaQueryWrapper<TaskInfo> wrapper = Wrappers.<TaskInfo>lambdaQuery().eq(TaskInfo::getIsLoad, 0).le(TaskInfo::getExecuteTime, preloadTime).ge(TaskInfo::getExecuteTime, System.currentTimeMillis());
        List<TaskInfo> taskInfoList = taskInfoMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(taskInfoList)){

            return;
        }
        //2.保存预加载数据到redis的zset集合
        taskInfoList.stream().forEach(taskInfo -> {
            String key = ScheduleConstants.TASK_FUTURE+taskInfo.getTaskType()+"_"+taskInfo.getPriority();
            //zset 根据执行时间来排序, 执行时间小的排到前面
            stringRedisTemplate.opsForZSet().add(key,taskInfo.getTaskId().toString(),taskInfo.getExecuteTime().getTime());

            //3.更新任务的加载状态
            taskInfo.setIsLoad(1);
            taskInfoMapper.updateById(taskInfo);
        });

    }
}
