package com.heima.schedule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.heima.model.common.schedule.pojo.TaskInfo;
import org.springframework.stereotype.Repository;

/**
 * (TaskInfo)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-18 19:34:55
 */
@Repository
public interface TaskInfoMapper extends BaseMapper<TaskInfo> {

}

