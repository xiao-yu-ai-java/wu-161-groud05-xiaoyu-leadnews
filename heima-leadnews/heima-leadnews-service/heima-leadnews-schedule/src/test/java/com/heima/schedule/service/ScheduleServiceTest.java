package com.heima.schedule.service;

import cn.hutool.core.date.DateUtil;
import com.heima.model.common.schedule.dto.TaskDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ScheduleServiceTest {

    @Resource
    private ScheduleService scheduleService;

    @Test
    void addTask() {
        TaskDto taskDto = new TaskDto();
        taskDto.setTaskType(1);
        taskDto.setPriority(1);
        taskDto.setExecuteTime(System.currentTimeMillis());
        taskDto.setParameters("aa".getBytes(StandardCharsets.UTF_8));
        Long taskId = scheduleService.addTask(taskDto);
        System.out.println(taskId);
    }
}