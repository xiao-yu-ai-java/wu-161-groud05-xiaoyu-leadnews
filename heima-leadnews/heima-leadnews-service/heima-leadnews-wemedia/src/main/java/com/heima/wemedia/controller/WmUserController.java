package com.heima.wemedia.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.wemedia.pojo.WmUser;
import com.heima.wemedia.service.WmUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/v1/user")  //服务内部调用, 不用和前端对接, 自己定
@Api(value = "自媒体用户管理",tags = "自媒体")
public class WmUserController {


    @Resource
    private WmUserService wmUserService;

    /**
     * 这个接口后面要被用户服务调用
     * 保存自媒体用户信息
     * 新增用post
     * @param wmUser
     * @return
     */
    @PostMapping("/save")
    @ApiOperation(value = "保存自媒体用户信息")
    public ResponseResult save(@RequestBody WmUser wmUser){

        return  wmUserService.save(wmUser);
    }

}
