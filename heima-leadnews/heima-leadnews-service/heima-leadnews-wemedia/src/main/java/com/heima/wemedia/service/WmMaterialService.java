package com.heima.wemedia.service;

import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.wemedia.dto.WmMaterialDto;
import com.heima.model.common.wemedia.pojo.WmMaterial;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface WmMaterialService {
    //素材上传
    ResponseResult<WmMaterial> upload(MultipartFile multipartFile) throws IOException;

    /**
     * 素材分页查询
     * @param wmMaterialDto :page size isCollection
     * @return
     */
    ResponseResult queryList(WmMaterialDto wmMaterialDto);

    /**
     * 删除素材
     *
     * @param id
     * @return
     */
    ResponseResult deleteById(Integer id);

    /**
     * 收藏素材
     *
     * @param id
     * @return
     */
    ResponseResult collect(Long id);

    /**
     * 取消收藏素材
     *
     * @param id
     * @return
     */
    ResponseResult cancel(Long id);
}
