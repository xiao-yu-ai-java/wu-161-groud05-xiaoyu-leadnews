package com.heima.wemedia.controller;



import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.wemedia.dto.ChannelDto;
import com.heima.model.common.wemedia.dto.PageChannelDto;
import com.heima.model.common.wemedia.pojo.WmChannel;
import com.heima.wemedia.service.WmChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/**
 * 自媒体频道表(WmChannel)表控制层
 *
 * @author makejava
 * @since 2023-06-14 11:30:28
 */
@RestController
@RequestMapping("/api/v1/channel")
@Api(value = "自媒体频道相关api",tags = "自媒体")

public class WmChannelController  {
    /**
     * 服务对象
     */
    @Resource
    private WmChannelService wmChannelService;

    /**
     * 查询频道列表接口
     * @return
     */
    @ApiOperation(value = "查询频道列表接口")
    @GetMapping("/channels")
    public ResponseResult<List<WmChannel>> channels(){
    return wmChannelService.channels();
    }

    /**
     * 频道名称模糊分页查询
     *
     * @param pageChannelDto
     * @return
     */
    @PostMapping("/list")
    public ResponseResult<List<WmChannel>> list(@RequestBody PageChannelDto pageChannelDto) {
        return wmChannelService.pageList(pageChannelDto);
    }

    /**
     * 删除频道
     * @param id
     * @return
     */
    @GetMapping("/del/{id}")
    public ResponseResult deleteById(@PathVariable Long id) {
        return wmChannelService.deleteChannel(id);
    }

    /**
     * 添加频道
     * @param channelDto
     * @return
     */
    @PostMapping("/save")
    public ResponseResult save(@RequestBody ChannelDto channelDto) {
        return wmChannelService.saveChannel(channelDto);
    }

    /**
     * 修改频道
     * @param wmChannel
     * @return
     */
    @PostMapping("/update")
    public ResponseResult update(@RequestBody WmChannel wmChannel) {
        return wmChannelService.updateChannel(wmChannel);
    }



}

