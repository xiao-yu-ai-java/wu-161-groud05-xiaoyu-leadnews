package com.heima.wemedia.controller;



import com.heima.model.common.admin.dto.NewsAuthDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.dtos.WmNewsDownOrUpDto;
import com.heima.model.common.wemedia.dto.WmNewsDto;
import com.heima.model.common.wemedia.dto.WmNewsPageReqDto;
import com.heima.model.common.wemedia.pojo.WmNews;
import com.heima.wemedia.service.WmNewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * 自媒体文章表(WmNews)表控制层
 *
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
@RestController
@RequestMapping("/api/v1/news")
@Api(value = "自媒体文章发布的相关api",tags = "自媒体")
public class WmNewsController  {
    /**
     * 服务对象
     */
    @Resource
    private WmNewsService wmNewsService;


    /**
     *
     * 自媒体文章发布(新增 ,和修改一起了)
     * @param wmNewsDto
     * @return
     */
    @ApiOperation("自媒体文章发布(新增和修改一起了, 有newId就是下修改, 没有就是新增)")
    @PostMapping("/submit")
    public ResponseResult submit(@RequestBody WmNewsDto wmNewsDto){

        return  wmNewsService.submit(wmNewsDto);
    }

    /**
     *查询文章列表(分页查询)
     */
    @PostMapping("/list")
    @ApiOperation(value = "文章分页查询")
    public ResponseResult list(@RequestBody
            WmNewsPageReqDto wmNewsPageReqDto){
        return wmNewsService.queryList(wmNewsPageReqDto);
    }


    /**
     * 根据ID查询详情
     * @param id
     * @return
     */
    @ApiOperation(value = "根据文章id查询文章, 用于修改回显")
    @GetMapping(path = "/one/{id}")
    public ResponseResult<WmNews> findById(@PathVariable("id") Long id){
        return wmNewsService.findById(id);
    }


    /**
     * 根据文章ID删除文章
     * @param newsId
     * @return
     */
    @ApiOperation(value = "根据文章ID删除文章")
    @GetMapping(path = "/del_news/{newsId}")
    public ResponseResult deleteById(@PathVariable("newsId") Long newsId) {
        return wmNewsService.deleteById(newsId);
    }

    /**
     * 文章的上下架
     * @param wmNewsDownOrUpDto
     * @return
     */
    @PostMapping("/down_or_up")
    public ResponseResult downOrUp(@RequestBody WmNewsDownOrUpDto wmNewsDownOrUpDto){
        return wmNewsService.downOrUp(wmNewsDownOrUpDto);
    }





    /**
     *查询自媒体文章列表
     * @param dto
     * @return
     */
    @PostMapping("/list_vo")
    public ResponseResult findList(@RequestBody NewsAuthDto dto){
     return wmNewsService.findList(dto);
    }


    /**
     * 查询自媒体文章详情
     * @param id
     * @return
     */
    @GetMapping("/one_vo/{id}")
    public ResponseResult findWmNewsVo(@PathVariable Integer id){
        return wmNewsService.findWmNewsVo(id);
    }




}

