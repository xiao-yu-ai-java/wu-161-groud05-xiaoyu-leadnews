package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.wemedia.pojo.WmNewsMaterial;

/**
 * 文章素材关联表(WmNewsMaterial)表服务接口
 *
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
public interface WmNewsMaterialService extends IService<WmNewsMaterial> {

}

