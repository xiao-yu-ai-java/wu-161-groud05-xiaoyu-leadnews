package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserInfoThreadLocalUtil;
import com.heima.minio.MinIOTemplate;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.wemedia.dto.WmMaterialDto;
import com.heima.model.common.wemedia.pojo.WmMaterial;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.mapper.WmUserMapper;
import com.heima.wemedia.service.WmMaterialService;
import com.heima.wemedia.service.WmUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;


@Service
public class WmMaterialServiceImpl implements WmMaterialService {

    /**
     * 素材上传
     *
     * @param multipartFile 图片路径
     * @return
     */

    @Resource
    private MinIOTemplate minIOTemplate;

    @Resource
    private WmMaterialMapper wmMaterialMapper;

    @Resource
    private WmUserService wmUserService;

    @Override
    public ResponseResult<WmMaterial> upload(MultipartFile multipartFile) throws IOException {

        /*1.检验参数 看是否为空
          2. 获取登录用户id
          3.用minio上传文件, 反回url放到对象中
          4.封装参数
          5.保存对象数据到数据库
          6.返回数据*/

        //1.检验参数 看是否为空
        if (multipartFile == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE, "请选择素材!");
        }
        //2. 获取登录用户id
        UserInfo userInfo = UserInfoThreadLocalUtil.get();
        Long userId = userInfo.getUserId();

        //3.用minio上传文件, 反回url放到对象中
            //文件名称创建一个唯一的
        String uuid = UUID.randomUUID().toString();
        String filename = multipartFile.getOriginalFilename();  //aaa.jpg
        String suffix = filename.substring(filename.lastIndexOf("."));//.jpg
         filename = uuid + suffix;
        String url = minIOTemplate.uploadImgFile("", filename, multipartFile.getInputStream());  //返回url

        //4.封装参数
        WmMaterial wmMaterial = new WmMaterial();
        wmMaterial.setUrl(url);
        wmMaterial.setType(1);
        wmMaterial.setIsCollection(0);
        wmMaterial.setUserId(userId.intValue());
        wmMaterial.setCreatedTime(new Date());
        wmMaterial.setUpdatedTime(new Date());

        //5.保存对象数据到数据库
        wmMaterialMapper.insert(wmMaterial);
        //6.返回数据

        return ResponseResult.okResult(wmMaterial);
    }


    /**
     * 素材分页查询
     * @param wmMaterialDto :page size isCollection
     * @return
     */
    @Override    //ResponseResult 返回值用这个类型主要是为了让返回错误信息
    public ResponseResult queryList(WmMaterialDto wmMaterialDto) {

        //1.检验参数
        if (wmMaterialDto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //分页参数校验
        wmMaterialDto.checkParam();

        //2.定义分页
        Page<WmMaterial> page = new Page<>(wmMaterialDto.getPage(), wmMaterialDto.getSize());  //定义分页参数

        LambdaQueryWrapper<WmMaterial> wrapper = Wrappers.<WmMaterial>lambdaQuery();  //定义查询参数
        wrapper.eq(WmMaterial::getUserId,UserInfoThreadLocalUtil.get().getUserId());

        if (wmMaterialDto.getIsCollection() != null && wmMaterialDto.getIsCollection() == 1){
            wrapper.eq( WmMaterial::getIsCollection,wmMaterialDto.getIsCollection());
        }

        //3.调用mapper查询 返回值直接在page里
         wmMaterialMapper.selectPage(page, wrapper);

        //4.返回分页对象   Records分页的数据
        PageResponseResult result = new PageResponseResult(((Long)page.getCurrent()).intValue(),((Long)page.getSize()).intValue(),((Long)page.getTotal()).intValue());
        result.setData(page.getRecords());    //数据列表
        return result;
    }


    /**
     * 删除素材
     * @param id
     * @return
     */
    @Override
    public ResponseResult deleteById(Integer id) {

        //参数失效
        if (id==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        WmMaterial wmMaterial = wmMaterialMapper.selectById(id);
        //数据不存在
        if (wmMaterial ==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        int num = wmMaterialMapper.deleteById(id);
        String s = num + "";
        //文件删除失败
        if (StringUtils.isEmpty(s)){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"文件删除失败");
        }
        //操作成功
       return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }


    /**
     * 收藏素材
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult collect(Long id) {


        WmMaterial wmMaterial = new WmMaterial();
        wmMaterial.setId(id);
        wmMaterial.setIsCollection(1);

        if (id==null){
            //参数不存在
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        wmMaterialMapper.updateById(wmMaterial);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 取消收藏素材
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult cancel(Long id) {
        WmMaterial wmMaterial = new WmMaterial();
        wmMaterial.setId(id);
        wmMaterial.setIsCollection(0);

        if (id==null){
            //参数不存在
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        wmMaterialMapper.updateById(wmMaterial);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }


}
