package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.wemedia.pojo.WmSensitive;
import org.springframework.stereotype.Repository;

@Repository
public interface WmSensitiveMapper extends BaseMapper<WmSensitive> {
}
