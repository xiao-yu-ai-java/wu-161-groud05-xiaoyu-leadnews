package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.wemedia.pojo.WmNewsMaterial;
import org.springframework.stereotype.Repository;

/**
 * 文章素材关联表(WmNewsMaterial)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
@Repository
public interface WmNewsMaterialMapper extends BaseMapper<WmNewsMaterial> {

}

