package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.pagehelper.Page;
import com.heima.model.common.wemedia.pojo.WmNews;
import com.heima.model.common.wemedia.vo.WmNewsVo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 自媒体文章表(WmNews)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
@Repository
public interface WmNewsMapper extends BaseMapper<WmNews> {


    Page<WmNewsVo> list();
}

