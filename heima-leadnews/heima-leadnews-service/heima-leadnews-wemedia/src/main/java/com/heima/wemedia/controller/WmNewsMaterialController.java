package com.heima.wemedia.controller;



import com.heima.wemedia.service.WmNewsMaterialService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * 文章素材关联表(WmNewsMaterial)表控制层
 *
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
@RestController
@RequestMapping("wmNewsMaterial")
public class WmNewsMaterialController  {
    /**
     * 服务对象
     */
    @Resource
    private WmNewsMaterialService wmNewsMaterialService;

}

