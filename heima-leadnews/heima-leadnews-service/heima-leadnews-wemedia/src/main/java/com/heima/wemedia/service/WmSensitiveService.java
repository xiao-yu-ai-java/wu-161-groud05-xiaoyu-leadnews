package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.wemedia.dto.PageSensitiveDto;
import com.heima.model.common.wemedia.dto.SensitiveDto;
import com.heima.model.common.wemedia.pojo.WmSensitive;

import java.util.List;



    /**
     * 自媒体敏感词表(WmSensitive)表服务接口
     *
     * @author makejava
     * @since 2023-06-25 17:00:44
     */
    public interface WmSensitiveService extends IService<WmSensitive> {

        /**
         * 新增敏感词
         * @param adSensitiveDto
         * @return
         */
        ResponseResult saveSensitive(SensitiveDto adSensitiveDto);

        /**
         * 敏感词条件分页查询
         * @param pageSensitiveDto
         * @return
         */
        ResponseResult<List<WmSensitive>> listSensitive(PageSensitiveDto pageSensitiveDto);

        /**
         * 修改敏感词
         * @param sensitiveDto
         * @return
         */
        ResponseResult updateSensitive(SensitiveDto sensitiveDto);

        /**
         * 删除敏感词
         * @param id
         * @return
         */
        ResponseResult deleteSensitive(Long id);
    }



