package com.heima.wemedia.config;


import com.heima.common.thread.UserInfoThreadLocalUtil;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;

/**
 * feign的远程调用不经过网关, 到了下游到了拦截器
 * feign拦截器, 也是一个组件交给spring管理
 * 拦截feign的远程调用请求
 * 当使用发请求的过程中, feign的拦截器就会自动执行
 * 就把requestTemplate模板对象传给我, 就可以用模板对象重新构建请求
 *
 */
@Component
public class FeignHeaderInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        Long userId = UserInfoThreadLocalUtil.get().getUserId();
        //判断 如果线程有id 就设置
        if (userId != null){
            requestTemplate.header("userId",userId.toString());  //值在线程局部变量中
        }

    }
}
