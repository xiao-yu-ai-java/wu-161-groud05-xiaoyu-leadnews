package com.heima.wemedia.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.wemedia.dto.WmLoginDto;
import com.heima.wemedia.service.WmUserService;
import io.micrometer.core.instrument.config.validate.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j //请求路径映射
@RequestMapping(path = "/login")
@Api(value = "自媒体用户登录API", tags = "自媒体")
public class WmUserLoginController {

    //@Autowired  //spring 的注解 换了框架结构就要换
    @Resource   //jdk 提供的
    private WmUserService wmUserService;
    /**
     *自媒体用户登录接口
     */

    @ApiOperation(value = "自媒体用户登录接口")
    @PostMapping("/in")
    public ResponseResult login(@RequestBody WmLoginDto dto){

        return wmUserService.login(dto);
    }

}
