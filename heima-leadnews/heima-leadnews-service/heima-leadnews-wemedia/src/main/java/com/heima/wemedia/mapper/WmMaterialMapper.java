package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.wemedia.pojo.WmMaterial;
import org.springframework.stereotype.Repository;

@Repository
public interface WmMaterialMapper extends BaseMapper<WmMaterial> {
}
