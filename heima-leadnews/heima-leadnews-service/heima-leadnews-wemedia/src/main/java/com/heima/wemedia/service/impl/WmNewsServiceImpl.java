package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.heima.common.constants.KafkaMessageConstants;
import com.heima.common.thread.UserInfoThreadLocalUtil;
import com.heima.model.common.admin.dto.NewsAuthDto;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.dtos.WmNewsDownOrUpDto;
import com.heima.model.common.enums.AppHttpCodeEnum;

import com.heima.model.common.wemedia.dto.WmNewsDto;
import com.heima.model.common.wemedia.dto.WmNewsPageReqDto;
import com.heima.model.common.wemedia.pojo.WmMaterial;
import com.heima.model.common.wemedia.pojo.WmNewsMaterial;
import com.heima.model.common.wemedia.pojo.WmUser;
import com.heima.model.common.wemedia.vo.WmNewsVo;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.model.common.wemedia.pojo.WmNews;
import com.heima.wemedia.mapper.WmNewsMaterialMapper;
import com.heima.wemedia.mapper.WmUserMapper;
import com.heima.wemedia.service.WmNewsMaterialService;
import com.heima.wemedia.service.WmNewsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 自媒体文章表(WmNews)表服务实现类
 * 文章保存到数据库, 后发消息 服务安全监听
 * 自媒体服务保存文章到Wmnews表中, 就开始用kafka 异步发送消息, 自动扫描类就可以监听到, 开始检查
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
@Service("wmNewsService")
public class WmNewsServiceImpl extends ServiceImpl<WmNewsMapper, WmNews> implements WmNewsService {


    @Resource
    private WmUserMapper wmUserMapper;


    @Resource
    private WmNewsMapper wmNewsMapper;

    @Resource
    private WmMaterialMapper wmMaterialMapper;

    @Resource
    private WmNewsMaterialMapper wmNewsMaterialMapper;
    @Resource
    private WmNewsMaterialService wmNewsMaterialService;

    @Resource
    private KafkaTemplate kafkaTemplate;


    /**
     * 自媒体文章发布
     *
     * @param wmNewsDto
     * @return
     */
    @Override
    public ResponseResult submit(WmNewsDto wmNewsDto) {
        //1.校验参数
        if (wmNewsDto == null || StringUtils.isEmpty(wmNewsDto.getTitle()) || wmNewsDto.getChannelId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            //参数 , 标题, 频道不能没有
        }
        //2.处理数据(传的参数和数据库的字段不一样 要处理)
        /**
         * 当type是(自动-1)的时候, 就要处理(抽取内容种的图片作为封面). image前端传过来是数组, 我们用对象中image属性用集合收,数据库字段为string  所以后面要把集合每个元素用",", 分割为大的字符串.
         * 前端传过来的内容中是数组, 用集合收, 每一个元素是一个map集合, 根据key可以知道是图片还是内容.
         */
        //2.1图片url的集合 获取 原始封面图片集合列表
        List<String> coverImages = wmNewsDto.getImages();
        //2.2获取内容中的图片列表
        String content = wmNewsDto.getContent();
        List<String> contentImages = extractContent(content);  //自己定义的方法 抽取文章内容中的图片url集合

        //2.3获取用户封面类型, 处理用户封面类型s
        Short type = wmNewsDto.getType();

        //用户选择自动封装图片.  ----- 需要从文章内容中提取3张图片作为封面
        if (wmNewsDto.getType() == -1) {
            if (contentImages.size() > 3) { //文章中图片大于3, 就取前3张图片
                coverImages = contentImages.stream().limit(3).collect(Collectors.toList());
                //Todo 这是一个用stream抽取的的方法
                type = 3; //多图
            } else if (contentImages.size() > 0) {//文章中图片大于1, 就取前1张图片
                coverImages = contentImages.stream().limit(1).collect(Collectors.toList());
                type = 1; //单图
            } else { //文章中图片为空, 就返回一个空集合
                coverImages = Collections.emptyList();
                type = 0;  //无图
            }
        }


        //3.封装数据 保存文章数据到文章表
        //TODO 图片属性要转化为多个url拼接,  用户id线程获取 , type : 不是-1自动就直接存, 是(-1)判断到底是无/单/多图
        WmNews wmNews = saveWmNews(wmNewsDto, coverImages, type);
        //4.封装数据 保存文章数据到关系表(文章id, 素材id , type(封面引用, 素材引用))
        saveWmNewsMaterialRelation(wmNews.getId(), coverImages, contentImages); //通过图片和内容中的图片来获得素材表中素材id

        //TODO 文章保存到数据库, 后发消息 服务安全监听 用kafka------发布审核消息  自媒体发消息给审核scan服务
        if (wmNews.getStatus()==1){  //待审核的状态 才要审核
            //把主题搞个常量类, 后面好改  key可以省略  value是 文章的id(字符串)  往主题topi从中发, 所以主题要存在
            kafkaTemplate.send(KafkaMessageConstants.WM_NEWS_AUTO_SCAN_TOPIC,wmNews.getId().toString());
        }

        return ResponseResult.okResult("发布文章成功");
    }



    /**
     * 查询文章列表(分页查询)
     *
     * @param wmNewsPageReqDto
     * @return
     */
    @Override
    public ResponseResult queryList(WmNewsPageReqDto wmNewsPageReqDto) {
        //1.参数校验
        if (wmNewsPageReqDto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        wmNewsPageReqDto.checkParam();  //检验page, size
        //2.设置分页条件(page,size)
        Page<WmNews> page = new Page<>(wmNewsPageReqDto.getPage(), wmNewsPageReqDto.getSize());
        //3.设基本查询条件
        LambdaQueryWrapper<WmNews> wrapper = Wrappers.<WmNews>lambdaQuery();
        if (wmNewsPageReqDto.getStatus() != null) { //状态
            wrapper.eq(WmNews::getStatus, wmNewsPageReqDto.getStatus());
        }
        if (wmNewsPageReqDto.getKeyword() != null) { //关键字不为空
            wrapper.like(WmNews::getTitle, wmNewsPageReqDto.getKeyword());
        }
        if (wmNewsPageReqDto.getBeginPubdate() != null && wmNewsPageReqDto.getEndPubdate() != null) {  //发布时间
            wrapper.between(WmNews::getPublishTime, wmNewsPageReqDto.getBeginPubdate(), wmNewsPageReqDto.getEndPubdate());
        }
        if (wmNewsPageReqDto.getChannelId() != null) {//频道不为空
            wrapper.eq(WmNews::getChannelId, wmNewsPageReqDto.getChannelId());
        }
        wrapper.eq(WmNews::getUserId, UserInfoThreadLocalUtil.get().getUserId());
        //4.调用mapper层
         wmNewsMapper.selectPage(page, wrapper);
        //5.封装返回数据

        PageResponseResult result = new PageResponseResult(((Long) page.getPages()).intValue(), ((Long) page.getSize()).intValue(), ((Long) page.getTotal()).intValue());
        result.setData(page.getRecords());
        return result;
    }


    /**
     * 根据ID查询详情 修改的回显
     * @param id
     * @return
     */
    @Override
    public ResponseResult<WmNews> findById(Long id) {
        WmNews wmNews = wmNewsMapper.selectById(id);
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        return ResponseResult.okResult(wmNews);
    }


    /**
     * 根据文章ID删除文章
     * @param newsId
     * @return
     */
    @Override
    public ResponseResult deleteById(Long newsId) {
        //1.根据主键id查询文章
        WmNews wmNews = wmNewsMapper.selectById(newsId);
        //2.文章为空不能删
        if (wmNews==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.2文章发布不能删
        if (wmNews.getStatus()==9){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE,"已经发布的文章需要下架才能删除");
        }

        //3.删除文章数据
       wmNewsMapper.deleteById(newsId);

        //4.删除文章关联的素材数据
        wmNewsMaterialMapper.delete(Wrappers.<WmNewsMaterial>lambdaQuery().eq(WmNewsMaterial::getNewsId,wmNews.getId()));


        return ResponseResult.okResult("删除文章成功");
    }


    /**
     * 文章的上下架
     * @param wmNewsDownOrUpDto:  id    enable
     * @return\
     * enable  0 下架  1 上架
     */
    @Override
    public ResponseResult downOrUp(WmNewsDownOrUpDto wmNewsDownOrUpDto) {

        //1.检验参数
        if (wmNewsDownOrUpDto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.根据id查文章
        Long id = wmNewsDownOrUpDto.getId();
        WmNews wmNews = wmNewsMapper.selectById(id);
        //3.文章不存在结束
        if (wmNews==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //4.文章没发布, 不能上下架
        if (wmNews.getStatus()!=9){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"文章没发布, 不能上下架");
        }
        //5修改enable状态
        wmNews.setEnable(wmNewsDownOrUpDto.getEnable().intValue());
        wmNewsMapper.updateById(wmNews);

        //6.利用kafka 向文章服务发送状态 更新文章服务文章的状态
        Map<String, String> data = new HashMap<>();
        data.put("newsId", wmNews.getId() + "");
        data.put("enable", wmNewsDownOrUpDto.getEnable() + "");

        kafkaTemplate.send(KafkaMessageConstants.WM_Material_AUTO_SCAN_TOPIC,JSON.toJSONString(data));

        return ResponseResult.okResult("文章上下架成功");
    }


    /**
     *查询自媒体文章列表  (作业...)
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findList(NewsAuthDto dto) {
        //1.参数检查
        dto.checkParam();
        //2.分页查询 分页插件PageHelp
        //2.1设置分页参数
        PageHelper.startPage(dto.getPage(),dto.getSize());

        //2.2分页查询
        com.github.pagehelper.Page<WmNewsVo> page = wmNewsMapper.list();
        List<WmNewsVo> result = page.getResult();
        Long total = page.getTotal();

        Integer size = dto.getSize();
        Integer current = dto.getPage();  //当前页
        //3.结果返回
        PageResponseResult pageResponseResult = new PageResponseResult(current, size, total.intValue());
        pageResponseResult.setData(result);  //结果要有作者名字 有瑕疵
        return pageResponseResult;
    }


    /**
     * 查询自媒体文章详情
     * @param id
     * @return
     */
    @Override
    public ResponseResult findWmNewsVo(Integer id) {

        //1.检验参数
        if (id == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.查询文章信息
        WmNews wmNews = wmNewsMapper.selectById(id);
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //3.查询用户信息
        WmUser wmUser = wmUserMapper.selectById(wmNews.getUserId());
        //4.封装vo返回
        WmNewsVo wmNewsVo = new WmNewsVo();
        BeanUtils.copyProperties(wmNews,wmNewsVo);
        if (wmUser!=null){
            wmNewsVo.setAuthorName(wmUser.getNickname());
        }

        return new ResponseResult().ok(wmNewsVo);
    }


//========================================================================

    /**
     * 获取内容中的 图片列表(集合) 的方法
     *
     * @param content
     * @return
     */
    private List<String> extractContent(String content) {
        //content中的字符串是一个json数组, 每一个元素是map集合

        if (StringUtils.isEmpty(content)) {
            return Collections.emptyList();  //没有传内容就返回一个空集合
        }
        /*
         * 普通方法就是在外面搞个集合, 把遍历的东西放进去
         * */
        //如果内容不为空, 就获取图片的集合
        // 首先要把content数组字符串妆化为list集合, 里面每一个对象转化为一个map集合   parseArray 解析数组为集合  map型的集合
        //TODO 明确内容的的格式, 把数组转为ma形式 集合
        List<Map> mapList = JSON.parseArray(content, Map.class);

        //获取集合中的type为image的vaule(图片的url)______集合
        //TODO 过滤出图片的url
        List<String> list = mapList.stream()  //获得流对象
                .filter(map -> "image".equals(map.get("type"))) //过滤掉集合中key(type)不为image的map.
                .map(map -> map.get("value").toString()) //转化--->  把单个map集合妆化为字符串形式的value(图片的url)
                .collect(Collectors.toList());  //把流中的元素收集到集合
        return list;  //图片url集合
    }

    /**
     * 保存文章数据到文章表的方法
     *
     * @param wmNewsDto
     * @param coverImages
     * @param type
     */
    private WmNews saveWmNews(WmNewsDto wmNewsDto, List<String> coverImages, Short type) {
        //TODO 补充: 判断当前用户是新增还是下修改

        //TODO 属性拷贝的条件是 属性名一样, 属性类型一样
        WmNews wmNews = new WmNews();
        //userId(线程中)  type(数据类型不一样)  status(类型不一样) images(需要切割) enable(固定值) 要自己写
        BeanUtils.copyProperties(wmNewsDto, wmNews,"id");
        //StringUtils.join把字符串里面的元素按照","的方式分割
        wmNews.setImages(StringUtils.join(coverImages, ","));
        wmNews.setType(type.intValue());
        wmNews.setEnable(1);
        wmNews.setUserId(UserInfoThreadLocalUtil.get().getUserId());
        wmNews.setStatus(wmNewsDto.getStatus().intValue());

         //文章id为空, 就是新增操作
        if (wmNewsDto.getId()==null){
            //保存自媒体文章到文章数据库
            wmNewsMapper.insert(wmNews);
            return wmNews;
        }

        //文章id不为为空, 就是修改操作:
        wmNews.setId(wmNewsDto.getId());
        wmNewsMapper.updateById(wmNews);
        //删除旧文章和素材的关联数据
        wmNewsMaterialMapper.delete(Wrappers.<WmNewsMaterial>lambdaQuery().eq(WmNewsMaterial::getNewsId,wmNews.getId()));
        return wmNews;
    }


    /**
     * 保存自媒体文章封面 以及自媒体文章内容图片和素材的引用关系
     *
     * @param newId
     * @param coverImages
     * @param contentImages
     */
    private void saveWmNewsMaterialRelation(Long newId, List<String> coverImages, List<String> contentImages) {

        //1.保存自媒体封面素材引用   如果封面图片为空不用保存
        //都是用根据url查询素材表, 找到素材id, 保存到文章内容素材表(中间表)
        if (!CollectionUtils.isEmpty(coverImages)) {   //0 就是封面的url
            saveWmNewsCoverImagesRelation(newId, coverImages, 0);

        }
        //2.保存自媒体内容素材引用   如果内容图片为空不用保存
        if (!CollectionUtils.isEmpty(contentImages)) { //1 就是内容的url
            saveWmNewsCoverImagesRelation(newId, contentImages, 1);
        }
    }


    /**
     * 保存图片引用数据到数据库
     *
     * @param newId
     * @param coverImages
     * @param type
     */
    private void saveWmNewsCoverImagesRelation(Long newId, List<String> coverImages, int type) {
        //TODO 对图片URL集合进行去重  distinct
        coverImages = coverImages.stream().distinct().collect(Collectors.toList());


        //1.就差素材wmMaterialId 用coverImages 去素表中查

        //1.2 list 批量查询in eq查一个, in查多个
        List<WmMaterial> wmMaterials = wmMaterialMapper.selectList(Wrappers.<WmMaterial>lambdaQuery().in(WmMaterial::getUrl, coverImages));

        //取出素材中的ids 到集合
        // List<Long> ids = wmMaterials.stream().map(wmMaterial -> wmMaterial.getId()).collect(Collectors.toList());

        //2.TODO 封装数据保存到中间表(文章/素材表)
       /* WmNewsMaterial wmNewsMaterial = new WmNewsMaterial();
        for (Long materialId : ids) {
            wmNewsMaterial.setId(materialId);
            wmNewsMaterial.setNewsId(UserInfoThreadLocalUtil.get().getUserId());
            wmNewsMaterial.setType(type);
            wmNewsMaterialMapper.insert(wmNewsMaterial);
        }*/
        //用stream流  批量保存素材关系数据到数据库 --- 批处理操作
        List<WmNewsMaterial> wmNewsMaterialList = wmMaterials.stream()
                .map(wmMaterial -> {
                    WmNewsMaterial wmNewsMaterial = new WmNewsMaterial();
                    wmNewsMaterial.setMaterialId(wmMaterial.getId());
                    wmNewsMaterial.setNewsId(UserInfoThreadLocalUtil.get().getUserId());
                    wmNewsMaterial.setType(type);
                    return wmNewsMaterial;
                }).collect(Collectors.toList());

        wmNewsMaterialService.saveBatch(wmNewsMaterialList);
    }



}

