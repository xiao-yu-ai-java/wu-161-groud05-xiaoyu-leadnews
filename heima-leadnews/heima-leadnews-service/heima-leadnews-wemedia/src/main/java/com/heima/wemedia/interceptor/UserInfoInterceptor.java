package com.heima.wemedia.interceptor;


import com.alibaba.fastjson.JSON;
import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserInfoThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 写完拦截器 , 要注册配置拦截器 不然不生效
 * 拦截器用于拦截请求, 接收网关的消息, 将用户id放到局部变量中
 * 以前是拦截器看有没有token 没有就登录
 * 现在有了网关, 在网关设置了用户id 所以直接看有没有用户id
 * 网关-->过滤器----->拦截器-->controller
 */

@Component //注意放到容器
public class UserInfoInterceptor extends HandlerInterceptorAdapter {  //继承拦截器适配器


    //前置拦截设置userId到线程局部变量
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //1.获取请求中的userId头信息
        String userId = request.getHeader("userId");

        //2. 设置userId到ThreadLocal中
        if (StringUtils.isNotEmpty(userId)){  //不为空  为空就是因为没有登录
            UserInfo userInfo = new UserInfo();
            userInfo.setUserId(Long.valueOf(userId));
            UserInfoThreadLocalUtil.set(userInfo);
            return true;
        }

        //没有userId代表没有认证, 返回需要认证登录
        ResponseResult responseResult = ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN); //返回错误信息没登陆

        //如何将这个对象打回客户端 用response 写入到响应中
        response.setContentType("application/json;charset=utf-8");  //指定字符集
        response.getWriter().write(JSON.toJSONString(responseResult)); //响应错误信息
        return false;
    }


    //后置拦截清理数据
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
    }
}
