package com.heima.wemedia.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.wemedia.dto.SensitiveDto;
import com.heima.model.common.wemedia.dto.PageSensitiveDto;
import com.heima.model.common.wemedia.pojo.WmSensitive;
import com.heima.wemedia.service.WmSensitiveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/**
 * 自媒体敏感词表(WmSensitive)表控制层
 *
 * @author makejava
 * @since 2023-06-25 17:00:44
 */
@RestController
@RequestMapping("/api/v1/sensitive")
@Slf4j
public class WmSensitiveController  {
    /**
     * 服务对象
     */
    @Resource
    private WmSensitiveService wmSensitiveService;

    /**
     * 新增敏感词
     * @param adSensitiveDto
     * @return
     */
    @PostMapping("/save")
    public ResponseResult saveSensitive(@RequestBody SensitiveDto adSensitiveDto){
        return wmSensitiveService.saveSensitive(adSensitiveDto);
    }

    /**
     * 敏感词条件分页查询
     * @param pageSensitiveDto
     * @return
     */
    @PostMapping("/list")
    public ResponseResult<List<WmSensitive>> list(@RequestBody PageSensitiveDto pageSensitiveDto){
        return wmSensitiveService.listSensitive(pageSensitiveDto);
    }

    /**
     * 修改敏感词
     * @param sensitiveDto
     * @return
     */
    @PostMapping("/update")
    public ResponseResult update(@RequestBody SensitiveDto sensitiveDto){
        return wmSensitiveService.updateSensitive(sensitiveDto);
    }

    /**
     * 删除敏感词
     * @param id
     * @return
     */
    @DeleteMapping("/del/{id}")
    public ResponseResult del(@PathVariable Long id){
        return wmSensitiveService.deleteSensitive(id);
    }
}


