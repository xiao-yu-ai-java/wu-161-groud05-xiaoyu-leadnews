package com.heima.wemedia.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.wemedia.dto.WmLoginDto;
import com.heima.model.common.wemedia.pojo.WmUser;

public interface WmUserService {
    /**
     *自媒体用户登录接口
     */
    ResponseResult login(WmLoginDto dto);


    /**
     * 这个接口后面要被用户服务调用
     * 新增自媒体用户
     * @param wmUser
     */
    ResponseResult save(WmUser wmUser);
}
