package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.admin.dto.NewsAuthDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.dtos.WmNewsDownOrUpDto;
import com.heima.model.common.wemedia.dto.WmNewsDto;
import com.heima.model.common.wemedia.dto.WmNewsPageReqDto;
import com.heima.model.common.wemedia.pojo.WmNews;

/**
 * 自媒体文章表(WmNews)表服务接口
 * 文章保存到数据库, 后发消息 服务安全监听
 *
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
public interface WmNewsService extends IService<WmNews> {

    /**
     *
     * 自媒体文章发布
     * @param wmNewsDto
     * @return
     */
    ResponseResult submit(WmNewsDto wmNewsDto);

    /**
     *查询文章列表(分页查询)
     */
    ResponseResult queryList(WmNewsPageReqDto wmNewsPageReqDto);


    /**
     * 根据ID查询详情
     * @param id
     * @return
     */
    ResponseResult<WmNews> findById(Long id);

    /**
     * 根据文章ID删除文章
     * @param newsId
     * @return
     */
    ResponseResult deleteById(Long newsId);

    /**
     * 文章的上下架
     * @param wmNewsDownOrUpDto
     * @return
     */
    ResponseResult downOrUp(WmNewsDownOrUpDto wmNewsDownOrUpDto);


    /**
     *查询自媒体文章列表
     * @param dto
     * @return
     */
    ResponseResult findList(NewsAuthDto dto);

    /**
     * 查询自媒体文章详情
     * @param id
     * @return
     */
    ResponseResult findWmNewsVo(Integer id);
}

