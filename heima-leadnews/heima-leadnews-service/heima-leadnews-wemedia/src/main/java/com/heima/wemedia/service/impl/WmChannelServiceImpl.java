package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.wemedia.dto.ChannelDto;
import com.heima.model.common.wemedia.dto.PageChannelDto;
import com.heima.model.common.wemedia.pojo.WmNews;
import com.heima.model.common.wemedia.pojo.WmSensitive;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.model.common.wemedia.pojo.WmChannel;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.service.WmChannelService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 自媒体频道表(WmChannel)表服务实现类
 *
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
@Service("wmChannelService")
public class WmChannelServiceImpl extends ServiceImpl<WmChannelMapper, WmChannel> implements WmChannelService {

    @Resource
   private WmChannelMapper wmChannelMapper;

    @Resource
    private WmNewsMapper wmNewsMapper;

    /**
     * 查询所有正常状态的频道数据 状态=1 就是正常的
     * @return
     */
    @Override
    public ResponseResult<List<WmChannel>> channels() {

        //1.封装隐含条件 status = 1
        LambdaQueryWrapper<WmChannel> wrapper = Wrappers.<WmChannel>lambdaQuery()
                .eq(WmChannel::getStatus, 1)
                .orderByAsc(WmChannel::getOrd);

        //2.调用mapper查询
        List<WmChannel> channelList = wmChannelMapper.selectList(wrapper);
        return ResponseResult.okResult(channelList);
    }


    /**
     * 频道名称模糊分页查询 like
     * @param pageChannelDto
     * @return
     */
    @Override
    public ResponseResult<List<WmChannel>> pageList(PageChannelDto pageChannelDto) {
        //1.参数校验
        if (pageChannelDto == null ) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //检验参数
        pageChannelDto.checkParam();

        //2.设置分页参数 : 起始页 页面大小
        Page<WmChannel> page = new Page<>(pageChannelDto.getPage(), pageChannelDto.getSize());
        //3.创建查询条件 模糊查询
        LambdaQueryWrapper<WmChannel> wrapper = Wrappers.<WmChannel>lambdaQuery();

        if (StringUtils.isNotEmpty(pageChannelDto.getName())) {
            wrapper.like(WmChannel::getName, pageChannelDto.getName());
        }
        //4.查询 page和条件
        wmChannelMapper.selectPage(page, wrapper);

        //5.PageResponseResult返回查询结果
        Long total = page.getTotal();  //总条数
        List<WmChannel> records = page.getRecords();//详细数据
        Long size = page.getSize(); //分页大小
        Long current = page.getCurrent(); //当前页
        PageResponseResult result = new PageResponseResult(current.intValue(), size.intValue(), total.intValue());

        result.setData(records);
        return result;
    }


    /**
     * 删除频道 先查判断再删
     *
     * @param channelId
     * @return
     */
    /*先查询看是否有, 再删除 根据id删除即可*/
    @Override
    public ResponseResult deleteChannel(Long channelId) {
        //1.校验参数
        if (channelId == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.查询数据库是否存在数据
        WmChannel wmChannel = wmChannelMapper.selectById(channelId);
        //2.1判断
        if (wmChannel == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //3.频道是否有效, 有效(正在看)不能删
        if (wmChannel.getStatus()==1) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //4.判断是否被引用 (上架状态为9 , 根据频道id查)
        Integer count = wmNewsMapper.selectCount(Wrappers.<WmNews>lambdaQuery().eq(WmNews::getChannelId, channelId).eq(WmNews::getStatus, 9));

        if (count > 0){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);//不能删
        }

        //3.删除
        wmChannelMapper.deleteById(channelId);
        return ResponseResult.okResult("删除频道成功!");
    }

    /**
     * 添加(新增)频道
     *
     * @param channelDto
     * @return
     */
    @Override
    public ResponseResult saveChannel(ChannelDto channelDto) {
        //1.参数校验
        if (channelDto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.判断频道是否已经存在
        WmChannel wmChannel = wmChannelMapper.selectOne(Wrappers.<WmChannel>lambdaQuery()
                .eq(WmChannel::getName, channelDto.getName()));
        if (wmChannel != null) {
            return ResponseResult.okResult(AppHttpCodeEnum.DATA_EXIST);
        }
        //3.数据库新增
        wmChannel = new WmChannel();
        BeanUtils.copyProperties(channelDto, wmChannel);
        wmChannel.setIsDefault(channelDto.isStatus() ? 1 : 0);
        wmChannel.setStatus(channelDto.isDefault() ? 1 : 0);
        wmChannelMapper.insert(wmChannel);
        return ResponseResult.okResult("新增成功");
    }

    /**
     * 修改频道
     * @param wmChannel
     * @return
     */
    @Override
    public ResponseResult updateChannel(WmChannel wmChannel) {
        //1.参数校验
        if (wmChannel == null || wmChannel.getId()== null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.查询频道 有没有是上架状态的9 频道被引用不能被删除  条件: name id
        Integer count = wmNewsMapper.selectCount(Wrappers.<WmNews>lambdaQuery()
                .eq(WmNews::getChannelId, wmChannel.getId())
                .eq(WmNews::getStatus, 9));

        //2.1判断
        if (count > 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE,"频道被引用不能修改或禁用");
        }

        //3.数据库修改
        wmChannelMapper.updateById(wmChannel);
        return ResponseResult.okResult("修改成功了");
    }
}

