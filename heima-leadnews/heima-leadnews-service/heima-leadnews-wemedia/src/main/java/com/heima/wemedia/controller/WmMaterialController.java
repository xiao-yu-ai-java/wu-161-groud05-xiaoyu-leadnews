package com.heima.wemedia.controller;

import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.wemedia.dto.WmMaterialDto;
import com.heima.model.common.wemedia.pojo.WmMaterial;
import com.heima.wemedia.service.WmMaterialService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

@RestController
@RequestMapping("/api/v1/material")
@Api(value = "自媒体素材管理相关API",tags = "自媒体")
public class WmMaterialController {

    @Resource
    private WmMaterialService wmMaterialService;

    /**
     * 素材上传
     * 图片文件用固定的MultipartFile类型接收文件流
     * 成功需要回显图片，返回素材对象
     */
    @ApiOperation(value = "自媒体上传接口")
    @PostMapping("/upload_picture")
    public ResponseResult<WmMaterial> upload(MultipartFile multipartFile) throws IOException {
        return wmMaterialService.upload(multipartFile);
    }


    /**
     * 素材分页查询
     * @param wmMaterialDto :page size isCollection
     * @return
     */
    @ApiOperation(value = "自媒体分页查询接口")
    @PostMapping("/list")
    public ResponseResult list(@RequestBody WmMaterialDto wmMaterialDto) {

        return wmMaterialService.queryList(wmMaterialDto);
    }


    /**
     * 删除素材
     * @param id
     * @return
     */
    @ApiOperation(value = "自媒体素材删除接口")
    @DeleteMapping("/del_picture/{id}")
    public ResponseResult delete(@PathVariable("id") Integer id){

        return wmMaterialService.deleteById(id);
    }


    /**
     * 收藏素材
     *
     * @param id
     * @return
     */
    @PutMapping(path = "/collect/{id}")
    @ApiOperation(value = "素材收藏")
    public ResponseResult collect(@PathVariable("id") Long id) {
        return wmMaterialService.collect(id);
    }

    /**
     * 取消收藏素材
     *
     * @param id
     * @return
     */
    @PutMapping(path = "/cancel_collect/{id}")
    @ApiOperation(value = "素材取消删除")
    public ResponseResult cancel(@PathVariable("id") Long id) {
        return wmMaterialService.cancel(id);
    }

}
