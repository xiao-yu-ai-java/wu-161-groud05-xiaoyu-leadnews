package com.heima.wemedia.task;


import com.heima.common.enums.TaskTypeEnum;
import com.heima.common.exception.CustomException;
import com.heima.feign.api.ApArticleFeignClient;
import com.heima.feign.api.ScheduleFeignClient;
import com.heima.model.common.article.dto.ApArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.schedule.pojo.TaskInfo;
import com.heima.model.common.wemedia.pojo.WmChannel;
import com.heima.model.common.wemedia.pojo.WmNews;
import com.heima.model.common.wemedia.pojo.WmUser;
import com.heima.utils.common.ProtostuffUtil;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
@Slf4j
@Component
public class WmNewsAutoPublishTask {


    @Resource
    private ApArticleFeignClient apArticleFeignClient;

    @Resource
    private WmUserMapper wmUserMapper;

    @Resource
    private WmChannelMapper wmChannelMapper;

    @Resource
    private ScheduleFeignClient scheduleFeignClient;

    @Resource
    private WmNewsMapper wmNewsMapper;

    @Scheduled(fixedDelay = 2000)
    public void wmNewsAutoPublishTask(){
        log.info("拉取任务自动发布文章开始执行----------");
        while (true){ //提高效率
        //1.调用任务调度服务拉取任务
        ResponseResult<TaskInfo> result = scheduleFeignClient.pollTask(TaskTypeEnum.NEWS_SCAN_TIME.getTaskType(), TaskTypeEnum.NEWS_SCAN_TIME.getPriority());
            log.info("拉取任务成功, 任务数据:{}",result.getData());
        if (result.getCode() != 200 ||result.getData() ==null){
            break;
        }
        log.info("拉取到任务, 开始自动发布文章, 任务数据{}:",result.getData());
        //2.获取任务参数
        TaskInfo taskInfo = result.getData();
        WmNews param = ProtostuffUtil.deserialize(taskInfo.getParameters(), WmNews.class);  //反序列化
        //3.查询文章详情
        WmNews wmNews = wmNewsMapper.selectById(param.getId());
        log.info("查询到需要发布的文章数据, 文章id:{}",wmNews.getId());
        if (wmNews==null){
            continue;
        }
        //4.发布文章到App端
        saveApArticle(wmNews);
        log.info("发布文章到App成功-----------");
        //5.修改文章状态为审核通过已发布
        wmNews.setStatus(9);
        wmNewsMapper.updateById(wmNews);
            log.info("文章发布成功, 修改文章状态为审核通过已发布");
        }
    }


    /**
     * 通过feign 发布(保存)自媒体文章到APP端(文章服务)
     *
     * @param wmNews
     */
    private void saveApArticle(WmNews wmNews) {
        //1.封装文章信息   对比者2个类的属性wmNews 和apArticleDto
        ApArticleDto apArticleDto = new ApArticleDto();
        BeanUtils.copyProperties(wmNews, apArticleDto, "id");
        // newsId authorId outhorName channelName Layout
        apArticleDto.setNewsId(wmNews.getId());
        //封装作者信息
        apArticleDto.setAuthorId(wmNews.getUserId());
        WmUser wmUser = wmUserMapper.selectById(wmNews.getUserId());
        if (wmUser != null) {
            apArticleDto.setAuthorName(wmUser.getNickname());
        }
        //封装频道信息
        WmChannel wmChannel = wmChannelMapper.selectById(wmNews.getChannelId());
        if (wmChannel != null){
            apArticleDto.setChannelName(wmChannel.getName());
        }
        apArticleDto.setLayout(wmUser.getType());

        //2.远程调用 把自媒体的文章保存到文章服务中
        ResponseResult<String> result = apArticleFeignClient.saveApArticle(apArticleDto);

        //3.调用失败 抛异常
        if (result.getCode() !=200){
            throw new CustomException(AppHttpCodeEnum.SERVER_ERROR);
        }
    }
}
