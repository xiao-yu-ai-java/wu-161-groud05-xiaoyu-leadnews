package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.wemedia.dto.WmLoginDto;
import com.heima.model.common.wemedia.pojo.WmUser;
import com.heima.utils.common.AppJwtUtil;
import com.heima.utils.common.MD5Utils;
import com.heima.wemedia.mapper.WmUserMapper;
import com.heima.wemedia.service.WmUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;

@Service
public class WmUserServiceImpl implements WmUserService {
    @Resource
    private WmUserMapper wmUserMapper;

    /**
     *自媒体用户登录接口
     * 用MD5加 加盐方式保证密码的安全性  加盐就是随机生成一个字符串 与MD5生成的密码组合
     */
    @Override
    public ResponseResult login(WmLoginDto dto) {
        /*
        1.检验参数 判断用户输入的信息是否为非法(用户名/密码/dto是否为空 )
        2.根据用户名字来查询数据库的密码(已经是加盐后的), 也要查询加盐字符串,用于判断用户输入的密码是否正确
        3.根据用户输入的密码和加盐字符串来组合生成密文
        4.判断: 数据库返回的密码 和 用户输入的密码(加盐参数组合)是否相等
        5.相等返回用户信息和jwt 不相等返回友好错误提示
        */

        //1.检验参数 判断用户输入的信息是否为非法(用户名/密码/dto是否为空 )
        if (dto==null||StringUtils.isEmpty(dto.getName())||StringUtils.isEmpty(dto.getPassword())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE,"用户名和密码不能为空");
        }

        //2.根据用户名字来查询数据库的密码(已经是加盐后的), 也要查询加盐字符串,用于判断用户输入的密码是否正确

//        QueryWrapper<WmUser> wrapper = new QueryWrapper<>();
//        wrapper.eq("username",dto.getName());  有硬编码 第一个是数据库的字段, 第二个是传的参数值

        //没有硬编码 第一个参数方法映射 WmUser::getPassword会找到WmUser类中的password属性对应的列名,
        LambdaQueryWrapper<WmUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(WmUser::getUsername,dto.getName());

//        LambdaQueryWrapper<WmUser> wrapper = Wrappers.<WmUser>lambdaQuery();
//        wrapper.eq(WmUser::getUsername,dto.getName());  //需要指定泛型


        WmUser wmUser = wmUserMapper.selectOne(wrapper); //查询一个对象 selectOne ,查询多个对象用selectList


        if (wmUser.getStatus()!=1){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE,"用户已被禁用");
        }
        //3.根据用户输入的密码和加盐字符串来组合生成密文
        String password = dto.getPassword(); //用户输入密码
        String salt = wmUser.getSalt();  //盐值 字符串
        String encode = MD5Utils.encodeWithSalt(password, salt);//md5加盐处理

        //4.判断: 数据库返回的密码 和 用户输入的密码(加盐参数组合)是否相等

        if (!StringUtils.equals(wmUser.getPassword(),encode)){ //用工具类不会出现空指针
            return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR); //密码错误
        }

         //一致 返回对象和jwt  登录成功返回JWT 之前是把用户信息封装在jwt中
        HashMap<String, Object> result = new HashMap<>();

        wmUser.setPassword("******");
        wmUser.setSalt(null);  //bu返回颜值和密码
        result.put("user",wmUser);
        result.put("token", AppJwtUtil.getToken(wmUser.getId()));//AppJwtUtil工具类

        return ResponseResult.okResult(result);  //返回正确的信息
    }



    public static void main(String[] args) {
        String password = "123456";
        String salt = "abcd";
        String encode = MD5Utils.encodeWithSalt(password, salt);
        System.out.println(encode);  //3c1fd4fd06dcaff7fe95b32f7ec02a71
    }


    /**
     * 这个接口后面要被用户服务调用
     * 新增自媒体用户
     * @param wmUser
     */
    @Override
    public ResponseResult save(WmUser wmUser) {

        //1.检验参数  : wmUser不能为空, 用户名, 密码 , 盐都不能为空 自媒体用户登录必备
        if (wmUser == null|| StringUtils.isEmpty(wmUser.getUsername())||StringUtils.isEmpty(wmUser.getPassword())||StringUtils.isEmpty(wmUser.getSalt())) {
        return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        //2.保存
        wmUserMapper.insert(wmUser);
        return ResponseResult.okResult("操作成功");
    }

}














































