package com.heima.wemedia.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.wemedia.pojo.WmUser;
import org.springframework.stereotype.Repository;

@Repository   //数据访问层 注册到容器中, 需要继承BaseMapper泛型写实体类
public interface WmUserMapper extends BaseMapper<WmUser> {
}
