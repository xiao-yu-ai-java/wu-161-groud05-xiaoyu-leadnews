package com.heima.wemedia;

import com.heima.feign.api.ApArticleFeignClient;
import com.heima.feign.api.ScheduleFeignClient;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.scheduling.annotation.EnableScheduling;

//自媒体启动类
@SpringBootApplication
@MapperScan(basePackages = "com.heima.wemedia.mapper")
@EnableScheduling
@EnableFeignClients(clients = {ScheduleFeignClient.class, ApArticleFeignClient.class})
public class WemediaApplication {
    public static void main(String[] args) {
        SpringApplication.run(WemediaApplication.class,args);
    }
}