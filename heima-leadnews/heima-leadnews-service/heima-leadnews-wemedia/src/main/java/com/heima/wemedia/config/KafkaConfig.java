package com.heima.wemedia.config;

import com.heima.common.constants.KafkaMessageConstants;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;


/**
 * kafka的配置类 用于声明消息发送的主题topic(队列)
 */
@Configuration
public class KafkaConfig {


    @Bean
    public NewTopic newTopic(){
        return TopicBuilder.name(KafkaMessageConstants.WM_NEWS_AUTO_SCAN_TOPIC).build();
    }
}
