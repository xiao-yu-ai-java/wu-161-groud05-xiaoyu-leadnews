package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.wemedia.mapper.WmNewsMaterialMapper;
import com.heima.model.common.wemedia.pojo.WmNewsMaterial;
import com.heima.wemedia.service.WmNewsMaterialService;
import org.springframework.stereotype.Service;

/**
 * 文章素材关联表(WmNewsMaterial)表服务实现类
 *
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
@Service("wmNewsMaterialService")
public class WmNewsMaterialServiceImpl extends ServiceImpl<WmNewsMaterialMapper, WmNewsMaterial> implements WmNewsMaterialService {

}

