package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.wemedia.dto.ChannelDto;
import com.heima.model.common.wemedia.dto.PageChannelDto;
import com.heima.model.common.wemedia.pojo.WmChannel;

import java.util.List;

/**
 * 自媒体频道表(WmChannel)表服务接口
 *
 * @author makejava
 * @since 2023-06-14 11:30:35
 */
public interface WmChannelService extends IService<WmChannel> {

    /**
     * 查询频道列表接口
     * @return
     */
    ResponseResult<List<WmChannel>> channels();


    /**
     * 频道名称模糊分页查询
     * @param pageChannelDto
     * @return
     */
    ResponseResult<List<WmChannel>> pageList(PageChannelDto pageChannelDto);

    /**
     * 删除频道
     * @param channelId
     * @return
     */
    ResponseResult deleteChannel(Long channelId);

    /**
     * 添加频道
     * @param channelDto
     * @return
     */
    ResponseResult saveChannel(ChannelDto channelDto);

    /**
     * 修改频道
     * @param wmChannel
     * @return
     */
    ResponseResult updateChannel(WmChannel wmChannel);
}

