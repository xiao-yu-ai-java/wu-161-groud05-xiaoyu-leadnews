package com.heima.wemedia.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.wemedia.dto.PageSensitiveDto;
import com.heima.model.common.wemedia.dto.SensitiveDto;
import com.heima.model.common.wemedia.pojo.WmSensitive;
import com.heima.wemedia.mapper.WmSensitiveMapper;
import com.heima.wemedia.service.WmSensitiveService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 自媒体敏感词表(WmSensitive)表服务实现类
 */
@Service
public class WmSensitiveServiceImpl extends ServiceImpl<WmSensitiveMapper, WmSensitive> implements WmSensitiveService {


    @Resource
    private WmSensitiveMapper wmSensitiveMapper;

    /**
     * 新增敏感词
     *
     * @param adSensitiveDto
     * @return
     */
    @Override
    public ResponseResult saveSensitive(SensitiveDto adSensitiveDto) {
        //1.参数校验
        if (StringUtils.isEmpty(adSensitiveDto.getSensitives())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        //2.判断数据库中敏感词是否已经存在   已存在的敏感词不用保存
        WmSensitive wmSensitive = wmSensitiveMapper.selectOne(Wrappers.<WmSensitive>lambdaQuery()
                .eq(WmSensitive::getSensitives, adSensitiveDto.getSensitives()));

        if (wmSensitive != null) {
            return ResponseResult.okResult(AppHttpCodeEnum.DATA_EXIST);
        }
        //3.数据库新增
        wmSensitive = new WmSensitive();
        wmSensitive.setSensitives(adSensitiveDto.getSensitives());
        wmSensitiveMapper.insert(wmSensitive);
        return ResponseResult.okResult("新增成功");
    }

    /**
     * 敏感词条件分页查询
     *
     * @param pageSensitiveDto
     * @return
     */
    @Override
    public ResponseResult<List<WmSensitive>> listSensitive(PageSensitiveDto pageSensitiveDto) {

        //1.参数校验
        if (pageSensitiveDto == null ) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //1.1 检查分页
        pageSensitiveDto.checkParam();

        //2.设置分页参数 : 起始页 页面大小
        Page<WmSensitive> page = new Page<>(pageSensitiveDto.getPage(), pageSensitiveDto.getSize());
        //3.创建封装查询条件  模糊查询
        LambdaQueryWrapper<WmSensitive> wrapper = Wrappers.<WmSensitive>lambdaQuery();

        if (StringUtils.isNotEmpty(pageSensitiveDto.getName())) {
            wrapper.like(WmSensitive::getSensitives, pageSensitiveDto.getName());
        }
        //4.查询
        wmSensitiveMapper.selectPage(page, wrapper);
        //5.PageResponseResult返回查询结果
        Long total = page.getTotal();  //总条数
        List<WmSensitive> records = page.getRecords(); //详细数据
        Long size = page.getSize(); //分页大小
        Long current = page.getCurrent(); //当前页

        PageResponseResult result = new PageResponseResult(current.intValue(), size.intValue(), total.intValue());

        result.setData(records);


        return result;
    }

    /**
     * 修改敏感词
     *
     * @param sensitiveDto
     * @return
     */
    @Override
    public ResponseResult updateSensitive(SensitiveDto sensitiveDto) {
        //1.校验参数
        if (sensitiveDto == null || sensitiveDto.getId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.查询数据库是否存在数据
        WmSensitive wmSensitive = wmSensitiveMapper.selectById(sensitiveDto.getId());
        //2.1判断
        if (wmSensitive == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //3.设值修改修改
        wmSensitive.setSensitives(sensitiveDto.getSensitives());
        wmSensitiveMapper.updateById(wmSensitive);
        return ResponseResult.okResult("新增成功");
    }

    /**
     * 删除敏感词
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult deleteSensitive(Long id) {
        /*先查询看是否有, 再删除 根据id删除即可*/
        //1.校验参数
        if (id == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.查询数据库是否存在数据
        WmSensitive wmSensitive = wmSensitiveMapper.selectById(id);
        if (wmSensitive == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //3.删除
        wmSensitiveMapper.deleteById(id);
        return ResponseResult.okResult("删除成功");
    }
}
