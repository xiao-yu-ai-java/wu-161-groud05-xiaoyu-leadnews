package com.heima.admin.service;

import com.heima.model.common.admin.dto.AdUserLoginDto;
import com.heima.model.common.dtos.ResponseResult;

import java.util.List;

/**
 * 管理员用户信息表(AdUser)表服务接口
 *
 * @author makejava
 * @since 2022-04-22 14:38:46
 */
public interface AdUserService {
    /**
     * 管理端用户登录
     *
     * @param dto
     * @return
     */
    ResponseResult login(AdUserLoginDto dto);
}
