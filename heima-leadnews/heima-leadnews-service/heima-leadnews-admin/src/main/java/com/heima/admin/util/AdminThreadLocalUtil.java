package com.heima.admin.util;


import com.heima.model.common.admin.entity.AdUser;

public class AdminThreadLocalUtil {

    private static final ThreadLocal<AdUser> TL = new ThreadLocal();

    /**
     * 保存用户信息到ThreadLocal
     *
     * @param adUser
     */
    public static void set(AdUser adUser) {
        if (adUser != null) {
            TL.set(adUser);
        }
    }


    /**
     * 从ThreadLocal获取用户信息
     *
     * @return
     */
    public static AdUser get() {
        return TL.get();
    }

    /**
     * 从ThreadLocal获取用户信息
     *
     * @return
     */
    public static void remove() {
        TL.remove();
    }


}
