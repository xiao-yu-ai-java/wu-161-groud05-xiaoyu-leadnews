package com.heima.behavior.service.impl.ApFollowBehaviorServiceImpl;


import com.heima.behavior.dto.LikesBehaviorDto;
import com.heima.behavior.pojo.ApLikesBehavior;
import com.heima.behavior.service.ApLikesBehaviorService;
import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserInfoThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class ApLikesBehaviorServiceImpl implements ApLikesBehaviorService {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private MongoTemplate mongoTemplate;
    /**
     * 用户点赞接口
     * @param dto
     * @return
     */
    @Override
    public ResponseResult like(LikesBehaviorDto dto) {
        //1. 校验参数 : 文章id都没有点个毛
        if (dto==null||dto.getArticleId()==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        short operation = dto.getOperation();
        if (operation !=0 && operation !=1){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        UserInfo userInfo = UserInfoThreadLocalUtil.get();
        if (userInfo==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //2.判断是含之前点过赞  : 到redis中查看
        String like_key = "USER_LIKES_"+ dto.getArticleId();
        Boolean hasKey = stringRedisTemplate.opsForHash().hasKey(like_key, userInfo.getUserId().toString());
        //2.1之前未点赞 ----> 点赞 :新增点赞数据到mongodb和redis
        if (!hasKey&&operation==0){   //之前没点过赞
        //新增点赞数据到mongodb
            ApLikesBehavior apLikesBehavior = new ApLikesBehavior();
            apLikesBehavior.setType(dto.getType());
            apLikesBehavior.setArticleId(dto.getArticleId());
            apLikesBehavior.setUserId(userInfo.getUserId());
            apLikesBehavior.setCreatedTime(new Date());

            mongoTemplate.save(apLikesBehavior);
        // 新增点赞数据到 redis
            stringRedisTemplate.opsForHash().put(like_key,userInfo.getUserId(),"1");
            return ResponseResult.okResult("点赞成功");
        }
        //2.2之前点过赞 ----> 取消赞 : 从mongodb和redis删除点赞数据
        //从mongodb删除点赞数据
        if (hasKey&&operation==1){
            Query query = Query.query(Criteria.where("userId").is(userInfo.getUserId()).and("articleId").is(dto.getArticleId()));  //根据用户id和文章id 去删除
            mongoTemplate.remove(query,ApLikesBehavior.class);
            //从redis删除点赞数据
            stringRedisTemplate.opsForHash().delete(like_key,userInfo.getUserId().toString());
            return ResponseResult.okResult("操作成功");
        }
        return ResponseResult.okResult("操作成功");
    }
}
