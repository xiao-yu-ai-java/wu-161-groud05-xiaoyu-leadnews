package com.heima.behavior.interceptor;

import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserInfoThreadLocalUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author Administrator
 * @Date 2023/6/12
 **/
@Component
public class UserInfoInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1. 获取请求中的userId头信息
        String userId = request.getHeader("userId");
        if (StringUtils.isNotEmpty(userId) && !StringUtils.equals(userId, "0")) {
            //2. 设置userId到ThreadLocal中
            UserInfo userInfo = new UserInfo();
            userInfo.setUserId(Long.valueOf(userId));

            UserInfoThreadLocalUtil.set(userInfo);

            return super.preHandle(request, response, handler);
        }

        //如果没有登录, 也能进来
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        UserInfoThreadLocalUtil.remove();

        super.postHandle(request, response, handler, modelAndView);
    }
}