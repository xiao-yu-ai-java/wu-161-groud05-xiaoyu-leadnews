package com.heima.behavior.controller;

import com.heima.behavior.dto.FollowBehaviorDto;
import com.heima.behavior.service.ApFollowBehaviorService;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/v1/follow_behavior")
public class ApFollowBehaviorController {

    @Resource
    private ApFollowBehaviorService apFollowBehaviorService;

    /**
    *用户关注行为
    */
    @PostMapping(path = "/user_follow")
    public ResponseResult userFollow(@RequestBody FollowBehaviorDto dto) {
        return apFollowBehaviorService.userFollow(dto);
    }
}