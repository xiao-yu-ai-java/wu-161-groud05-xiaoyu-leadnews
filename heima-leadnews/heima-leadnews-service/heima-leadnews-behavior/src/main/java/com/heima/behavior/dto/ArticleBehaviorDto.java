package com.heima.behavior.dto;

import lombok.Data;

/**
 * @author Administrator
 */
@Data
public class ArticleBehaviorDto {

    /**
     * 文章ID
     */
    Long articleId;

    /**
     * 作者ID
     */
    Long authorId;
}