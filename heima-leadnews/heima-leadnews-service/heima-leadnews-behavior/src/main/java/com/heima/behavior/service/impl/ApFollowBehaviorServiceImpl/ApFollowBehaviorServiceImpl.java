package com.heima.behavior.service.impl.ApFollowBehaviorServiceImpl;

import com.heima.behavior.dto.FollowBehaviorDto;
import com.heima.behavior.pojo.ApFollowBehavior;
import com.heima.behavior.service.ApFollowBehaviorService;
import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserInfoThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class ApFollowBehaviorServiceImpl implements ApFollowBehaviorService {


    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private MongoTemplate mongoTemplate;
    /**
     * 用户关注行为接口
     * 这个接口即做关注又做取消业务
     * @param dto
     * @return
     */
    @Override
    public ResponseResult userFollow(FollowBehaviorDto dto) {

        //1.校验参数 : 作者id不能为空, 用户id不能为空:没登陆不能关注,操作方式只能是关注和取消
        if (dto ==null||dto.getAuthorId()==null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        short operation = dto.getOperation();
        if (operation !=0 && operation !=1){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        UserInfo userInfo = UserInfoThreadLocalUtil.get();
        if (userInfo==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //2.判断是关注还是取消操作
        //2.1关注 : 当前状态是未关注  -->关注: 新增数据到mongodb和redis
        //从redis中查是否关注了. 看有没有数据
        String follow_key = "USER_FOLLOW_"+ UserInfoThreadLocalUtil.get().getUserId();
        String fans_key= "USER_FANS_"+dto.getAuthorId();
        Boolean hasKey = stringRedisTemplate.opsForHash().hasKey(follow_key, dto.getArticleId().toString());  //大key:登录用户id, 小key作者id
        //未关注状态
        if (!hasKey&&operation==0){  //在redis查未查到 未关注, 且传过来的是关注参数
        //新增数据到mongodb
            ApFollowBehavior apFollowBehavior = new ApFollowBehavior();
            apFollowBehavior.setId(ObjectId.get().toHexString());
            apFollowBehavior.setUserId(userInfo.getUserId());
            apFollowBehavior.setArticleId(dto.getArticleId());
            apFollowBehavior.setFollowId(dto.getAuthorId()); //作者id
            apFollowBehavior.setCreatedTime(new Date());
            mongoTemplate.save(apFollowBehavior);  //保存直接保存实体类
        //新增数据到redis缓存 , 保存当前登录用户的关注作者列表
            stringRedisTemplate.opsForHash().put(follow_key,dto.getAuthorId().toString(),"1");
        //保存关注作者的粉丝列表

            stringRedisTemplate.opsForHash().put(fans_key,userInfo.getUserId().toString(),"1");

            return ResponseResult.okResult("关注成功");
        }
        //2.2取消  :当前状态是关注 -->取消关注 : 从mongodb和redis删除数据
        //在reis关注列表中能找到key就说明关注了, 再点就是取消 且传过来的状态的关注0
        if (hasKey&&operation==1){
            //删除mmondb数据  删除的当前登录用户id对作者id的关注数据
            Query query = Query.query(Criteria.where("userId").is(userInfo.getUserId()).and("followId").is(dto.getAuthorId()));
            mongoTemplate.remove(query,ApFollowBehavior.class);  //条件删除, 在这个实体类映射的表中删
            //删除reids缓存数据
            //1.从用户关注列表中移除作者id
            stringRedisTemplate.opsForHash().delete(follow_key,dto.getArticleId().toString());//大key带前缀, 小key
            //2.从作者粉丝列表表删除用户id
            stringRedisTemplate.opsForHash().delete(fans_key,userInfo.getUserId().toString());
            return ResponseResult.okResult("操作成功");
        }
        return ResponseResult.okResult("操作成功");
    }
}
