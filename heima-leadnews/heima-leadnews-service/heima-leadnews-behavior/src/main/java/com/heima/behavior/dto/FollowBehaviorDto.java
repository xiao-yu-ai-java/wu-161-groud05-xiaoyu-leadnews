package com.heima.behavior.dto;

import lombok.Data;

@Data
public class FollowBehaviorDto {
    //用户id不需要, 直接线程中取
    /**
     * 文章id
     */
    private Long articleId;

    /**
     * 关注的id
     */
    private Integer authorId;

    /**
     * 操作方式
     * 0  关注
     * 1  取消
     */
    private short operation;
}