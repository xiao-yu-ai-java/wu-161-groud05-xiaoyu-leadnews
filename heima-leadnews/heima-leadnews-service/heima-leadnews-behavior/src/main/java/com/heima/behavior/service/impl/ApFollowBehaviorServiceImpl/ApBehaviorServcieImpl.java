package com.heima.behavior.service.impl.ApFollowBehaviorServiceImpl;

import com.heima.behavior.dto.ArticleBehaviorDto;
import com.heima.behavior.service.ApBehaviorServcie;
import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserInfoThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class ApBehaviorServcieImpl implements ApBehaviorServcie {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 加载app端文章行为状态
     *
     * @return
     */
    @Override
    public ResponseResult loadArticleBehavior(ArticleBehaviorDto dto) {
        //1.检查参数 连作者id, 文章id 都没有咋知道关注没,
        if (dto == null || dto.getArticleId() == null || dto.getAuthorId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //1.1用户没登录不用回显
        UserInfo userInfo = UserInfoThreadLocalUtil.get();
        if (userInfo == null) {
            return ResponseResult.okResult(Collections.emptyMap());  //没影响, 返回一个空集合
        }
        //2.查询文章各种行为状态
        //定义变量 连接式定义变量  查到再修改它
        boolean isUnlike = false, isLike = false, isCollection = false, isFollow = false;
        //2.1用户点赞状态  mongodb和reids查都可以, 但是redis效率高
        //看redis中   大key中的小key:app用户id 存在否, 存在就是点过赞, 不存在就是没点
        String like_key = "USER_LIKES_" + dto.getArticleId();
        isLike = stringRedisTemplate.opsForHash().hasKey(like_key, userInfo.getUserId().toString());  //看是否有key  true 就是点赞过, false就是没点赞

        //2.2用户关注状态
        String follow_key = "USER_FOLLOW_"+ UserInfoThreadLocalUtil.get().getUserId();
        isFollow = stringRedisTemplate.opsForHash().hasKey(follow_key, dto.getAuthorId());
        //2.3用户收藏状态
        String collection_hash_key = "USER_COLLECTION_" + dto.getArticleId();
        isCollection = stringRedisTemplate.opsForHash().hasKey(collection_hash_key,userInfo.getUserId().toString() );
        //2.4用户不喜欢状态
        String unlike_hash_key = "USER_UNLIKES_" + dto.getArticleId();
        isUnlike = stringRedisTemplate.opsForHash().hasKey(unlike_hash_key, userInfo.getUserId().toString());
        //3.封装数据返回
        //返回结果样式{"isfollow":true,"islike":true,"isunlike":false,"iscollection":true}
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("isfollow", isFollow);
        resultMap.put("islike", isLike);
        resultMap.put("isunlike", isUnlike);
        resultMap.put("iscollection", isCollection);
        return ResponseResult.okResult(resultMap);
    }

}
