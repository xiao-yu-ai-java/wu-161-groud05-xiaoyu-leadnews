package com.heima.behavior.service;

import com.heima.behavior.dto.LikesBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;

public interface ApLikesBehaviorService {
    /**
     * 用户点赞接口
     * @param dto
     * @return
     */
    ResponseResult like(LikesBehaviorDto dto);
}
