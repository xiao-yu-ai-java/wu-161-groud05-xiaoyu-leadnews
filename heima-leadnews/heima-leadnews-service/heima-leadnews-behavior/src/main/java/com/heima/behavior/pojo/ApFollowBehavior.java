package com.heima.behavior.pojo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * APP关注行为表
 * </p>
 *
 * @author itheima2023-06-28 14:32:49


 */
@Data
@Document("ap_follow_behavior")  //映射哪一个
public class ApFollowBehavior implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    /**
     * 实体ID
     */
    private Long userId;

    /**
     * 文章ID
     */
    private Long articleId;

    /**
     * 关注用户ID
     */
    private Integer followId;

    /**
     * 关注时间
     */
    private Date createdTime;

}