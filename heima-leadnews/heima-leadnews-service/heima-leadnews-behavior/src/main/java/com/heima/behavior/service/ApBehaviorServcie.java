package com.heima.behavior.service;

import com.heima.behavior.dto.ArticleBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;

public interface ApBehaviorServcie {

    /**
     * 加载app端文章行为状态
     *
     * @return
     */
    ResponseResult loadArticleBehavior(ArticleBehaviorDto dto);
}
