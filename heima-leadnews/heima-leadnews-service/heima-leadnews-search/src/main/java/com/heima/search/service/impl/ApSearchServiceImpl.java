package com.heima.search.service.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserInfoThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.search.dto.UserSearchDto;
import com.heima.search.service.ApSearchService;
import com.heima.search.service.ApUserSearchService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ApSearchServiceImpl implements ApSearchService {

    @Resource
    private RestHighLevelClient restHighLevelClient;

    @Resource
    private ApUserSearchService apUserSearchService;


    /**
     * 根据关键词搜索文章列表
     * @param dto
     * @return
     */
    @Override
    public ResponseResult search(UserSearchDto dto) throws IOException {

        log.info("文章搜索服务业务代码开始执行, 线程:{}",Thread.currentThread().getName());
        //1.校验参数
        if (dto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //最小时间校验 没有就给默认值:当前时间
        Date minTime = dto.getMinBehotTime();
        minTime = minTime == null ? new Date() : minTime;
        //每页展示数据条数校验
        int size = dto.getPageSize();
        size = size <= 0 ? 10 : size;
        size = Math.min(size, 50);

        //保存用户搜索历史记录: 第一次搜索的过程中才需要保存, 后面是不需要保存的
        //问题一 : 业务代码耦合问题， 两个业务之间会相五影啊   :一保存搜索历史记录失败, 下面代码执行不下去了
        //问题二 :影响程序的执行效率，影响系统的否叶量和用户体验 先去调用mogdb再操作es, 操作时间很长
        //解决: 解除耦合,即使一个出问题了, 别一个不会受影响
        //即使保存记录出现了问题, 也不要影响核心业务. es .  还要提高效率
        //使用MQ外, 还可以使用线程池
        UserInfo userInfo = UserInfoThreadLocalUtil.get();
        if (dto.getFromIndex()==0&&StringUtils.isNotEmpty(dto.getSearchWords())&&userInfo != null){
             apUserSearchService.insert(userInfo.getUserId(),dto.getSearchWords());
         }

         /*子线程拿不到核心线程, 所以核心线程传进去 上上上上上上上上上*/



         /*万一保存搜索历史记录失败, 下面代码执行不下去了 */


        //2.创建ES请求对象
        SearchRequest request = new SearchRequest("app_article_info");

        //3.设置DSL参数
        //设置搜索条件 做拼接boolQuery.filter()  必须拼接boolQuery.must()
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        //3.1搜索关键词参数(根据上一页的最后一个文章的发布时间查询下一页数据)

        if (StringUtils.isEmpty(dto.getSearchWords())){
            boolQuery.must(QueryBuilders.matchAllQuery());
            request.source().query();
        }else {
            boolQuery.must(QueryBuilders.matchQuery("all",dto.getSearchWords()));

        }

        //3.2发布时间参数  多个查询条件不能单独写, query是覆盖 所以要用bool拼接
        /*request.source().query(QueryBuilders.rangeQuery("publishTime").lt(minTime));*/
        boolQuery.filter(QueryBuilders.rangeQuery("publishTime").lt(minTime));

        request.source().query(boolQuery);  //设置查询条件
        //3.3分页参数
        request.source().from(0).size(size);

        //3.4排序参数
        request.source().sort("publishTime", SortOrder.DESC);
        //3.5高亮参数\
        //高亮构建器对象
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        //设置里面相关属性
        highlightBuilder.field("title");  //对那个字段做gaol
        highlightBuilder.preTags("<font style='color:red; font-size: inherit;'>");  //设置前后缀
        highlightBuilder.postTags("</font>");
        //高亮字段和搜索字段不同名, 想要展示高亮结果集, 就要设置
        highlightBuilder.requireFieldMatch(false);

        request.source().highlighter(highlightBuilder);

        //4.发送请求, 获取响应
        SearchResponse response = restHighLevelClient.search(request, RequestOptions.DEFAULT);
        //5.解析响应结果
        //5.1 获取命中数据
        SearchHits hits = response.getHits();
        //5.2获取命中数据列表
        SearchHit[] hitsHits = hits.getHits();
        //5.3遍历循环获取每一条命中数据
        List<Map<String, Object>> mapList = Arrays.stream(hitsHits).map(SearchHit -> {
            //获取命中数据的原始文档内容
            Map<String, Object> source = SearchHit.getSourceAsMap();
            //获取文档的高亮结果集 高亮字段可能是个集合
            Map<String, HighlightField> highlightFields = SearchHit.getHighlightFields();
            if (!CollectionUtils.isEmpty(highlightFields)) {
                //获取指定字段名称的高亮结果
                HighlightField titleField = highlightFields.get("title");
                if (titleField != null) {
                    Text[] fragments = titleField.getFragments(); //高亮片段
                    String highlightResult = StringUtils.join(fragments);  //分割高亮片段
                    source.put("h_title", highlightResult); //封装高亮文本片段到原始文档, 返回
                }
            }
            return source;
        }).collect(Collectors.toList());

        return ResponseResult.okResult(mapList);
    }
}
