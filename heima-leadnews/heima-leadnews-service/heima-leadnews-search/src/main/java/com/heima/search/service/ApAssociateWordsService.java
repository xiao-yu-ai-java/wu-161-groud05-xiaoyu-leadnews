package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.search.dto.UserSearchDto;

import java.io.IOException;

public interface ApAssociateWordsService {

    /**
     自动补全
     @param userSearchDto
     @return
     */
    ResponseResult search(UserSearchDto userSearchDto) throws IOException;

}