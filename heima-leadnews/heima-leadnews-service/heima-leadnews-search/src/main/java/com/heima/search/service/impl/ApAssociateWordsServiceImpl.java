package com.heima.search.service.impl;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.search.dto.UserSearchDto;
import com.heima.search.service.ApAssociateWordsService;
import net.bytebuddy.asm.Advice;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApAssociateWordsServiceImpl implements ApAssociateWordsService {


    @Resource
    private RestHighLevelClient restHighLevelClient;


    /**
     * 自动补全
     *
     * @param userSearchDto
     * @return
     */
    @Override
    public ResponseResult search(UserSearchDto userSearchDto) throws IOException {
        //1.创建请求对象
        SearchRequest request = new SearchRequest("app_article_info");
        //2.封装DSL请求参数
        request.source().suggest(new SuggestBuilder().addSuggestion(
                //自动补全名称, 后面根据这个名称获取对应的补全结果
                "suggestions", SuggestBuilders.completionSuggestion("suggestion") //补全的字段
                        .prefix(userSearchDto.getSearchWords())   //关键字
                        .skipDuplicates(true)
                        .size(10)
        ));
        //3.发请求
        SearchResponse response = restHighLevelClient.search(request, RequestOptions.DEFAULT);
        //4.解析结果
        //4.1根据名称获取补全结果
        Suggest suggest = response.getSuggest();
        CompletionSuggestion suggestions = suggest.getSuggestion("suggestion");
        //4.2获取options
        List<CompletionSuggestion.Entry.Option> options = suggestions.getOptions();
        //4.3遍历得到text
        List<String> list = options.stream().map(option -> option.getText().string()).collect(Collectors.toList());


        return ResponseResult.okResult(list);
    }
}
