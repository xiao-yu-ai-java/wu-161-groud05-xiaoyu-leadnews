package com.heima.search.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.search.dto.UserSearchDto;
import com.heima.search.service.ApSearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

@RestController
@RequestMapping(path = "/api/v1/article/search")
@Api(value = "App端搜索相关Api",tags="搜索")
public class ApSearchController {


    @Resource
    private ApSearchService apSearchService;


    /**
     * 根据关键词搜索文章列表
     * @param dto
     * @return
     */
    @PostMapping("/search")
    @ApiOperation("根据关键词搜索文章列表")
    public ResponseResult search(@RequestBody UserSearchDto dto) throws IOException {
        return apSearchService.search(dto);
    }
}
