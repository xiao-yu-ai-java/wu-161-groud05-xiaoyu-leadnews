package com.heima.search.service.impl;

import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserInfoThreadLocalUtil;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.search.dto.HistorySearchDto;
import com.heima.search.pojo.ApUserSearch;
import com.heima.search.service.ApUserSearchService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class ApUserSearchServiceImpl implements ApUserSearchService {

    @Resource
    private MongoTemplate mongoTemplate;

    /*在什么时候要调用? 用户在搜素的时候*/
    @Override  // 用户id
    public void insert(Long id,String keyword) {
        log.info("保存用户搜索历史记录业务开始执行, 线程:{}",Thread.currentThread().getName());

        /*开启了子线程执行了这段代码, 和外面的核心线程不一样*/
        //1.检验参数
        if (StringUtils.isEmpty(keyword)){
            return;
        }
        /*把数据存到mondb中(每个用户最多保存10条记录)*/
        //2.根据关键词查询用户搜索历史记录(在mondb 的数据库中)  Criteria标准
        /*UserInfo userInfo = UserInfoThreadLocalUtil.get();  //线程局部变量获取用户id
        if (userInfo == null){  //没有登录 不用保存 直接返回
            return;
        }*/

        Query query = Query.query(Criteria.where("keyword").is(keyword).and("userId").is(id));  //根据关键字和用户id来查找ap_user_search库中的数据
        ApUserSearch apUserSearch = mongoTemplate.findOne(query, ApUserSearch.class);
        //2.1如果这个关键字之前被搜索过,更新搜索时间
        if (apUserSearch != null){
            apUserSearch.setCreatedTime(new Date());
            //跟新到数据库
            mongoTemplate.save(apUserSearch);
        }
        //2.2如果不不存在(没有搜过), 插入搜索历史记录
        //3.查询用户的搜索历史记录数据(当前用户有多少搜索记录, 直接根据用户 id查) 这里query和上面的不一样了
        query = Query.query(Criteria.where("userId").is(id)).with(Sort.by("createdTime").ascending());

        List<ApUserSearch> userSearchList = mongoTemplate.find(query, ApUserSearch.class);
        //3.1 如果数量>=10. 删除最早(最之前)的记录, 插入新纪录(下面)  (最早时间最小)
        if (userSearchList.size()>=10){
            //获取最早的用户搜索历史记录
            ApUserSearch lastApUserSearch = userSearchList.get(0);
            //删除最早的用户搜索历史记录
            mongoTemplate.remove(lastApUserSearch);

        }
        //3.2如果数量<10, 插入新纪录 保存到mondb
        apUserSearch =  new ApUserSearch();

        apUserSearch.setUserId(id.intValue());
        apUserSearch.setId(ObjectId.get().toHexString());
        apUserSearch.setKeyword(keyword);
        apUserSearch.setCreatedTime(new Date());
        mongoTemplate.save(apUserSearch);

    }


    /**
     * app端用户搜索历史记录列表查询
     * @return
     */
    @Override
    public ResponseResult<List<ApUserSearch>> load() {
        //1.获取当前登录用户信息
        UserInfo userInfo = UserInfoThreadLocalUtil.get();
        if (userInfo == null){
            return ResponseResult.okResult(Collections.emptyList());
        }

        //2.从mongoDB查询用户搜索历史记录
        Query query = Query.query(Criteria.where("userId").is(userInfo.getUserId())).with(Sort.by("createdTime").descending());
        List<ApUserSearch> userSearchList = mongoTemplate.find(query, ApUserSearch.class);
        return ResponseResult.okResult(userSearchList);
    }



    /**
     * 根据id删除用户搜索历史记录
     * @return
     */
    @Override
    public ResponseResult deleteById(HistorySearchDto historySearchDto) {

        //1.检验参数
        if (historySearchDto.getId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.根据用户id和数据id查询搜索记录
        UserInfo userInfo = UserInfoThreadLocalUtil.get();
        if (userInfo==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_ADMIND);
        }

        //自己只能看自己的历史记录
        Query query = Query.query(Criteria.where("userId").is(userInfo.getUserId()).and("_id").is(historySearchDto.getId()));
        ApUserSearch apUserSearch = mongoTemplate.findOne(query, ApUserSearch.class);
        //3.删除搜索历史记录
        mongoTemplate.remove(apUserSearch);
        return ResponseResult.okResult("操作成功");
    }
}
