package com.heima.search.config;


import com.heima.common.exception.ExceptionCatch;
import com.heima.common.jackson.JacksonConfig;
import com.heima.common.swagger.SwaggerConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


/**
 * swagger 配置类  用来扫包
 * 本来启动类只扫描本包和子包, 这里导入了swagger类, 就会加载swagger
 */
@Configuration
@Import({SwaggerConfiguration.class, ExceptionCatch.class, JacksonConfig.class})
public class SystemConfigInit {
}
