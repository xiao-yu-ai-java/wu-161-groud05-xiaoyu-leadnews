package com.heima.search.kafka;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.heima.common.constants.KafkaMessageConstants;
import com.heima.model.common.search.vo.SearchArticleVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

@Component
@Slf4j
public class ArticleSyncListener {

    @Resource
    private RestHighLevelClient restHighLevelClient;

    /**
     * 数据同步的监听器
     *
     * @param record
     */
    @KafkaListener(topics = KafkaMessageConstants.AP_ARTICLE_ES_SYNC_TOPIC, groupId = "ARTICLE_ES_SYNC")
    public void articleSync(ConsumerRecord<String, String> record) {
        //1.接收同步消息  value里面就是文章数据,
        String value = record.value();
        log.info("接收到文章数据同步消息，消息内容", value);
        if (StringUtils.isEmpty(value)) {
            return;
        }

        try {
            //2.创建请求对象
            IndexRequest request = new IndexRequest("app_article_info");  //指定对应的索引
            //3.封装DsL参数  id :文档id
            //将字符串json转化为json对象   用这个发, 用这个转SearchArticleVo
            SearchArticleVo searchArticleVo = JSON.parseObject(value, SearchArticleVo.class);
            //文档id , 内容source
            request.id(searchArticleVo.getId().toString()).source(value, XContentType.JSON);
            //4.发送请求将数据写入到ES
            restHighLevelClient.index(request, RequestOptions.DEFAULT);
            log.error("同步数据到ES索引库成功_____________");
        } catch (IOException e) {
            log.error("同步数据到ES索引库失败,失败原因:{}",e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
