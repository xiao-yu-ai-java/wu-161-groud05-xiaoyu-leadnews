package com.heima.search.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.search.dto.HistorySearchDto;
import com.heima.search.pojo.ApUserSearch;
import com.heima.search.service.ApUserSearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/v1/history")
@Api(value = "app端用户搜索历史记录",tags = "搜索")
public class ApUserSearchController {

    @Resource
    private ApUserSearchService apUserSearchService;

    /**
     * app端用户搜索历史记录列表查询
     * @return
     */
    @PostMapping("/load")
    @ApiOperation(value = "app端用户搜索历史记录列表查询")
    public ResponseResult<List<ApUserSearch>> load(){
          return  apUserSearchService.load();
    }


    /**
     * 根据id删除用户搜索历史记录
     * @return
     */
    @PostMapping("/del")
    @ApiOperation(value = "根据id删除用户搜索历史记录")
    public ResponseResult deleteById(@RequestBody HistorySearchDto historySearchDto){
        return apUserSearchService.deleteById(historySearchDto);
    }
}
