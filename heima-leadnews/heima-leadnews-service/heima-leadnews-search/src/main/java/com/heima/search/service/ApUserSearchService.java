package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.search.dto.HistorySearchDto;
import com.heima.search.pojo.ApUserSearch;
import org.springframework.scheduling.annotation.Async;

import java.util.List;

public interface ApUserSearchService {

    /**
     * 保存用户搜索记录
     * @param keyword
     */
    @Async("taskExecutor")
    public void insert(Long id,String keyword);

    /**
     * app端用户搜索历史记录列表查询
     * @return
     */
    ResponseResult<List<ApUserSearch>> load();


    /**
     * 根据id删除用户搜索历史记录
     * @return
     */
    ResponseResult deleteById(HistorySearchDto historySearchDto);
}
