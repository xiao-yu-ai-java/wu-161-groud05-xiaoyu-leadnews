package com.heima.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.user.pojo.ApUserRealname;


/**
 * APP实名认证信息表(ApUserRealname)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-26 23:25:59
 */
public interface ApUserRealnameMapper extends BaseMapper<ApUserRealname> {

}

