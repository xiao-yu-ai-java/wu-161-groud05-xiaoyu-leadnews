package com.heima.user.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.user.dto.LoginDto;
import com.heima.user.service.ApUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(path = "/api/v1/login")
@Api(value = "App端用户登录Api",tags = "APP端用户")
public class ApUserLoginController {

    @Resource
    private ApUserService apUserService;


    /**
     * App端用户登录接口
     * @param loginDto
     * @return
     */
    @PostMapping(path = "/login_auth")
    @ApiOperation("App端用户登录接口")
    public ResponseResult login(@RequestBody LoginDto loginDto){
       return apUserService.login(loginDto);
    }
}
