package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.common.exception.CustomException;
import com.heima.common.thread.UserInfoThreadLocalUtil;
import com.heima.feign.api.WmUserFeignClient;
import com.heima.minio.MinIOTemplate;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.user.dto.AuthDto;
import com.heima.model.common.user.pojo.ApUser;
import com.heima.model.common.user.pojo.ApUserRealname;
import com.heima.model.common.wemedia.pojo.WmUser;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.mapper.ApUserRealnameMapper;
import com.heima.user.service.ApUserRealnameService;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.core.pattern.RootThrowablePatternConverter;
import org.springframework.beans.BeanUtils;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * APP实名认证信息表(ApUserRealname)表服务实现类
 * 导入
 * @author makejava
 * @since 2023-06-26 23:26:00
 */
@Service("apUserRealnameService")
public class ApUserRealnameServiceImpl extends ServiceImpl<ApUserRealnameMapper, ApUserRealname> implements ApUserRealnameService {


        @Resource
        private MinIOTemplate minIOTemplate;

        @Resource
        private ApUserRealnameMapper apUserRealnameMapper;

       @Resource
       private ApUserMapper apUserMapper;



    @Resource
    private WmUserFeignClient wmUserFeignClient;


        /**
         * 提交用户实名认证信息
         * @param font_image
         * @param back_image
         * @param hold_image
         * @param live_image
         * @param name
         * @param idno
         * @return
         * @throws IOException
         */
    @Override
    public ResponseResult submit(MultipartFile font_image, MultipartFile back_image, MultipartFile hold_image, MultipartFile live_image, String name, String idno) throws IOException {
            //1. 校验参数
            if (StringUtils.isEmpty(name) || StringUtils.isEmpty(idno)) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
            }
            if (font_image.isEmpty() || back_image.isEmpty() || hold_image.isEmpty() || live_image.isEmpty()) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE, "缺失认证信息");
            }
            //2. 上传提交的照片数据到minio
            //2.1 身份证正面上传
            //获取原始路径名字
            String filename = font_image.getOriginalFilename();
            //用UUID拼接后缀  lastIndexOf 截取那个点后缀.jpg
            filename = UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf("."));
            //上传 : 前缀(空 minio中前缀为空), 唯一路径名字, 流
            String font_url = minIOTemplate.uploadImgFile("", filename, font_image.getInputStream());

            //2.2 身份证反面上传
            filename = back_image.getOriginalFilename();
            filename = UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf("."));
            String back_url = minIOTemplate.uploadImgFile("", filename, back_image.getInputStream());

            //2.3 手持照片上传
            filename = hold_image.getOriginalFilename();
            filename = UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf("."));
            String hold_url = minIOTemplate.uploadImgFile("", filename, hold_image.getInputStream());

            //2.4 手持照片上传
            filename = live_image.getOriginalFilename();
            filename = UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf("."));
            String live_url = minIOTemplate.uploadImgFile("", filename, live_image.getInputStream());

            //3. 封装数据保存到数据 --- 构建者模式进行数据封装
            ApUserRealname apUserRealname = ApUserRealname.builder()
                    .userId(UserInfoThreadLocalUtil.get().getUserId())  //谁提交的
                    .name(name)  //用户名字
                    .idno(idno)  //身份证
                    .fontImage(font_url)  //图片
                    .backImage(back_url)
                    .holdImage(hold_url)
                    .liveImage(live_url)
                    .status(1)  //状态 待审核
                    .createdTime(new Date())
                    .updatedTime(new Date())
                    .build();

            //app端用户提交身份认证, 需要保存到数据库表中
            //接下来审核通过就调用自媒体开通自媒体账号, 不通过就修改用户微服务审核状态(用户实名失败)
            apUserRealnameMapper.insert(apUserRealname);

            return ResponseResult.okResult("操作成功");
        }


        /**
         * 按照状态分页查询用户列表
         * @param authDto
         * @return
         */

        @Override
        public ResponseResult queryList(AuthDto authDto) {
                //1. 校验参数
                authDto.checkParam();
                //2. 封装分页查询条件 , 执行查询
                //2.1 构建分页查询page对象  page size
                Page<ApUserRealname> page  =  new Page<>(authDto.getPage(), authDto.getSize());
                //2.2查询条件
                LambdaQueryWrapper<ApUserRealname> wrapper = Wrappers.<ApUserRealname>lambdaQuery();
                if (authDto.getStatus() != null) {
                        wrapper.eq(ApUserRealname::getStatus, authDto.getStatus());
                }
                //2.3 调用mapper层, 条件查询
                apUserRealnameMapper.selectPage(page, wrapper);

                Long current = page.getCurrent();  //当前页
                Long size = page.getSize(); //本页大小
                Long total = page.getTotal(); //总条数
                //3. 封装数据返回
                ResponseResult responseResult = new PageResponseResult(current.intValue(), size.intValue(), total.intValue());
                responseResult.setData(page.getRecords());

                return responseResult;
        }




        //++++++++++++++++++++++++++++++++++++++++++


        /**
         * 审核通过或失败 都要修改实名认证表中的状态
         * 跟新实名认证审核状态
         * @param dto
         * @return
         */

        @GlobalTransactional(rollbackFor = Exception.class)
        @Override
        public ResponseResult updateAuthStatus(AuthDto dto) {
            //1.检验参数 没用户id都不知道存哪里
            if (dto == null || dto.getId() == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            //status 状态 : 要么为2(审核通过) , 要么为9(审核失败) 不能为其他
            if (dto.getStatus() != 2 && dto.getStatus() !=9){
                return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            }
            //2.1审核失败-->修改数据库实名认证表审核状态
            //2.2审核成功-->修改数据库实名认证表审核状态
            ApUserRealname apUserRealname = apUserRealnameMapper.selectById(dto.getId()); //用户id
            if (apUserRealname ==null){  //先保存, 再审核 数据库数据不存在
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
            }

            apUserRealname.setStatus(dto.getStatus().intValue());
            //审核失败就有错误信息 , 传过来
            if (StringUtils.isNotEmpty(dto.getMsg())){
                apUserRealname.setReason(dto.getMsg());
            }
            apUserRealnameMapper.updateById(apUserRealname);

            //2.2审核成功-->修改数据库实名认证表审核状态-->调用自媒体服务保存自媒体账号信息
            if (dto.getStatus()==9){
                //自媒体用户表wm_user里面的数据, 来自ap_user表
                //通过ap_user_realname表用户id, 可以去ap_user表查用户信息, 封装到自媒体用户信息表wm_user
                createWmUser( apUserRealname.getUserId());
            }
                return ResponseResult.okResult("操作成功");
        }


    /**
     * 创建自媒体用户账户
     *调用自媒体服务保存自媒体账号信息
     * @param userId
     */
    private void createWmUser(Long userId) {
        //3.去数据库中查用户信息
        ApUser apUser = apUserMapper.selectById(userId);

        if (apUser == null){
            throw new CustomException(AppHttpCodeEnum.DATA_NOT_EXIST);
        }
        //2.封装数据 : 要WmUser对象  apUserMapper
        WmUser wmUser = new WmUser();
        BeanUtils.copyProperties(apUser,wmUser);
        wmUser.setAvatar(apUser.getImage());
        wmUser.setNickname(apUser.getNicknme());
        wmUser.setStatus(1);  //默认启用
        wmUser.setType(1);  //普通账号
        wmUser.setApUserId(apUser.getId());

       // wmUser.setUsername(apUser.getUsername());  //从哪里来  去ap_user app用户表中查


        //1.调用feign保存数据
        ResponseResult result = wmUserFeignClient.save(wmUser);

        if (result.getCode() != 200){
            throw new CustomException(AppHttpCodeEnum.SERVER_ERROR);
        }
    }
}

