package com.heima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.user.dto.LoginDto;
import com.heima.model.common.user.pojo.ApUser;

/**
 * APP用户表(ApUser)表服务接口
 *
 * @author makejava
 * @since 2023-06-22 21:26:33
 */
public interface ApUserService extends IService<ApUser> {

    /**
     * App端用户登录接口
     * @param loginDto
     * @return
     */
    ResponseResult login(LoginDto loginDto);


}

