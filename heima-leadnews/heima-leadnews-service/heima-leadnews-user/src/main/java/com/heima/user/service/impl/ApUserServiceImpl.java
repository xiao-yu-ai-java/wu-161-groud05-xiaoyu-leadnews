package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.user.dto.LoginDto;
import com.heima.model.common.user.pojo.ApUser;
import com.heima.user.mapper.ApUserMapper;

import com.heima.user.service.ApUserService;
import com.heima.utils.common.AppJwtUtil;
import com.heima.utils.common.MD5Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * APP用户表(ApUser)表服务实现类
 *
 * @author makejava
 * @since 2023-06-22 21:26:33
 */
@Service("apUserService")
public class ApUserServiceImpl extends ServiceImpl<ApUserMapper, ApUser> implements ApUserService {


    @Resource
    private ApUserMapper apUserMapper;


    /**
     * App端用户登录接口
     * @param loginDto
     * @return
     */
    @Override
    public ResponseResult login(LoginDto loginDto) {
        //1.判断用户是正常登录还是匿名登录
        //匿名登录
        if (StringUtils.isEmpty(loginDto.getPhone())&&StringUtils.isEmpty(loginDto.getPassword())) {
            //得到0号token
            String token = AppJwtUtil.getToken(0L);
            //返回一个map
            Map<String, Object> data = new HashMap<>();
            data.put("token",token);
            return ResponseResult.okResult(data);
        }
        //正常登录
        //2.校验用户名和密码
        if (StringUtils.isEmpty(loginDto.getPhone())||StringUtils.isEmpty(loginDto.getPassword())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //3.根据用户手机号查询用户信息(盐. 密文密码)
        ApUser apUser = apUserMapper.selectOne(Wrappers.<ApUser>lambdaQuery().eq(ApUser::getPhone, loginDto.getPhone()));
        if (apUser == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST);
        }
        //判断用户状态是否可用
        if (apUser.getStatus()!=0){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_ADMIND,"账号状态异常, 请联系管理员");
        }
        //4.使用输入的密码加上盐, 生成一个密文字符串
        String withSalt = MD5Utils.encodeWithSalt(loginDto.getPassword(), apUser.getSalt());
        //5.使用生成的密文字符串和数据库返回的密文密码进行比对
         if (StringUtils.equals(withSalt,apUser.getPassword())){
             //5.1比对不一致登录失败
             return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
         }

        //5.2比对一致, 登录成功, 返回token
        Map<String, Object> data = new HashMap<>();
        data.put("token",AppJwtUtil.getToken(apUser.getId()));

        apUser.setSalt(null);
        apUser.setPassword(null);
        data.put("user",apUser);
        return ResponseResult.okResult(data);

    }

    public static void main(String[] args) {
        String withSalt = MD5Utils.encodeWithSalt(
                "123456", "abcd");
        System.out.println(withSalt);
    }




}

