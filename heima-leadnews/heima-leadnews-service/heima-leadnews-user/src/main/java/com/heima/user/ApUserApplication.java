package com.heima.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@MapperScan("com.heima.user.mapper")
@EnableFeignClients(basePackages = "com.heima.feign.api")
public class ApUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApUserApplication.class, args);
    }
}