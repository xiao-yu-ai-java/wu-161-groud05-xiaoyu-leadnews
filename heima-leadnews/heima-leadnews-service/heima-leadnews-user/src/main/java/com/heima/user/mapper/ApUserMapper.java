package com.heima.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.user.pojo.ApUser;


/**
 * APP用户表(ApUser)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-22 21:26:31
 */
public interface ApUserMapper extends BaseMapper<ApUser> {

}

