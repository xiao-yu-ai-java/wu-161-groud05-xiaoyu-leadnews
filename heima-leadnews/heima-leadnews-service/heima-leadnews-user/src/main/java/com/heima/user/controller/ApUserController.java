package com.heima.user.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.user.pojo.ApUser;
import com.heima.user.service.ApUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1/user")
public class ApUserController {


    @Autowired
    private ApUserService apUserService;

    /**
     * 根据用户id查询用户信息
     */
    @GetMapping("/{id}")
    public ResponseResult<ApUser> findUserById(@PathVariable("id") Long id) {
        ApUser apUser = apUserService.getById(id);
        apUser.setSalt(null);
        apUser.setPassword(null);
        return ResponseResult.okResult(apUser);
    }
}
