package com.heima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.user.dto.AuthDto;
import com.heima.model.common.user.pojo.ApUserRealname;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * APP实名认证信息表(ApUserRealname)表服务接口
 *
 * @author makejava
 * @since 2023-06-26 23:26:00
 */
public interface ApUserRealnameService extends IService<ApUserRealname> {

    /**
     * 提交用户实名认证信息
     * 参数名=参数值&参数名=参数值&参数名=参数值
     * idno 身份证号
     * @return
     */
    ResponseResult submit(MultipartFile font_image, MultipartFile back_image, MultipartFile hold_image, MultipartFile live_image, String name, String idno) throws IOException;

    /**
     * 按照状态分页查询用户列表
     * @param dto
     * @return
     */
    public ResponseResult queryList(AuthDto dto);

    /**
     * 审核通过
     * 都要修改mongodB数据库表ap_user_realname中的状态
     * @param dto
     * @return
     */
    ResponseResult updateAuthStatus(AuthDto dto);
}

