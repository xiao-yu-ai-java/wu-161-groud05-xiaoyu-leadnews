package com.heima.comment.pojo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * APP评论信息点赞
 */
@Data
@Document("ap_comment_like")
public class ApCommentLike {

    /**
     * id
     */
    private String id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 评论id
     */
    private String targetId;

    /**
     * 创建时间
     */
    private Date createdTime;
}