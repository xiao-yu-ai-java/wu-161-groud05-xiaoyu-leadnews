package com.heima.comment.interceptor;

import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserInfoThreadLocalUtil;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;

/**
 * 拦截feign的远程调用请求
 * 拦截的是发请求的feign操作
 * @Author Administrator
 * @Date 2023/6/26
 **/
//拦截的是发请求的feign操作
@Component
public class FeignHeaderInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        UserInfo userInfo = UserInfoThreadLocalUtil.get();
        if (userInfo!=null){
            requestTemplate.header("userId", userInfo.getUserId().toString());
        }
    }
}