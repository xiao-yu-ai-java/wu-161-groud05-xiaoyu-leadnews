package com.heima.comment.dto;

import lombok.Data;

/**
 * 点赞评论回复:CommentRepayLikeDto
 */
@Data
public class CommentRepayLikeDto {

   /**
    * 评论id
    */
   private String commentRepayId;

   /**
    * 0：点赞
    * 1：取消点赞
    */
   private Short operation;
}