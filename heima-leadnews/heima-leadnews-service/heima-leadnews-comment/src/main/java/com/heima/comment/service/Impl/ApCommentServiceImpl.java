package com.heima.comment.service.Impl;

import com.alibaba.fastjson.JSON;
import com.heima.baidu.BaiduAiTemplate;
import com.heima.baidu.pojo.ScanResult;
import com.heima.comment.pojo.ApComment;
import com.heima.comment.pojo.ApCommentLike;
import com.heima.comment.service.ApCommentService;
import com.heima.comment.vo.ApCommentVo;
import com.heima.common.thread.UserInfo;
import com.heima.common.thread.UserInfoThreadLocalUtil;
import com.heima.feign.api.ApUserFeignClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.user.pojo.ApUser;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ApCommentServiceImpl implements ApCommentService {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private ApUserFeignClient apUserFeignClient;

    @Resource
    private MongoTemplate mongoTemplate;

    @Resource
    private BaiduAiTemplate baiduAiTemplate;


    /**
     * app端发表评论
     *
     * @param content
     * @param targetId 评论目标id, 评论的是文章:这里就是文字章id ,评论的是视频:这里就是视频id
     * @return
     */
    @Override
    public ResponseResult saveComment(String content, String targetId) {
        //参数在外面已经校验
        //1.获取登录用户Id
        UserInfo userInfo = UserInfoThreadLocalUtil.get();
        if (userInfo == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //2，获取登录用户信息
        //从redis中获取用户信息(评论用户的信息会缓存到reids)
        String user_info_key = "USER_INFO_" + userInfo.getUserId();
        String user_info_json = stringRedisTemplate.opsForValue().get(user_info_key); //用户信息的json

        ApUser apUser = null;
        if (StringUtils.isNotEmpty(user_info_json)) {
            //如果用户信息不为空, 就把用户信息转为java对象 stringJson-->java对象
            apUser = JSON.parseObject(user_info_json, ApUser.class);
        } else {
            //如果redis 中不存在信息, 就去用户服务查 再存到redis
            ResponseResult<ApUser> result = apUserFeignClient.findUserById(userInfo.getUserId());
            if (result.getCode() != 200 && result.getData() == null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
            }
            apUser = result.getData();
            //再存到redis
            stringRedisTemplate.opsForValue().set(user_info_key, JSON.toJSONString(apUser), Duration.ofHours(24));

        }
        /**
         * 直接这里审核不合适, 调百度ai影响效率
         */
        //TODO :拿到用户信息了, 再做审核
        //TODO 直接保存评论信息到数据库, 发送异步审核消息进行审核 , 审核完毕之后如果审核失败, 删除评论 异步审核
        ScanResult scanResult = baiduAiTemplate.textScan(content);
        if (scanResult.getConclusionType() != 1) {  //1是合规
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "评论内容中包含敏感信息");
        }


        //3.封装评论数据--->存到mondb
        ApComment apComment = new ApComment();
        apComment.setId(ObjectId.get().toHexString());
        apComment.setUserId(userInfo.getUserId());
        apComment.setNickName(apUser.getNicknme());
        apComment.setTargetId(targetId);
        apComment.setContent(content);
        apComment.setImage(apUser.getImage());
        apComment.setLikes(0);
        apComment.setReply(0);
        apComment.setCreatedTime(new Date());
        apComment.setUpdatedTime(new Date());
        //4. 保存评论数据到mongodb
        mongoTemplate.save(apComment);


        return ResponseResult.okResult("评论成功");
        //发布要做审核
    }


    /**
     * 对评论点赞
     * 方法做一个通用的 主要由于targetId可以变, 可以是评论点赞id 评论回复点赞id
     * 可以对评论点赞, 也可以对评论回复点赞
     */
    @Override
    public ResponseResult like(String targetId, Short operation) {
        //1. 获取当前登录用户信息  只有登录了才能做点赞和取消赞
        UserInfo userInfo = UserInfoThreadLocalUtil.get();
        if (userInfo == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //2. 判端当前用户是否点过赞  redis存的是评论id   ismember 是否是成员
        String user_like_key = "COMMENT_LIKE_" + userInfo.getUserId();
        Boolean isMember = stringRedisTemplate.opsForSet().isMember(user_like_key, targetId);

        //3. 之前未点赞 ---->点赞 : 新点赞数据到mongodb和Redis  (点赞)
        if (!isMember && operation == 0) {

            //1.新增点赞数据到mongodb
            //评论点赞集合
            ApCommentLike apCommentLike = new ApCommentLike();
            apCommentLike.setId(ObjectId.get().toHexString());
            apCommentLike.setTargetId(targetId);
            apCommentLike.setUserId(userInfo.getUserId());
            apCommentLike.setCreatedTime(new Date());
            mongoTemplate.save(apCommentLike);
            //评论集合点赞集合+1  更新评论点赞数量+1
            //新增点赞数据到redis 字段Integer likes
            //mongo中根据评论id查评论
            //TODO 在并发环境下可能会出现点赞数量混乱的情况 分了3步, 在第二步有其他线程插进来 不能枷锁, 加锁只能有一个用户点, 其他不能点, 对性能影响大  希望3步变成一个原子性操作  :加乐观锁
            /**
             * 商品扣减库存为例 :
             * 1. 查询商品详情   select * from product where pid = 1
             * 2. 扣减商品库存   stock  =  10-1
             * 3. 更新最新的商品库存数据到数据 update product set stock = 9 where pid = 1
             * 会出现库存超卖问题
             * 解决方案 :
             * update product set stock = stock - num , version = version +1  where pid = 1 and version = 1
             *  stock = 0
             *  num 5 :  update product set stock = stock - num  where pid = 1 and stock - num >= 0
             *  num 5 :  update product set stock = stock - num  where pid = 1 and stock - num >= 0
             *
             * ApComment comment = mongoTemplate.findById(targetId, ApComment.class);
             * comment.setLikes(comment.getLikes() + 1);
             * mongoTemplate.save(comment);
             *
             * 要把这3个操作变成原子性, 就是变成一个查询语句
             * update ap_comment set likes = likes+1 where id = #{id}
             */
             /*
             ApComment comment = mongoTemplate.findById(targetId, ApComment.class);
             comment.setLikes(comment.getLikes() + 1);
             mongoTemplate.save(comment);
             */
            //目的:  修改评论like数量加一  先根据条件查, 再用inc函数加一
            Query query = Query.query(Criteria.where("is").is(targetId));
            Update update = new Update();
            update.inc("likes", 1);
            mongoTemplate.updateFirst(query, update, ApComment.class);

            //2.新增点赞数据到redis
            stringRedisTemplate.opsForSet().add(user_like_key, targetId);

        }else if (isMember && operation == 1) {

        //4， 之前点赞过 ---->取消点赞 : Mmongodb和Redis删除数据 (取消点赞)
        //4.1从Mmongodb删除数据
        //当前用户对这个目标的点赞数据

            Query query = Query.query(Criteria.where("userId").is(userInfo.getUserId()).and("targetId").is(targetId));
            mongoTemplate.remove(query, ApCommentLike.class);
            //评论集合中点赞数据 -1
            query = Query.query(Criteria.where("is").is(targetId));
            Update update = new Update();
            update.inc("likes", -1);
            mongoTemplate.updateFirst(query, update, ApComment.class);

            //4.2redis删除数据
            stringRedisTemplate.opsForSet().remove(user_like_key, targetId);

        }

        /**
         * 点赞数量
         */
        ApComment comment = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(targetId)), ApComment.class);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("likes", comment.getLikes());

        return ResponseResult.okResult("操作成功");
    }


    /**
     * 加载(查询文章)评论列表
     * 通过文章id查询 评论列表
     *
     * @param targetId 目标id
     *                 minTime  上一页的最小时间
     *                 size     每页展示的数据条数
     * @return
     */
    @Override
    public ResponseResult<List<ApCommentVo>> load(String targetId, Date minTime, Integer size) {

        //1.根据目标id查询目标下的评论列表  :去哪查? 去mondb查
        //分装查询条件 根据targetId查, 评论创建时间要小于最小时间(上一页最后一个时间), 限制大小limit  排序
        Query query = Query.query(Criteria.where("targetId").is(targetId).and("createdTime").lt(minTime))
                .with(Sort.by("createdTime").descending())
                .limit(size);

        List<ApComment> commentList = mongoTemplate.find(query, ApComment.class);

        //2.判断查询出来的评论列表中, 用户点赞的有那些 : 点赞是在redis, set集合中存的
        //2.1没登录不用查有没有点赞, 直接返回评论列表
        UserInfo userInfo = UserInfoThreadLocalUtil.get();
        if (userInfo.getUserId() == null) {
            return ResponseResult.okResult(commentList);
        }

        //2.2登录了, 就要去redis中看这个评论列表中, 用户对那些评论做了点赞 -->加上点赞标识
        //现在集合中是一个个ApComment, 没有标识字段, 所以要转化为vo 用map

        /*优化: 获取用户的点赞集合列表*/
        String user_like_key = "COMMENT_LIKE_" + userInfo.getUserId();
        Set<String> members = stringRedisTemplate.opsForSet().members(user_like_key);
        //这里的就是用户的点赞集合, 在redis中  这样只对redis操作了一次

        List<ApCommentVo> apCommentVoList = commentList.stream().map(apComment -> {
            ApCommentVo apCommentVo = new ApCommentVo();

            BeanUtils.copyProperties(apComment, apCommentVo); //赋值属性
            //判断是否点过赞  reids中判断
            //TODO: 这样要与redis交互很多次, 可以优化用管道
            //String user_like_key = "COMMENT_LIKE_" + userInfo.getUserId();
            //Boolean isLike = stringRedisTemplate.opsForSet().isMember(user_like_key, apComment.getId());
            /*判断当前集合是否包含点赞id, 包含就是点过赞, 不包含就是没点过*/
            boolean isLike = members.contains(apComment.getId());

            //根据判断结果给实体类加上点赞标识
            if (isLike) {
                apCommentVo.setOperation((short) 0);
            } else {
                apCommentVo.setOperation((short) 1);
            }
            return apCommentVo;
        }).collect(Collectors.toList());


        //3.封装数返回
        return ResponseResult.okResult(apCommentVoList);
    }


    /**
     * 发表评论回复的方法
     * @param
     * @return
     */
    @Override
    public ResponseResult saveCommentRepay(String content, String targetId) {

        //调用方法, 完成评论
        ResponseResult result = saveComment(content, targetId);

        if (result.getCode()==200) {
            //就要对评论回复数量加一
            //1.查询条件
            Query query = Query.query(Criteria.where("_id").is(targetId));
            //2.修改
            Update update = new Update();
            update.inc("reply",1);
            mongoTemplate.updateFirst(query,update,ApComment.class);
        }


        return result;
    }


}
