package com.heima.comment.controller;

import com.heima.comment.dto.CommentLikeDto;
import com.heima.comment.dto.CommentListDto;
import com.heima.comment.dto.CommentSaveDto;
import com.heima.comment.service.ApCommentService;
import com.heima.comment.vo.ApCommentVo;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/comment")
@Api(value = "App端评论管理",tags = "评论管理")
public class ApCommentController {

    @Resource
    private ApCommentService apCommentService;

    /**
     * 发表评论
     * @param dto
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("发表评论")
    public ResponseResult save(@RequestBody CommentSaveDto dto){
        if (StringUtils.isEmpty(dto.getContent())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        //想复用不传dto    传评论目标id, 传评论内容
        return apCommentService.saveComment(dto.getContent(),dto.getArticleId().toString());
    }


    /**
     * 对评论点赞
     * 方法做一个通用的
     * 可以对评论点赞, 也可以对评论回复点赞
     */

    @PostMapping("/like")
    @ApiOperation("评论点赞")
    public ResponseResult like(@RequestBody CommentLikeDto dto){
        if (StringUtils.isEmpty(dto.getCommentId())){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        if (dto.getOperation() !=0 ||dto.getOperation() !=1){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }


        //通用方法   可以对评论点赞, 也可以对评论回复点赞
        return apCommentService.like(dto.getCommentId(),dto.getOperation());
    }



    /**
     * 加载(查询文章)评论列表
     * 通过文章id查询
     * @param dto
     * @return
     */
    @PostMapping("/load")
    public ResponseResult<List<ApCommentVo>> findByArticleId(@RequestBody CommentListDto dto) {
        //之前传的是目标, 所以要传目标id(看是文章还是视频)
        if(dto.getArticleId()==null){
        return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
      //最小时间校验, 没有给当前时间
      Date minTime =   dto.getMinDate()==null?new Date():dto.getMinDate();
      //每页展示条数校验
       Integer  size = dto.getSize()==null||dto.getSize()<=0?10:dto.getSize();
      //如果传1000我们只给他展示50条
        size = Math.min(size,50);
        return apCommentService.load(dto.getArticleId().toString(),minTime,dto.getSize());
    }
}
