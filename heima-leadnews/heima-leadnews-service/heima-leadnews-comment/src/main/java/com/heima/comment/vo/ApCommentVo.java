package com.heima.comment.vo;

import com.heima.comment.pojo.ApComment;
import lombok.Data;

@Data
public class ApCommentVo extends ApComment {

    /**
     * 0：点赞
     * 1：没有点赞
     */
    private Short operation;
}