package com.heima.comment.pojo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;

/**
 * APP评论信息
 */
@Data
@Document("ap_comment")  //后面评论数据都要保存到ap_comment集合(mongoDB表)中
public class ApComment {

    /**
     * id
     */
    private String id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String image;

    /**
     * 评论目标id 评论的是文章: targetId就是文章id, 评论的是视频:targetId 就是视频id ,评论的是动态:targetId 就是动态id
     */
    private String targetId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 点赞数
     */
    private Integer likes;

    /**
     * 回复数
     */
    private Integer reply;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 更新时间
     */
    private Date updatedTime;

}