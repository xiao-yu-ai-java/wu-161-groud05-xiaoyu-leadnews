package com.heima.comment.service;

import com.heima.comment.dto.CommentSaveDto;
import com.heima.comment.vo.ApCommentVo;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


public interface ApCommentService {


    /**
     * 发表评论
     * @param content
     * @param
     * @return
     */
    ResponseResult saveComment(String content, String targetId);
    /**
     * 对评论点赞
     * 方法做一个通用的
     * 可以对评论点赞, 也可以对评论回复点赞
     */
    ResponseResult like(String targetId, Short operation);


    /**
     * 加载(查询文章)评论列表
     * 通过文章id查询
     * @param
     * @return
     */
    ResponseResult<List<ApCommentVo>> load(String targetId, Date minTime, Integer size);

    /**
     * 发表评论回复的方法
     * @param dto
     * @return
     */
    ResponseResult saveCommentRepay(String content, String toString);
}
