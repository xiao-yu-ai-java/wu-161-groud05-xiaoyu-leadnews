package com.heima.comment.dto;

import lombok.Data;

/**
 * 发表评论回复: CommentRepayDto
 */
@Data
public class CommentRepayDto {

   /**
    * 文章id
    */
   private String commentId;

   /**
    * 评论内容
    */
   private String content;
}