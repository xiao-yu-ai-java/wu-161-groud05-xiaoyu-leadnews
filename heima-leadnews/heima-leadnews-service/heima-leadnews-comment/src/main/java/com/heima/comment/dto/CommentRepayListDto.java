package com.heima.comment.dto;

import lombok.Data;

import java.util.Date;

/**查询评论回复列表 : CommentRepayListDto
 *
 */
@Data
public class CommentRepayListDto {

   private String commentId;

   private Date minDate;

   private Integer size;
}