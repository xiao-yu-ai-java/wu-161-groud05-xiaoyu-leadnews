package com.heima.recommend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.article.pojo.ApArticleContent;


/**
 * 文章内容信息表(ApArticleContent)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
public interface ApArticleContentMapper extends BaseMapper<ApArticleContent> {

}

