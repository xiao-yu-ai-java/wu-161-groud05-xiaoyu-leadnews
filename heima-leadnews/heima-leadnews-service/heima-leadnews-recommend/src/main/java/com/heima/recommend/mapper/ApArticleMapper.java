package com.heima.recommend.mapper;

import cn.hutool.core.date.DateTime;
import com.heima.model.common.article.pojo.ApArticle;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 文章基础信息表(ApArticle)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
public interface ApArticleMapper extends BaseMapper<ApArticle> {



    /**
     * 根据查询条件查询文章列表
     * @param queryParams
     * @return
     */
    //List<ApArticle> queryArticleByCondition(Map<String, Object> queryParams);


    /**
     * 根据5天前的时间(开始时间)查询发布时间大于 5天前的时间(开始时间)的数据
     */

    List<ApArticle> loadArticleByPublishTime(Date beginTime, Integer size, Integer shardTotal, Integer shardIndex);

}

