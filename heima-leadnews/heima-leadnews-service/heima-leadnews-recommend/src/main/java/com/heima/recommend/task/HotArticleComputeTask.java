package com.heima.recommend.task;

import com.heima.recommend.service.HotArticleService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import javafx.scene.layout.CornerRadii;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Author Administrator
 * @Date 2023/6/29
 **/
@Component
@Slf4j
public class HotArticleComputeTask {

    @Resource
    private HotArticleService hotArticleService;


    /**
     * 定时任务 : 查询5天内发布的数据,存到redis
     *
     */

    //@Scheduled(cron = "0 0 2 * * ?")  //每天凌晨2点
    @XxlJob("computeHotArticleJob")
    public void hotArticleCompute(){
        //获取任务节点分片总数量
        int shardTotal = XxlJobHelper.getShardTotal();
        //获取当前任务节点的编号
        int shardIndex = XxlJobHelper.getShardIndex();

        log.info("热点文章计算任务开始执行");
        hotArticleService.computeHotArticle(shardTotal,shardIndex);
        log.info("热点文章计算任务执行结束");
    }


    /**
     * 每次存任务之前清理redis的任务
     * 清理热点文章数据
     */
    @XxlJob("clearHotArticle")
    public void clearHotArticle(){
        log.info("清理热点文章数据任务开始执行--------------");
        hotArticleService.clearHotArticle();
        log.info("清理热点文章数据任务执行结束--------------");
    }
}
