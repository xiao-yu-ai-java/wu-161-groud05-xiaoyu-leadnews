package com.heima.recommend.service.impl;


import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.heima.common.constants.ArticleContants;
import com.heima.model.common.article.pojo.ApArticle;
import com.heima.model.common.recommend.vo.HotArticleVo;
import com.heima.recommend.mapper.ApArticleMapper;
import com.heima.recommend.service.HotArticleService;
import com.heima.recommend.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class HotArticleServiceImpl implements HotArticleService {

    @Resource
    private ApArticleMapper apArticleMapper;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 手动提交任务到线程池, 直接将线程池对象注过来
     */
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    /*@Resource
    private ComputeAndCacheService computeAndCacheService;
*/
    @Resource
    private RedisUtil redisUtil;

    /**
     * 计算热点文章数据
     * 直接放redis中, 所以不需要返回值
     * compute   计算
     */

//TODO 这种每次查询1000条数据, 然后计算, 再查1000条, 效率低下
    @Override
    public void computeHotArticle(Integer shardTotal,Integer shardIndex) {
        //优化 分片任务调度 不应该取全部数据, 自己分片(节点)取自己的数据
        //优化方式: id取摸 文章id%shardTotal分片总数 =shardIndex任务节点的编号   就放在自己的分片
        /*1. 查询5天内发布的文章数据(mysql中查)  多表查询: ap_article 与 ap_article_config*/

        //查未下架,未删除的 关联查询,   plus无法满足我们需求, 要自己写映射
        //1.1 查询5天前的时间 (因为5天的数据量很大, 不能一次性查询所有, 需要分页加载)
        Date beginTime = DateUtil.offsetDay(new Date(), -5).toJdkDate();
        Integer size = 1000;
        //1.2循环查询数据库中的文章列表
        List<ApArticle> articleList = apArticleMapper.loadArticleByPublishTime(beginTime, size,shardTotal,shardIndex);

        while (true) {
            if (CollectionUtils.isEmpty(articleList)) {
                break;
            }
            //给开始时间赋值, articleList.get(999) 假如查1000条(0-999 索引) 就是最后一个数据
            beginTime = articleList.get(articleList.size() - 1).getPublishTime();


            //Todo 一个线程只能处理10000万条数据, 用10个线程呢  直接加@async注解 直接在方法上注解
            //计算并缓存热点文章数据

            /**
             * 手动提交任务到线程池, 直接将线程池对象注过来
             */

            threadPoolTaskExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    //任务
                    computeAndCacheHotArticle(articleList);
                }
            });



        }


    }

    /**
     * 每次存任务之前清理redis的任务
     * 清理热点文章数据
     */
    @Override
    public void clearHotArticle() {
        // 获取频道缓存的数据的key, 前缀一样
        String hot_article_channel_key = "HOT_ARTICLE_PREFIX" + "_" ;
        // 推荐频道缓存key
        //String hot_article_all_key = "HOT_ARTICLE";
        //所有文章基本信息缓存key
        //String article_info_key = "HOT_ARTICLE_INFO";

        Set<String> keys = redisUtil.scan("HOT_ARTICLE" + "*");
        log.info("扫秒到所有需要清理的key:{}",keys);
        //从redis中删除key
        stringRedisTemplate.delete(keys);

    }
   //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /**
     * 做多线程的异步调度
     * @Async 失效解决方法
     * 1.使用代理对象调用, 麻烦, 要重新定义接口实现类, 把逻辑放里面, 要用直接注入, 调方法
     * 2.手动提交任务到线程池
     * @param articleList
     */
    //Todo 一个线程只能处理10000万条数据, 用10个线程呢  直接加@async注解 直接在方法上注解
    /*@Async("taskExecutor")*/  //开启多线程, 必须要用多public修饰, 返回值用viod 或者future  指定多线程的方法  底层aop aop底层根据代理对象实现 , 所以必须代理对象调用这个方法 (我们这里本类的方法待用本类的方法, 不是代理对象调用的)
    public void computeAndCacheHotArticle(List<ApArticle> articleList) {
        /*2. 根据权重计算文章的推荐分值*/
        List<HotArticleVo> articleVoList = computeHotArticleScore(articleList);//封装一个方法算分  返回一个有算分属性的集合
        /*3. 为推荐频道和每一个频道缓存推荐数(Redis)*/
        savaHotArticleToCache(articleVoList);  //保存热点文章到redis缓存中, 把分值文章集合给我, 我去存
    }


    /**
     * 计算每一个文章的分值
     * 返回的是带有文章分值的文章数据
     *
     * @param articleList
     * @return
     */
    private List<HotArticleVo> computeHotArticleScore(List<ApArticle> articleList) {

        //判断, [这个集合是否为空, 为空直接返回空集合
        if (CollectionUtils.isEmpty(articleList)) {
            return Collections.emptyList();
        }
        //1.遍历获取每一个文章数据(包含算分属性的)
        List<HotArticleVo> articleVoList = articleList.stream().map(apArticle -> {
            HotArticleVo hotArticleVo = new HotArticleVo();
            BeanUtils.copyProperties(apArticle, hotArticleVo);
            //算分设置到vo里面 搞一个方法算分
            Integer score = computeScore(apArticle);
            hotArticleVo.setScore(score);
            return hotArticleVo;
        }).collect(Collectors.toList());

        return articleVoList;
    }


    /**
     * 计算文章分值
     * @param apArticle
     * @return
     */
    private Integer computeScore(ApArticle apArticle) {
        Integer score = 0 ;

        //阅读行为得分累加
        score += apArticle.getViews() == null ? 0 : apArticle.getViews() * ArticleContants.HOT_ARTICLE_VIEW_WEIGHT;
        //点赞行为得分累加
        score += apArticle.getLikes() == null ? 0 : apArticle.getLikes() * ArticleContants.HOT_ARTICLE_LIKE_WEIGHT;
        //评论行为得分累加
        score += apArticle.getComment() == null ? 0 : apArticle.getComment() * ArticleContants.HOT_ARTICLE_COMMENT_WEIGHT;
        //收藏行为得分累加
        score += apArticle.getCollection() == null ? 0 : apArticle.getCollection() * ArticleContants.HOT_ARTICLE_COLLECT_WEIGHT;

        return score;
    }


    /**
     * 保存热点文章到redis缓存中, 把分值文章集合给我, 我去存
     * @param articleVoList  文章集合
     */
    //TODO : 每次高1000条数据, 每条和redis交互3次, 这样压力很大 创建连接, 执行 , 销毁
    private void savaHotArticleToCache(List<HotArticleVo> articleVoList) {

        if (CollectionUtils.isEmpty(articleVoList)){
            return;  //为空存毛
        }

        /**
         * 分组: 原始方法, 为每一个频道存文章  key: 频道id , value: 频道下对应的文章列表
         */
        /*Map<Long,List<HotArticleVo>> channels = new HashMap<>();  //一个频道有多个文章

        for (HotArticleVo hotArticleVo : articleVoList) {  //遍历每一个热点文章
            //从map集合中获取频道数据(存的文章) 集合
            List<HotArticleVo> voList = channels.get(hotArticleVo.getChannelId());
            if (CollectionUtils.isEmpty(voList)){  //刚开始没有频道
                voList = new ArrayList<>();

                voList.add(hotArticleVo);
                channels.put(hotArticleVo.getChannelId(),voList);
            }else { //有频道 把文章加到频道数据集合即可
                voList.add(hotArticleVo);
            }
        }*/

        //    当前频道缓存key
//            String hot_article_channel_key = ArticleContants.HOT_ARTICLE_PREFIX + "_" + channelId;
//            //推荐频道缓存key
//            String hot_article_all_key = ArticleContants.HOT_ARTICLE_PREFIX + ArticleContants.DEFAULT_TAG;
        /**
         * 用stream流分组 HotArticleVo articleVoList 文章集合
         * 对集合数据分组 : 对什么分组, 对热点文章中的频道id分组
         * 分组:  为每一个频道存文章  key: 频道id , value: 频道下对应的文章列表
         */
        Map<Long, List<HotArticleVo>> channels = articleVoList.stream().collect(Collectors.groupingBy(HotArticleVo::getChannelId));
        //1. 为每一个频道缓存文章数据  Zset  为每个频道分组 : 拿到每一个频道, 往redis放

        //TODO 遍历每一个map集合 , map集合存的是频道id和频道下的文章集合
        channels.forEach((channelId , hotArticleVoList) ->{
             // 当前频道缓存key
            String hot_article_channel_key = "HOT_ARTICLE_PREFIX" + "_" + channelId;
            // 推荐频道缓存key
            String hot_article_all_key = "HOT_ARTICLE";
            //所有文章基本信息缓存key
            String article_info_key = "HOT_ARTICLE_INFO";

            /*存到不同的 , redis的大key中*/

            //TODO : 1.优化 每次高1000条数据, 每条和redis交互3次, 这样压力很大 创建连接, 执行 , 销毁 频繁操作reids优化
            //TODO : 2.解决方法: 一 .利用pipleline管道操作(多种类型),.  二. 使用redis批处理指令操作(同一种类型操作)
             // TODO 遍历频道下的文章集合    获取每一个文章数据  对文章数据进行缓存
           /* hotArticleVoList.stream().forEach(hotArticleVo->{
                //1. 为每一个频道缓存文章数据  Zset
                stringRedisTemplate.opsForZSet().add(hot_article_channel_key,hotArticleVo.getId().toString(),hotArticleVo.getScore());

                //2. 为推荐频道(所有频道)缓存文章数据  Zset
                stringRedisTemplate.opsForZSet().add(hot_article_all_key,hotArticleVo.getId().toString(),hotArticleVo.getScore());

                //3. 缓存文章基本信息到Redis  Hash  : 后期从zset集合查文章id, 拿到文章id去hash集合中找文章信息
                stringRedisTemplate.opsForHash().put(article_info_key,hotArticleVo.getId().toString(), JSON.toJSONString(hotArticleVo));
            });*/
            //TODO : 优化: 利用redis的批处理 新的api
            //1. 为每一个频道缓存文章数据  Zset  --->把频道集合下的每一个文章数据对象 转化为 TypedTuple对象(value, 和 score) 转化为set集合

            Set<ZSetOperations.TypedTuple<String>> typedTupleSet = articleVoList.stream().map(hotArticleVo -> new DefaultTypedTuple<String>(hotArticleVo.getChannelId().toString(), hotArticleVo.getScore().doubleValue())).collect(Collectors.toSet());
            stringRedisTemplate.opsForZSet().add(hot_article_channel_key,typedTupleSet);// 一次性存多个文章到一个频道里面(key,set<TypedTuple>)

            //2. 为推荐频道(所有频道)缓存文章数据  Zset;
            stringRedisTemplate.opsForZSet().add(hot_article_all_key,typedTupleSet);

            //3. 缓存文章基本信息到Redis
            //map 集合key文章id  , value文章json字符串  每一个键值对是一个文章对象

            Map<String, String> articleInfoMap = hotArticleVoList.stream().collect(Collectors.toMap(hotArticleVo -> hotArticleVo.getId().toString(), hotArticleVo -> JSON.toJSONString(hotArticleVo)));
            stringRedisTemplate.opsForHash().putAll(article_info_key,articleInfoMap);

        });
    }
}
