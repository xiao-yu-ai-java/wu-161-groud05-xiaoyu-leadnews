package com.heima.recommend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.article.pojo.ApArticleConfig;


/**
 * 文章配置表(ApArticleConfig)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
public interface ApArticleConfigMapper extends BaseMapper<ApArticleConfig> {

}

