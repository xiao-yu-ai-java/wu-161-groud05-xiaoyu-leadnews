package com.heima.recommend.service;

import cn.hutool.core.date.DateTime;

public interface HotArticleService {

    /**
     * 计算热点文章数据
     * 直接放redis中, 所以不需要返回值
     * compute   计算
     */

    public void computeHotArticle(Integer shardTotal,Integer shardIndex);

    void clearHotArticle();
}
