package com.heima.recommend.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.common.constants.ArticleContants;
import com.heima.model.common.article.pojo.ApArticle;
import com.heima.model.common.recommend.vo.HotArticleVo;
import com.heima.recommend.service.ComputeAndCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.DefaultTypedTuple;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Author Administrator
 * @Date 2023/6/29
 **/
@Service
@Slf4j
public class ComputeAndCacheServiceImpl implements ComputeAndCacheService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void computeAndCacheHotArticle(List<ApArticle> articleList) {
        log.info("异步线程池进行热文章计算和缓存,线程名称:{}",Thread.currentThread().getName());
        //2. 根据权重计算文章的推荐分值
        List<HotArticleVo> articleVoList = computeHotArticleScore(articleList);
        //articleVoList.stream().forEach(hotArticleVo -> System.out.println(hotArticleVo));
        //3. 为推荐频道和每一个频道缓存推荐数据(Redis)
        savaHotArticleToCache(articleVoList);
    }


    /**
     * 缓存热点文章数据到Redis
     *
     * @param articleVoList
     */
    private void savaHotArticleToCache(List<HotArticleVo> articleVoList) {
        if (CollectionUtils.isEmpty(articleVoList)) {
            return;
        }
        //使用Stream流对集合数据进行分组  key : 频道id    value :频道下对应的文章列表
        Map<Long, List<HotArticleVo>> channels = articleVoList.stream().collect(Collectors.groupingBy(HotArticleVo::getChannelId));
        //遍历对数据进行缓存
        channels.forEach((channelId, hotArticleVoList) -> {
            //当前频道缓存key
            String hot_article_channel_key = ArticleContants.HOT_ARTICLE_PREFIX + "_" + channelId;
            //推荐频道缓存key
            String hot_article_all_key = ArticleContants.HOT_ARTICLE_PREFIX +"_"+ ArticleContants.DEFAULT_TAG;
            //文章基本信息缓存key
            String article_info_key = ArticleContants.HOT_ARTICLE_INFO_KEY;
            //遍历获取每个文章数据 , 对文章数据进行缓存
            /**
             * 频繁操作Redis优化 :
             * 1. 使用Redis管道操作进行优化 pipeline
             * 2. 使用Redis批处理指令进行优化
             *
             * 如果一批次命令中涉及到多种数据类型的多个不同操作, 可以使用pipeline
             * 如果一批次命令操作的数据类型一样操作的数据key也相同 , 可以使用批处理指令
             */
            //hotArticleVoList.stream().forEach(hotArticleVo -> {
            //    //1. 为每一个频道缓存文章数据 ZSET
            //    stringRedisTemplate.opsForZSet().add(hot_article_channel_key, hotArticleVo.getId().toString(), hotArticleVo.getScore());
            //    //2. 为推荐频道(所有频道)缓存文章数据 ZSET
            //    stringRedisTemplate.opsForZSet().add(hot_article_all_key, hotArticleVo.getId().toString(), hotArticleVo.getScore());
            //    //3. 缓存文章基本信息到Redis HASH
            //    stringRedisTemplate.opsForHash().put(article_info_key, hotArticleVo.getId().toString(), JSON.toJSONString(hotArticleVo));
            //});

            //1. 为每一个频道缓存文章数据 ZSET  ----> 把频道下的每一个文章数据转化为 TypeTuple
            Set<ZSetOperations.TypedTuple<String>> typedTupleSet = hotArticleVoList.stream()
                    .map(hotArticleVo -> new DefaultTypedTuple<String>(hotArticleVo.getId().toString(), hotArticleVo.getScore().doubleValue()))
                    .collect(Collectors.toSet());
            stringRedisTemplate.opsForZSet().add(hot_article_channel_key,typedTupleSet);

            //2. 为推荐频道(所有频道)缓存文章数据 ZSET
            stringRedisTemplate.opsForZSet().add(hot_article_all_key, typedTupleSet);

            //3. 缓存文章基本信息到Redis HASH --- toMap方法 : 集合中的每一个对象, 对应的是Map中的一个键值对
            Map<String, String> articleInfoMap = hotArticleVoList.stream().collect(Collectors.toMap(hotArticleVo -> hotArticleVo.getId().toString(), hotArticleVo -> JSON.toJSONString(hotArticleVo)));
            stringRedisTemplate.opsForHash().putAll(article_info_key,articleInfoMap);
        });
    }

    /**
     * 计算每一个文章的分值数据
     *
     * @param articleList 带有文章分值的文章列表数据
     * @return
     */
    private List<HotArticleVo> computeHotArticleScore(List<ApArticle> articleList) {
        if (CollectionUtils.isEmpty(articleList)) {
            return Collections.emptyList();
        }
        //遍历获取每一个文章数据计算分值
        List<HotArticleVo> articleVoList = articleList.stream().map(apArticle -> {
            HotArticleVo vo = new HotArticleVo();
            BeanUtils.copyProperties(apArticle, vo);
            //计算文章分值, 封装分值数据
            Integer score = computeScore(apArticle);
            vo.setScore(score);

            return vo;
        }).collect(Collectors.toList());

        return articleVoList;
    }

    private Integer computeScore(ApArticle apArticle) {
        Integer score = 0;
        //阅读行为得分累加
        score += apArticle.getViews() == null ? 0 : apArticle.getViews() * ArticleContants.HOT_ARTICLE_VIEW_WEIGHT;
        //点赞行为得分累加
        score += apArticle.getLikes() == null ? 0 : apArticle.getLikes() * ArticleContants.HOT_ARTICLE_LIKE_WEIGHT;
        //评论行为得分累加
        score += apArticle.getComment() == null ? 0 : apArticle.getComment() * ArticleContants.HOT_ARTICLE_COMMENT_WEIGHT;
        //收藏行为得分累加
        score += apArticle.getCollection() == null ? 0 : apArticle.getCollection() * ArticleContants.HOT_ARTICLE_COLLECT_WEIGHT;

        return score;
    }
}
