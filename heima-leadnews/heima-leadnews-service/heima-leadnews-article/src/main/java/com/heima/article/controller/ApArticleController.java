package com.heima.article.controller;



import com.heima.article.service.ApArticleService;
import com.heima.common.constants.ArticleContants;
import com.heima.model.common.article.dto.ApArticleDto;
import com.heima.model.common.article.dto.ArticleHomeDto;
import com.heima.model.common.dtos.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * 文章基础信息表(ApArticle)表控制层
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
@RestController
@RequestMapping("api/v1/article")
@Api(value = "App端文章管理api",tags = "App端文章")
public class ApArticleController  {
    /**
     * 服务对象
     */
    @Resource
    private ApArticleService apArticleService;

    /**自媒体服务审核文章后通过feign发布, 发布到文章服务,保存到文章数据库通过feign发布后, app用户通过文章服务就可以看到文章
     * 保存APP端文章数据到数据库
     * @param apArticleDto
     * @return
     */
    @PostMapping("/save")
    public ResponseResult<String> saveApArticle(@RequestBody ApArticleDto apArticleDto){
    return apArticleService.saveApArticle(apArticleDto);
    }





    /**
     * APP首页默认加载文章列表   0 : 加载 更多  1 : 加载最新
     *发布时间小于当前时间(最小时间)
     * @param dto
     * @return
     */
    @ApiOperation("首页默认加载")
    @PostMapping(path = "/load")
    public ResponseResult load(@RequestBody ArticleHomeDto dto) {
        return apArticleService.load(dto, ArticleContants.LOAD_TYPE_MORE);
    }

    /**
     * APP首页默认加载更多文章数据
     *发布时间小于(最小时间)
     * @param dto
     * @return
     */
    @ApiOperation("首页默认加载更多")
    @PostMapping(path = "/loadmore")
    public ResponseResult loadmore(@RequestBody ArticleHomeDto dto) {
        return apArticleService.load(dto, ArticleContants.LOAD_TYPE_MORE);
    }

    /**
     * APP首页默认加载最新文章数据
     *发布时间大于最大发布时间
     * @param dto
     * @return
     */
    @ApiOperation("首页默认加载最新")
    @PostMapping(path = "/loadnew")
    public ResponseResult loadnew(@RequestBody ArticleHomeDto dto) {
        return apArticleService.load(dto, ArticleContants.LOAD_TYPE_NEW);
    }

}

