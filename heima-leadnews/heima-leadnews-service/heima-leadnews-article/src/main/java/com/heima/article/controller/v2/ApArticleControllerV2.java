package com.heima.article.controller.v2;


import com.heima.article.service.ApArticleService;
import com.heima.common.constants.ArticleContants;
import com.heima.model.common.article.dto.ArticleHomeDto;
import com.heima.model.common.article.pojo.ApArticle;
import com.heima.model.common.dtos.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


/**
 * 文章基础信息表(ApArticle)表控制层
 *
 * @author dwxw
 * @since 2023-06-15 12:08:57
 */
@RestController
@RequestMapping("/api/v2/article")
@Api(value = "APP端文章管理API",tags = "APP端文章")
public class ApArticleControllerV2 {
    /**
     * 服务对象
     */
    @Resource
    private ApArticleService apArticleService;


    /**
     * 首页默认加载
     * @param dto
     * @return
     */
    @PostMapping(path = "/load")
    @ApiOperation("首页默认加载")
    public ResponseResult<List<ApArticle>> load(@RequestBody ArticleHomeDto dto){
        return apArticleService.loadV2(dto, ArticleContants.LOAD_TYPE_MORE);
    }

}

