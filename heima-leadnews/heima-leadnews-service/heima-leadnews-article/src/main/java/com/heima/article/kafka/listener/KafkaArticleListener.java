package com.heima.article.kafka.listener;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.common.constants.KafkaMessageConstants;
import com.heima.model.common.article.pojo.ApArticle;
import com.heima.model.common.article.pojo.ApArticleConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * kafka 消费者监听到了自媒体服务发送的消息(文章id和状态) 更改文章服务状态
 * 监听器也是spring中的一个组件 所以要加注解@Componen
 */

@Component
@Slf4j
public class KafkaArticleListener {

    @Resource
    private ApArticleMapper apArticleMapper;
    @Resource
    private ApArticleConfigMapper apArticleConfigMapper;

    @KafkaListener(topics = KafkaMessageConstants.WM_Material_AUTO_SCAN_TOPIC,groupId = "listenArticleUpOrDown")
    public void listenArticleUpOrDown(ConsumerRecord<String,String> record){
        String value = record.value();   // newsId
        log.info("接收到消息:{}", value);
        if (StringUtils.isEmpty(value)) {
            return;
        }

        Map<String,String> map = JSONObject.parseObject(value, Map.class);
        Long newsId = Long.valueOf(map.get("newsId"));
        Short enable = Short.valueOf(map.get("enable"));

        //通过newsId到ap_article查出article_id, 通过article_id到ap_article_config修改enable

        ApArticle article = apArticleMapper.selectOne(Wrappers.<ApArticle>lambdaQuery().eq(ApArticle::getNewsId,newsId));
        if (article==null){
            return;
        }
        Long articleId = article.getId();

        //通过article_id到ap_article_config修改enable  先查判断有没有
        ApArticleConfig apArticleConfig = apArticleConfigMapper.selectOne(Wrappers.<ApArticleConfig>lambdaQuery().eq(ApArticleConfig::getArticleId, article.getId()));
        if (apArticleConfig == null) {
            return;
        }

        apArticleConfig.setEnable(enable.intValue());

        apArticleConfigMapper.updateById(apArticleConfig);


    }
}
