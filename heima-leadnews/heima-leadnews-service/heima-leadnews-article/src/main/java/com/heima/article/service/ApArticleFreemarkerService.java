package com.heima.article.service;

public interface ApArticleFreemarkerService {


    /**
     * 构建文章详情页面
     * @param content
     * @return
     */
    public String buildArticleStaticHtml(Long id ,String content);
}
