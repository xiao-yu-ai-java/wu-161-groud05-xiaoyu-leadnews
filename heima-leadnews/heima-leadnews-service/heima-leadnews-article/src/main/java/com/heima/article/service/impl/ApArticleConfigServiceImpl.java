package com.heima.article.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleConfigMapper;

import com.heima.article.service.ApArticleConfigService;
import com.heima.model.common.article.pojo.ApArticleConfig;
import org.springframework.stereotype.Service;

/**
 * 文章配置表(ApArticleConfig)表服务实现类
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
@Service("apArticleConfigService")
public class ApArticleConfigServiceImpl extends ServiceImpl<ApArticleConfigMapper, ApArticleConfig> implements ApArticleConfigService {

}

