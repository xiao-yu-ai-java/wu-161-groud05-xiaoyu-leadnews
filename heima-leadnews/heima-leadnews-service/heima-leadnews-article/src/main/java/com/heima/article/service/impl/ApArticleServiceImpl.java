package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.mapper.ApArticleMapper;

import com.heima.article.service.ApArticleFreemarkerService;
import com.heima.article.service.ApArticleService;
import com.heima.common.constants.ArticleContants;
import com.heima.common.constants.KafkaMessageConstants;
import com.heima.model.common.article.dto.ApArticleDto;
import com.heima.model.common.article.dto.ArticleHomeDto;
import com.heima.model.common.article.pojo.ApArticle;
import com.heima.model.common.article.pojo.ApArticleConfig;
import com.heima.model.common.article.pojo.ApArticleContent;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.common.recommend.vo.HotArticleVo;
import com.heima.model.common.search.vo.SearchArticleVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 文章基础信息表(ApArticle)表服务实现类
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
@Service("apArticleService")
public class ApArticleServiceImpl extends ServiceImpl<ApArticleMapper, ApArticle> implements ApArticleService {

    /**
     * 自媒体服务审核文章后, 发布到文章服务,保存到文章数据库, app用户通过文章服务就可以看到文章
     * 保存APP端文章数据到数据库
     *
     * @param apArticleDto
     * @return 三个表的增删改
     */
    @Resource
    private ApArticleMapper apArticleMapper;

    @Resource
    private ApArticleConfigMapper apArticleConfigMapper;

    @Resource
    private ApArticleContentMapper apArticleContentMapper;

    @Resource
    private ApArticleFreemarkerService apArticleFreemarkerService;

    @Resource
    private KafkaTemplate kafkaTemplate;

    @Resource
    private StringRedisTemplate stringRedisTemplate;


    @Override
    public ResponseResult<String> saveApArticle(ApArticleDto apArticleDto) {

        //1.参数检验
        if (apArticleDto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        //判断用户修改或新增   用newId判断
        ApArticle article1 = apArticleMapper.selectOne(Wrappers.<ApArticle>lambdaQuery().eq(ApArticle::getNewsId, apArticleDto.getNewsId()));

        //新增逻辑
        if (article1 == null) {

            //2.保存文章数据到文章基本信息表
            article1 = new ApArticle();
            BeanUtils.copyProperties(apArticleDto, article1);
            apArticleMapper.insert(article1);
            //3.保存文章配置信息到文章配置信息表
            ApArticleConfig congig = new ApArticleConfig();
            congig.setArticleId(article1.getId());
            congig.setEnable(1);   //默认值
            congig.setIsDelete(1);  //默认值
            congig.setIsComment(1);  //默认值
            congig.setIsForward(1);  //默认值
            apArticleConfigMapper.insert(congig);

            //4.保存文章内容到文章文章内容信息表
            ApArticleContent content = new ApArticleContent();
            content.setArticleId(article1.getId());
            content.setContent(apArticleDto.getContent());

            apArticleContentMapper.insert(content);
        }else {
            //修改基本信息
            BeanUtils.copyProperties(apArticleDto, article1, "id");  //要忽略id dto里面id是空的 自媒体不知道app文章id(主键)
            apArticleMapper.updateById(article1);

            //修改文章内容信息
            ApArticleContent apArticleContent = new ApArticleContent();
            apArticleContent.setArticleId(article1.getId());
            apArticleContent.setContent(apArticleDto.getContent());
            apArticleContentMapper.updateById(apArticleContent);

        }

        /*新增文章, 修改文章, 都要生成*/
        //使用freemarket模板生成静态HtML页面
      String url = apArticleFreemarkerService.buildArticleStaticHtml(article1.getId(),apArticleDto.getContent());
        //更新静态页面地址到数据库
        article1.setStaticUrl(url);
        apArticleMapper.updateById(article1);



        //发送消息将文章数据同步到es
        SearchArticleVo searchArticleVo = new SearchArticleVo();
        BeanUtils.copyProperties(article1,searchArticleVo);
        searchArticleVo.setContent(apArticleDto.getContent());

        kafkaTemplate.send(KafkaMessageConstants.AP_ARTICLE_ES_SYNC_TOPIC, JSON.toJSONString(searchArticleVo));

        return ResponseResult.okResult("操作成功");
    }


    @Override
    public ResponseResult load(ArticleHomeDto dto, short loadType) {
        //1.参数校验
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //1.1频道参数检验
        String tag = StringUtils.isEmpty(dto.getTag()) ? ArticleContants.DEFAULT_TAG : dto.getTag();
        //1.2每页展示数据条数校验
        Integer size = dto.getSize() == null || dto.getSize() <= 0 ? 10 : dto.getSize();
        //1.3最大时间校验(发布时间最大)
        Date maxTime = dto.getMaxBehotTime() == null ? new Date() : dto.getMaxBehotTime();
        //1.4最小时间校验(发布时间最小)
        Date minTime = dto.getMinBehotTime() == null ? new Date() : dto.getMinBehotTime();
        //2.封装查询条件
        //文章有状态, 有的文章上架了 要展示, 有的没有上架, 不要展示. 有plus麻烦, 直接用mybatis
        Map<String,Object> queryParams = new HashMap<>(5);
        queryParams.put("tag",tag);
        queryParams.put("size",size);
        queryParams.put("maxTime",maxTime);
        queryParams.put("minTime",minTime);
        queryParams.put("loadType",loadType);
        //3.执行查询, 获取查询结果
      List<ApArticle> articleList = apArticleMapper.queryArticleByCondition(queryParams);
        //4.封装数据返回


        return ResponseResult.okResult(articleList);
    }
    @Override
    public ResponseResult<List<ApArticle>> loadV2(ArticleHomeDto dto, short loadTypeMore) {
        //参数校验
        String tag = dto.getTag();
        tag = StringUtils.isEmpty(tag) ? ArticleContants.DEFAULT_TAG : tag;
        //如果是首页---从缓存中加载
        //1. 从对应频道的推荐集合中查询推荐文章id
        String key = ArticleContants.HOT_ARTICLE_PREFIX + "_" + tag;
        //range 方法默认按照分值从低到高 , reverseRange : 按照分值从高到低
        Set hotArticleIds = stringRedisTemplate.opsForZSet().reverseRange(key, 0, 30);

        if (CollectionUtils.isEmpty(hotArticleIds)) {
            return load(dto, loadTypeMore);
        }
        //2. 根据id查询文章详情
        List<String> list = stringRedisTemplate.opsForHash().multiGet(ArticleContants.HOT_ARTICLE_INFO_KEY, hotArticleIds);
        //3. 进行数据格式转化
        List<HotArticleVo> hotArticleVoList = list.stream()
                .map(json -> JSON.parseObject(json, HotArticleVo.class))
                //对文章的分值进行重新排序
                .sorted((o1, o2) -> o2.getScore()-o1.getScore())
                .collect(Collectors.toList());

        return ResponseResult.okResult(hotArticleVoList);
    }

}

