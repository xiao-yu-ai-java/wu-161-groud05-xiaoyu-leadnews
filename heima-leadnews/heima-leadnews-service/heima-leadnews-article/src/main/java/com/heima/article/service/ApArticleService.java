package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.heima.model.common.article.dto.ApArticleDto;
import com.heima.model.common.article.dto.ArticleHomeDto;
import com.heima.model.common.article.pojo.ApArticle;
import com.heima.model.common.dtos.ResponseResult;

import java.util.List;

/**
 * 文章基础信息表(ApArticle)表服务接口
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
public interface ApArticleService extends IService<ApArticle> {
    /**自媒体服务审核文章后, 发布到文章服务,保存到文章数据库, app用户通过文章服务就可以看到文章
     * 保存APP端文章数据到数据库
     * @param apArticleDto
     * @return
     */
    ResponseResult<String> saveApArticle(ApArticleDto apArticleDto);


    /**
     * 加载首页文章数据
     * @param dto 条件
     * @param loadType 加载类型 1 : more加载更多  , 2 : new加载最新
     * @return
     */
    ResponseResult load(ArticleHomeDto dto, short loadType);

    /**
     * V2版本首页架加载文章列表
     * @param dto
     * @param loadTypeMore
     * @return
     */
    ResponseResult<List<ApArticle>> loadV2(ArticleHomeDto dto, short loadTypeMore);
}

