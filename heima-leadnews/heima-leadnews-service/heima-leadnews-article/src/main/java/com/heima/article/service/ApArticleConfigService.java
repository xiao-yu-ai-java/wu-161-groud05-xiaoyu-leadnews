package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.article.pojo.ApArticleConfig;


/**
 * 文章配置表(ApArticleConfig)表服务接口
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
public interface ApArticleConfigService extends IService<ApArticleConfig> {

}

