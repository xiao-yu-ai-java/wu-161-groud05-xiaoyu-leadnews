package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.common.article.pojo.ApArticle;

import java.util.List;
import java.util.Map;


/**
 * 文章基础信息表(ApArticle)表数据库访问层
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
public interface ApArticleMapper extends BaseMapper<ApArticle> {

    /**
     * 根据查询条件查询文章列表
     * @param queryParams
     * @return
     */
    List<ApArticle> queryArticleByCondition(Map<String, Object> queryParams);
}

