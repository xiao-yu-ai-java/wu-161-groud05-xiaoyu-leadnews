package com.heima.article.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleContentMapper;

import com.heima.article.service.ApArticleContentService;
import com.heima.model.common.article.pojo.ApArticleContent;
import org.springframework.stereotype.Service;

/**
 * 文章内容信息表(ApArticleContent)表服务实现类
 *
 * @author makejava
 * @since 2023-06-16 20:47:55
 */
@Service("apArticleContentService")
public class ApArticleContentServiceImpl extends ServiceImpl<ApArticleContentMapper, ApArticleContent> implements ApArticleContentService {

}

