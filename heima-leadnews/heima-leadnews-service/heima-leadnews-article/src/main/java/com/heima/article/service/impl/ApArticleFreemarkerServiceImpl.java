package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.heima.article.service.ApArticleFreemarkerService;
import com.heima.common.exception.CustomException;
import com.heima.minio.MinIOTemplate;
import com.heima.model.common.enums.AppHttpCodeEnum;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.stereotype.Service;
import springfox.documentation.spring.web.json.Json;

import javax.annotation.Resource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ApArticleFreemarkerServiceImpl implements ApArticleFreemarkerService {

    /**
     * article.ftl
     * 构建文章详情页面
     *
     * 转对象paserObject
     * 转数组paserArray 再转化为map我们要的类型
     * @param content
     * @return
     */

    @Resource
    private Configuration configuration;

    @Resource
    private MinIOTemplate minIOTemplate;


    @Override
    public String buildArticleStaticHtml(Long id ,String content) {
        String url = null;
        try {
            //1.加载freemarket模板*****
            Template template = configuration.getTemplate("article.ftl");
            //2.准备数据模型  content 里面是json的数组 先转数组, 再生成我们要的类型
            List<Map> maps = JSON.parseArray(content, Map.class);
            //3.基于数据模型填充模板, 生成静态HtMl页面
            Map<String,Object> dataModel = new HashMap<>();
            dataModel.put("content",maps);
            //创建一个字符中输出流
            StringWriter writer = new StringWriter();
            //基于模板生成静态文件， 到字符中输出流  writer之前是本地路径
            template.process(dataModel,writer);
            //4.将生成好的静态文件, 写入到字符串输出流
            //将字符中输出流中的文本内容取出 ， 转化为字节 ， 产生字节输入流
            ByteArrayInputStream inputStream = new ByteArrayInputStream(writer.toString().getBytes(StandardCharsets.UTF_8));
            url = minIOTemplate.uploadImgFile("", id + "html", inputStream);
        } catch (Exception e) {
            throw new CustomException(AppHttpCodeEnum.SERVER_ERROR);
        }
        return url;
    }
}
